﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace JLSportPlannerData.Models
{
    public class JoinSelection
    {
        [Key]
        public int Id { get; set; }

        public int ClubId { get; set; }
        public Club Club { get; set; }

        public int SelectionId { get; set; }
        public Selection Selection { get; set; }

        public ICollection<Post> Post { get; set; }
        public ICollection<Event> Event { get; set; }
    }
}
