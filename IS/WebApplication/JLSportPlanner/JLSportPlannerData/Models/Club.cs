﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace JLSportPlannerData.Models
{
    public class Club
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(60)]
        public string Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public int SportId { get; set; }
        public Sport Sport { get; set; }

        public ICollection<JoinClub> JoinClub { get; set; }

        public ICollection<JoinSelection> JoinSelection { get; set; }
    }
}
