﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace JLSportPlannerData.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime Created { get; set; }

        public int ClubId { get; set; }
        public Club Club { get; set; }
        public int SelectionId { get; set; }
        public Selection Selection { get; set; }
    }
}
