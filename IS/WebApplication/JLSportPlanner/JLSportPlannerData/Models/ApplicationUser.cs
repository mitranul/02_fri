﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace JLSportPlannerData.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(45)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string? Address { get; set; }

        [Phone]
        public string? Phone { get; set; }

        [EmailAddress]
        public string? Email { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateEdited { get; set; }
        public ICollection<JoinClub> JoinClub { get; set; }
        //public ICollection<ApplicationUserRole> UserRoles { get; set; }

    }
}
