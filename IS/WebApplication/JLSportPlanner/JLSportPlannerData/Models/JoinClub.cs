﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace JLSportPlannerData.Models
{
    public class JoinClub
    {
        [Key]
        public int Id { get; set; }

        public String UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int ClubId { get; set; }
        public Club Club { get; set; }

        [Required]
        public DateTime joinDate { get; set; }
    }
}
