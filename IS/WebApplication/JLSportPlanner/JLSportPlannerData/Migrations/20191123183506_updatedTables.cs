﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class updatedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clubs_Sports_SportidId",
                table: "Clubs");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Clubs_ClubId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_ClubId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Clubs_SportidId",
                table: "Clubs");

            migrationBuilder.DropColumn(
                name: "ClubId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "SportidId",
                table: "Clubs");

            migrationBuilder.AddColumn<int>(
                name: "SportId",
                table: "Clubs",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "JoinSelection",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClubId = table.Column<int>(nullable: true),
                    SelectionId = table.Column<int>(nullable: true),
                    EventId = table.Column<int>(nullable: true),
                    PostId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoinSelection", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JoinSelection_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JoinSelection_Events_EventId",
                        column: x => x.EventId,
                        principalTable: "Events",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JoinSelection_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JoinSelection_Selections_SelectionId",
                        column: x => x.SelectionId,
                        principalTable: "Selections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clubs_SportId",
                table: "Clubs",
                column: "SportId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelection_ClubId",
                table: "JoinSelection",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelection_EventId",
                table: "JoinSelection",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelection_PostId",
                table: "JoinSelection",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelection_SelectionId",
                table: "JoinSelection",
                column: "SelectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs",
                column: "SportId",
                principalTable: "Sports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs");

            migrationBuilder.DropTable(
                name: "JoinSelection");

            migrationBuilder.DropIndex(
                name: "IX_Clubs_SportId",
                table: "Clubs");

            migrationBuilder.DropColumn(
                name: "SportId",
                table: "Clubs");

            migrationBuilder.AddColumn<int>(
                name: "ClubId",
                table: "Posts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SportidId",
                table: "Clubs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ClubId",
                table: "Posts",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_Clubs_SportidId",
                table: "Clubs",
                column: "SportidId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clubs_Sports_SportidId",
                table: "Clubs",
                column: "SportidId",
                principalTable: "Sports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Clubs_ClubId",
                table: "Posts",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
