﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class UpdateDatabase00023 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinClubs_Clubs_ClubId",
                table: "JoinClubs");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections");

            migrationBuilder.AlterColumn<int>(
                name: "SelectionId",
                table: "JoinSelections",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClubId",
                table: "JoinSelections",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClubId",
                table: "JoinClubs",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinClubs_Clubs_ClubId",
                table: "JoinClubs",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinClubs_Clubs_ClubId",
                table: "JoinClubs");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections");

            migrationBuilder.AlterColumn<int>(
                name: "SelectionId",
                table: "JoinSelections",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ClubId",
                table: "JoinSelections",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ClubId",
                table: "JoinClubs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_JoinClubs_Clubs_ClubId",
                table: "JoinClubs",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
