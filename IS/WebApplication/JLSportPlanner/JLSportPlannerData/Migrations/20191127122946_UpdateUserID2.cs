﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class UpdateUserID2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinClubs_AspNetUsers_UserId1",
                table: "JoinClubs");

            migrationBuilder.DropIndex(
                name: "IX_JoinClubs_UserId1",
                table: "JoinClubs");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "JoinClubs");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "JoinClubs",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_JoinClubs_UserId",
                table: "JoinClubs",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinClubs_AspNetUsers_UserId",
                table: "JoinClubs",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinClubs_AspNetUsers_UserId",
                table: "JoinClubs");

            migrationBuilder.DropIndex(
                name: "IX_JoinClubs_UserId",
                table: "JoinClubs");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "JoinClubs",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "JoinClubs",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JoinClubs_UserId1",
                table: "JoinClubs",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinClubs_AspNetUsers_UserId1",
                table: "JoinClubs",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
