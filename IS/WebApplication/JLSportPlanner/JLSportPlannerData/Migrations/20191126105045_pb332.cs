﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class pb332 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs");

            migrationBuilder.AlterColumn<int>(
                name: "SportId",
                table: "Clubs",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs",
                column: "SportId",
                principalTable: "Sports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs");

            migrationBuilder.AlterColumn<int>(
                name: "SportId",
                table: "Clubs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Clubs_Sports_SportId",
                table: "Clubs",
                column: "SportId",
                principalTable: "Sports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
