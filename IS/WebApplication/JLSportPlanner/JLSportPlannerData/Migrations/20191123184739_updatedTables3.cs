﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class updatedTables3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Events_EventId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Posts_PostId",
                table: "JoinSelections");

            migrationBuilder.DropIndex(
                name: "IX_JoinSelections_EventId",
                table: "JoinSelections");

            migrationBuilder.DropIndex(
                name: "IX_JoinSelections_PostId",
                table: "JoinSelections");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "JoinSelections");

            migrationBuilder.DropColumn(
                name: "PostId",
                table: "JoinSelections");

            migrationBuilder.AddColumn<int>(
                name: "JoinSelectionId",
                table: "Posts",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JoinSelectionId",
                table: "Events",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Posts_JoinSelectionId",
                table: "Posts",
                column: "JoinSelectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_JoinSelectionId",
                table: "Events",
                column: "JoinSelectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_JoinSelections_JoinSelectionId",
                table: "Events",
                column: "JoinSelectionId",
                principalTable: "JoinSelections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_JoinSelections_JoinSelectionId",
                table: "Posts",
                column: "JoinSelectionId",
                principalTable: "JoinSelections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_JoinSelections_JoinSelectionId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_JoinSelections_JoinSelectionId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_JoinSelectionId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Events_JoinSelectionId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "JoinSelectionId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "JoinSelectionId",
                table: "Events");

            migrationBuilder.AddColumn<int>(
                name: "EventId",
                table: "JoinSelections",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PostId",
                table: "JoinSelections",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelections_EventId",
                table: "JoinSelections",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinSelections_PostId",
                table: "JoinSelections",
                column: "PostId");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Events_EventId",
                table: "JoinSelections",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Posts_PostId",
                table: "JoinSelections",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
