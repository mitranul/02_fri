﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class updatedTables2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelection_Clubs_ClubId",
                table: "JoinSelection");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelection_Events_EventId",
                table: "JoinSelection");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelection_Posts_PostId",
                table: "JoinSelection");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelection_Selections_SelectionId",
                table: "JoinSelection");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JoinSelection",
                table: "JoinSelection");

            migrationBuilder.RenameTable(
                name: "JoinSelection",
                newName: "JoinSelections");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelection_SelectionId",
                table: "JoinSelections",
                newName: "IX_JoinSelections_SelectionId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelection_PostId",
                table: "JoinSelections",
                newName: "IX_JoinSelections_PostId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelection_EventId",
                table: "JoinSelections",
                newName: "IX_JoinSelections_EventId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelection_ClubId",
                table: "JoinSelections",
                newName: "IX_JoinSelections_ClubId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JoinSelections",
                table: "JoinSelections",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "JoinClubs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: true),
                    ClubId = table.Column<int>(nullable: true),
                    joinDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoinClubs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JoinClubs_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JoinClubs_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JoinClubs_ClubId",
                table: "JoinClubs",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinClubs_UserId",
                table: "JoinClubs",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Events_EventId",
                table: "JoinSelections",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Posts_PostId",
                table: "JoinSelections",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Clubs_ClubId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Events_EventId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Posts_PostId",
                table: "JoinSelections");

            migrationBuilder.DropForeignKey(
                name: "FK_JoinSelections_Selections_SelectionId",
                table: "JoinSelections");

            migrationBuilder.DropTable(
                name: "JoinClubs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JoinSelections",
                table: "JoinSelections");

            migrationBuilder.RenameTable(
                name: "JoinSelections",
                newName: "JoinSelection");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelections_SelectionId",
                table: "JoinSelection",
                newName: "IX_JoinSelection_SelectionId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelections_PostId",
                table: "JoinSelection",
                newName: "IX_JoinSelection_PostId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelections_EventId",
                table: "JoinSelection",
                newName: "IX_JoinSelection_EventId");

            migrationBuilder.RenameIndex(
                name: "IX_JoinSelections_ClubId",
                table: "JoinSelection",
                newName: "IX_JoinSelection_ClubId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JoinSelection",
                table: "JoinSelection",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ClubId = table.Column<int>(type: "int", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateEdited = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(45)", maxLength: 45, nullable: false),
                    OwnerId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Clubs_ClubId",
                        column: x => x.ClubId,
                        principalTable: "Clubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_AspNetUsers_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_ClubId",
                table: "Users",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_OwnerId",
                table: "Users",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelection_Clubs_ClubId",
                table: "JoinSelection",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelection_Events_EventId",
                table: "JoinSelection",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelection_Posts_PostId",
                table: "JoinSelection",
                column: "PostId",
                principalTable: "Posts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_JoinSelection_Selections_SelectionId",
                table: "JoinSelection",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
