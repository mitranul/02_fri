﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JLSportPlannerData.Migrations
{
    public partial class DatabaseUpdate4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClubId",
                table: "Posts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SelectionId",
                table: "Posts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClubId",
                table: "Events",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SelectionId",
                table: "Events",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ClubId",
                table: "Posts",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_SelectionId",
                table: "Posts",
                column: "SelectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_ClubId",
                table: "Events",
                column: "ClubId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_SelectionId",
                table: "Events",
                column: "SelectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Clubs_ClubId",
                table: "Events",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Selections_SelectionId",
                table: "Events",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Clubs_ClubId",
                table: "Posts",
                column: "ClubId",
                principalTable: "Clubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Posts_Selections_SelectionId",
                table: "Posts",
                column: "SelectionId",
                principalTable: "Selections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Clubs_ClubId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Events_Selections_SelectionId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Clubs_ClubId",
                table: "Posts");

            migrationBuilder.DropForeignKey(
                name: "FK_Posts_Selections_SelectionId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_ClubId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Posts_SelectionId",
                table: "Posts");

            migrationBuilder.DropIndex(
                name: "IX_Events_ClubId",
                table: "Events");

            migrationBuilder.DropIndex(
                name: "IX_Events_SelectionId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "ClubId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "SelectionId",
                table: "Posts");

            migrationBuilder.DropColumn(
                name: "ClubId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "SelectionId",
                table: "Events");
        }
    }
}
