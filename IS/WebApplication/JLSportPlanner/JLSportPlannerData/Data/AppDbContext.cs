﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using JLSportPlannerData.Models;

namespace JLSportPlannerData.Data
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
                : base(options) { }
        //public DbSet<ApplicationUser> AppUsers { get; set; }
        public DbSet<ApplicationUser> Users { get; set; }
        //public DbSet<ApplicationUserRole> UserRoles { get; set; }
        //public DbSet<ApplicationRole> Roles { get; set; }
        public DbSet<Sport> Sports { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Selection> Selections { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<JoinSelection> JoinSelections { get; set; }
        public DbSet<JoinClub> JoinClubs { get; set; }


        //public DbSet<ProductOrder> ProductOrders { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Specify the path of the database here
            optionsBuilder.UseSqlServer("Server=tcp:jlsportplanner20191229122400dbserver.database.windows.net,1433;Initial Catalog=JLSportPlanner20191229122400_db;Persist Security Info=False;User ID=zanjerko;Password=Test*12345;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

        }
    }
}

