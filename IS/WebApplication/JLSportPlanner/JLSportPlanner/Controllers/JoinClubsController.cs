﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JLSportPlannerData.Data;
using JLSportPlannerData.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace JLSportPlanner.Controllers
{
    public class JoinClubsController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public JoinClubsController(AppDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: JoinClubs

        public async Task<IActionResult> Index()
        {
            if(User.IsInRole("Admin")) 
            {
                    var appDbContext = _context.JoinClubs.Include(j => j.Club).Include(j => j.User);
                    return View(await appDbContext.ToListAsync());
            }
            else
            {
                //var usersClub = _context.JoinClubs.Include(j => j.Club).Where(j => j.UserId == "c6120da2-5f84-4318-a504-adb549082457");
                var appDbContext = _context.JoinClubs.Include(j => j.Club).Include(j => j.User);
             
                return View(await appDbContext.ToListAsync());

                /* var usersClub = _context.JoinClubs.Include(j => j.Club).Where(j => j.UserId == "c6120da2-5f84-4318-a504-adb549082457");
                 return View(await usersClub.ToListAsync());*/

            }
        }

        public async Task<IActionResult> MyClubs()
        {
            if (User.IsInRole("Admin"))
            {
                var appDbContext = _context.JoinClubs.Include(j => j.Club).Include(j => j.User);
                return View(await appDbContext.ToListAsync());
            }
            else
            {

                var userId = _userManager.GetUserId(HttpContext.User);
                var usersClub = _context.JoinClubs.Include(j => j.Club).Where(j => j.UserId == userId);
                 return View(await usersClub.ToListAsync());

            }
        }

        // GET: JoinClubs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinClub = await _context.JoinClubs
                .Include(j => j.Club)
                .Include(j => j.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (joinClub == null)
            {
                return NotFound();
            }

            return View(joinClub);
        }


        // GET: JoinClubs/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email");
            return View();
        }

        // POST: JoinClubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,ClubId,UserId")] JoinClub joinClub)
        {
            //joinClub.UserId = _userManager.GetUserId(HttpContext.User);
            if (ModelState.IsValid)
            {

                _context.Add(joinClub);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name",joinClub.ClubId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email", joinClub.UserId);
            return View(joinClub);
        }

        // GET: JoinClubs/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinClub = await _context.JoinClubs.FindAsync(id);
            if (joinClub == null)
            {
                return NotFound();
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name", joinClub.ClubId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email", joinClub.UserId);
            return View(joinClub);
        }

        // POST: JoinClubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ClubId,UserId,joinDate")] JoinClub joinClub)
        {
            if (id != joinClub.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(joinClub);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JoinClubExists(joinClub.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name", joinClub.ClubId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Email", joinClub.UserId);
            return View(joinClub);
        }

        // GET: JoinClubs/Delete/5
       //[Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinClub = await _context.JoinClubs
                .Include(j => j.Club)
                .Include(j => j.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (joinClub == null)
            {
                return NotFound();
            }

            return View(joinClub);
        }

        // POST: JoinClubs/Delete/5
       // [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var joinClub = await _context.JoinClubs.FindAsync(id);
            _context.JoinClubs.Remove(joinClub);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JoinClubExists(int id)
        {
            return _context.JoinClubs.Any(e => e.Id == id);
        }
    }
}
