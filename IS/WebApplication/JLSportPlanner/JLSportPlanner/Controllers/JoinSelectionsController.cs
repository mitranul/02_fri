﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JLSportPlannerData.Data;
using JLSportPlannerData.Models;
using Microsoft.AspNetCore.Authorization;

namespace JLSportPlanner.Controllers
{
    public class JoinSelectionsController : Controller
    {
        private readonly AppDbContext _context;

        public JoinSelectionsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: JoinSelections
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.JoinSelections.Include(j => j.Club).Include(j => j.Selection);
            return View(await appDbContext.ToListAsync());
        }

        // GET: JoinSelections/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinSelection = await _context.JoinSelections
                .Include(j => j.Club)
                .Include(j => j.Selection)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (joinSelection == null)
            {
                return NotFound();
            }

            return View(joinSelection);
        }

        // GET: JoinSelections/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name");
            ViewData["SelectionId"] = new SelectList(_context.Selections, "Id", "Name");
            return View();
        }

        // POST: JoinSelections/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,ClubId,SelectionId")] JoinSelection joinSelection)
        {
            if (ModelState.IsValid)
            {
                _context.Add(joinSelection);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name", joinSelection.ClubId);
            ViewData["SelectionId"] = new SelectList(_context.Selections, "Id", "Name", joinSelection.SelectionId);
            return View(joinSelection);
        }

        // GET: JoinSelections/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinSelection = await _context.JoinSelections.FindAsync(id);
            if (joinSelection == null)
            {
                return NotFound();
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name", joinSelection.ClubId);
            ViewData["SelectionId"] = new SelectList(_context.Selections, "Id", "Name", joinSelection.SelectionId);
            return View(joinSelection);
        }

        // POST: JoinSelections/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ClubId,SelectionId")] JoinSelection joinSelection)
        {
            if (id != joinSelection.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(joinSelection);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JoinSelectionExists(joinSelection.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClubId"] = new SelectList(_context.Clubs, "Id", "Name", joinSelection.ClubId);
            ViewData["SelectionId"] = new SelectList(_context.Selections, "Id", "Name", joinSelection.SelectionId);
            return View(joinSelection);
        }

        // GET: JoinSelections/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var joinSelection = await _context.JoinSelections
                .Include(j => j.Club)
                .Include(j => j.Selection)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (joinSelection == null)
            {
                return NotFound();
            }

            return View(joinSelection);
        }

        // POST: JoinSelections/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var joinSelection = await _context.JoinSelections.FindAsync(id);
            _context.JoinSelections.Remove(joinSelection);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JoinSelectionExists(int id)
        {
            return _context.JoinSelections.Any(e => e.Id == id);
        }
    }
}
