﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JLSportPlanner.Models;
using Microsoft.AspNetCore.Identity;
using JLSportPlannerData.Models;
using JLSportPlannerData.Data;

namespace JLSportPlanner.Controllers
{
    public class RoleUserController : Controller 
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IdentityUserRole<string> userRoleManager;
        private readonly ILogger<RoleUserController> _logger;

        public RoleUserController(ILogger<RoleUserController> logger)
        {

            //this.userRoleManager = userRoleManager;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            /*var roles = roleManager.Roles;
            var users = userManager.Users;*/
           /*var role = userRoleManager.RoleId;
           var user = userRoleManager.UserId;
            var userRole = new IdentityUserRole<string>();
            userRole.RoleId = role;
            userRole.UserId = user;*/
      
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Create(IdentityUserRole<string> roleUserModel)
        {
            if (ModelState.IsValid)
            {
                /*IdentityUserRole<string> identityUserRole = new IdentityUserRole<string>
                {
                    Name = roleModel.Name
                };*/
                var userRole = new IdentityUserRole<string>
                {
                    UserId = "2db9d476-a1fa-4247-84c5-c65e596544e1",
                    RoleId = "2db9d476-a1fa-4247-84c5-c65e596544e1"
                };


                //IdentityResult result = await userRoleManager.CreateAsync(userRole);
                /* if (result.Succeeded)
                 {
                     return RedirectToAction("Index", "Dashboard");
                 }

                 foreach (IdentityError err in result.Errors)
                 {
                     ModelState.AddModelError("", err.Description);
                 }*/
            }

            //return View(roleModel);
            return View();
        }



    }
}
