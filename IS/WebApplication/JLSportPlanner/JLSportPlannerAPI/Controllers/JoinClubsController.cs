﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JLSportPlannerData.Data;
using JLSportPlannerData.Models;
using JLSportPlannerAPI.Filters;

namespace JLSportPlannerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[ApiKeyAuth]
    public class JoinClubsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public JoinClubsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/JoinClubs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<JoinClub>>> GetJoinClubs()
        {
            return await _context.JoinClubs.ToListAsync();
        }

        // GET: api/JoinClubs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<JoinClub>> GetJoinClub(int id)
        {
            var joinClub = await _context.JoinClubs.FindAsync(id);

            if (joinClub == null)
            {
                return NotFound();
            }

            return joinClub;
        }

        // PUT: api/JoinClubs/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJoinClub(int id, JoinClub joinClub)
        {
            if (id != joinClub.Id)
            {
                return BadRequest();
            }

            _context.Entry(joinClub).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JoinClubExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JoinClubs
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<JoinClub>> PostJoinClub(JoinClub joinClub)
        {
            _context.JoinClubs.Add(joinClub);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJoinClub", new { id = joinClub.Id }, joinClub);
        }

        // DELETE: api/JoinClubs/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<JoinClub>> DeleteJoinClub(int id)
        {
            var joinClub = await _context.JoinClubs.FindAsync(id);
            if (joinClub == null)
            {
                return NotFound();
            }

            _context.JoinClubs.Remove(joinClub);
            await _context.SaveChangesAsync();

            return joinClub;
        }

        private bool JoinClubExists(int id)
        {
            return _context.JoinClubs.Any(e => e.Id == id);
        }
    }
}
