﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JLSportPlannerData.Data;
using JLSportPlannerData.Models;
using JLSportPlannerAPI.Filters;

namespace JLSportPlannerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiKeyAuth]
    public class SelectionsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public SelectionsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Selections
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Selection>>> GetSelections()
        {
            return await _context.Selections.ToListAsync();
        }

        // GET: api/Selections/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Selection>> GetSelection(int id)
        {
            var selection = await _context.Selections.FindAsync(id);

            if (selection == null)
            {
                return NotFound();
            }

            return selection;
        }

        // PUT: api/Selections/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSelection(int id, Selection selection)
        {
            if (id != selection.Id)
            {
                return BadRequest();
            }

            _context.Entry(selection).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SelectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Selections
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Selection>> PostSelection(Selection selection)
        {
            _context.Selections.Add(selection);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSelection", new { id = selection.Id }, selection);
        }

        // DELETE: api/Selections/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Selection>> DeleteSelection(int id)
        {
            var selection = await _context.Selections.FindAsync(id);
            if (selection == null)
            {
                return NotFound();
            }

            _context.Selections.Remove(selection);
            await _context.SaveChangesAsync();

            return selection;
        }

        private bool SelectionExists(int id)
        {
            return _context.Selections.Any(e => e.Id == id);
        }
    }
}
