﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JLSportPlannerData.Data;
using JLSportPlannerData.Models;
using JLSportPlannerAPI.Filters;

namespace JLSportPlannerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[ApiKeyAuth]
    public class JoinSelectionsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public JoinSelectionsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/JoinSelections
        [HttpGet]
        public async Task<ActionResult<IEnumerable<JoinSelection>>> GetJoinSelections()
        {
            return await _context.JoinSelections.ToListAsync();
        }

        // GET: api/JoinSelections/5
        [HttpGet("{id}")]
        public async Task<ActionResult<JoinSelection>> GetJoinSelection(int id)
        {
            var joinSelection = await _context.JoinSelections.FindAsync(id);

            if (joinSelection == null)
            {
                return NotFound();
            }

            return joinSelection;
        }

        // PUT: api/JoinSelections/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutJoinSelection(int id, JoinSelection joinSelection)
        {
            if (id != joinSelection.Id)
            {
                return BadRequest();
            }

            _context.Entry(joinSelection).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JoinSelectionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/JoinSelections
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<JoinSelection>> PostJoinSelection(JoinSelection joinSelection)
        {
            _context.JoinSelections.Add(joinSelection);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetJoinSelection", new { id = joinSelection.Id }, joinSelection);
        }

        // DELETE: api/JoinSelections/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<JoinSelection>> DeleteJoinSelection(int id)
        {
            var joinSelection = await _context.JoinSelections.FindAsync(id);
            if (joinSelection == null)
            {
                return NotFound();
            }

            _context.JoinSelections.Remove(joinSelection);
            await _context.SaveChangesAsync();

            return joinSelection;
        }

        private bool JoinSelectionExists(int id)
        {
            return _context.JoinSelections.Any(e => e.Id == id);
        }
    }
}
