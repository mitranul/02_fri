package com.example.jlsportplanner;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private RequestQueue requestQueue;
    private TextView osebe;
    public static final String urlClubAPI = "https://jlsportplannerapi20191229123755.azurewebsites.net/api/Clubs";
    public static final String EXTRA_MESSAGE = "com.example.androidcontoso.MESSAGE";

    public void addClub(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, AddClub.class);
        String message = "Dodaj klub v tabelo.";
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        osebe = (TextView) findViewById(R.id.osebe);
    }

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject object = response.getJSONObject(i);
                    /*String name = object.getString("first Name");
                    String priimek = object.getString("lastName");
                    String id = object.getString("id");
                    String mesto = object.getString("address");
                    String naslov = object.getString("email");
                    data.add(name + " "  + priimek + " " + naslov + " " + mesto);*/
                    String clubId = object.getString("id");
                    String name = object.getString("name");
                    String email = object.getString("email");
                    data.add(clubId + "  " + name + " "  + email);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }
            osebe.setText("");
            osebe.setMovementMethod(new ScrollingMovementMethod());
            for (String row: data) {
                String currentText = osebe.getText().toString();
                osebe.setText(currentText + "\n\n" + row);
            }
        }
    };


    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("REST error", error.getMessage());
        }
    };

    public void clearHits(View view) {
        if(view != null) {
            osebe.setText("Clubs");
        }
    }

    public void showClubs(View view) {
        if (view != null) {
            JsonArrayRequest request = new JsonArrayRequest(urlClubAPI, jsonArrayListener, errorListener){
                @Override
                public Map<String,String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("ApiKey","SecretKey");
                    return params;
                }
            };
            requestQueue.add(request);
        }
    }

}
