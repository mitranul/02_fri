package com.example.jlsportplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddClub extends AppCompatActivity {
    private final String urlSportAPI = "https://jlsportplannerapi20191229123755.azurewebsites.net/api/Sports";
    private RequestQueue requestQueue;
    private TextView status;
    private EditText imeKluba;
    private EditText naslovKluba;
    private EditText emailKluba;
    private String sportId;
    private Spinner betterSpinner;
    private Map<String, String> listOfSelectedSports = new HashMap<String, String>();
    private ArrayList<String> spinnerList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_club);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        status = (TextView) findViewById(R.id.status);
        //status.setText(message);
        imeKluba = (EditText) findViewById(R.id.editText13);
        naslovKluba = (EditText) findViewById(R.id.editText12);
        emailKluba = (EditText) findViewById(R.id.editText11);

        generateSpinnerTable();

        ArrayAdapter<String> myArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, spinnerList);
        betterSpinner = (Spinner) findViewById(R.id.spinner1);
        betterSpinner.setAdapter(myArrayAdapter);

        betterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                if(parent.getItemAtPosition(position).equals("Select sport")) {

                }
                else {
                    sportId = parent.getItemAtPosition(position).toString();
                    Toast.makeText(parent.getContext(), "Selected: " + sportId, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public int checkTheRightSport() {
        return Integer.parseInt(listOfSelectedSports.get(sportId));
    }

    public void addClub(View view) {
        //this.status.setText("Dodajam v " + MainActivity.urlClubAPI);
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("name", imeKluba.getText());
            jsonBody.put("address", naslovKluba.getText());
            jsonBody.put("email", emailKluba.getText());
            jsonBody.put("sportId", checkTheRightSport());
            jsonBody.put("sport", null);
            jsonBody.put("joinClub", null);
            jsonBody.put("joinSelection", null);
            final String mRequestBody = jsonBody.toString();
            //status.setText(mRequestBody);
            final StringRequest stringRequest = new StringRequest(Request.Method.POST, MainActivity.urlClubAPI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i("LOG_VOLLEY", response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("LOG_VOLLEY", error.toString());
                }
            }) {
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String,String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json; charset=utf-8");
                    params.put("ApiKey","SecretKey");
                    return params;
                }

                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    String responseString = "";
                    if (response != null) {
                        responseString = String.valueOf(response.statusCode);
                        //status.setText(responseString);
                        //status.setText("Club added successfully");
                        status.setBackgroundResource(R.color.green);
                    }
                    else {
                        status.setBackgroundResource(R.color.red);
                    }
                    return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void goBack(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, MainActivity.class);
        String message = "Dodaj klub v tabelo.";
        intent.putExtra(MainActivity.EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    private Response.Listener<JSONArray> jsonArrayListener = new Response.Listener<JSONArray>() {
        @Override
        public void onResponse(JSONArray response) {
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject object = response.getJSONObject(i);
                    String sportId = object.getString("id");
                    String sportName = object.getString("name");

                    listOfSelectedSports.put(sportName, sportId);
                    spinnerList.add(sportName);
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("REST error", error.getMessage());
        }
    };

    public void generateSpinnerTable() {
        JsonArrayRequest request = new JsonArrayRequest(urlSportAPI, jsonArrayListener, errorListener){
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ApiKey","SecretKey");
                return params;
            }
        };
        requestQueue.add(request);
        spinnerList.add("Select sport");
    }
}
