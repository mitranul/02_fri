package com.example.flywithus;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA = "somethingNew";
    public static final String TABELA_KRAJEV = "tabela.vnesenih.krajev", STEVILO_POTNIKOV = "stevilo.potnikov", VRSTA_POVEZAVE = "vrsta.povezave";
    private String povezava;
    public static final String[] tabelaKrajev = {"", ""};
    private final int REQUEST = 1;
    public static float cenaRazreda;
    public static String[] krajOdhoda = {"Brnik (SLO)", "Marco Polo (ITA)", "Bergamo (ITA)", "Bologna (ITA)", "Zagreb (CRO)", "Vienna (AUT)", "Trieste (ITA)"};
    public static String[] krajPrihoda = {"Valencia (ESP)", "Valleta (MAL)", "Moscow (RUS)", "Bern (SWI)", "Porto (POR)", "Oslo (NOR)", "Hurgada (EGP)"};
    public static float[] cenaDestinacije = {100, 95, 120, 65, 88, 90, 200};
    public static List<String> mobileArray = new ArrayList<>();
    public Spinner destSpinner;
    public static ListView listView;
    public static ArrayAdapter<String> adapt;
    private CheckBox povratniLet;
    private EditText datumOdhoda, datumPrihoda, stPotnikovField;
    private TextView statusnaVrstica;
    private LocalDateClass enteredDepart, enteredCome, localTime;
    private Spinner krajOdhodaSpinner, krajPrihodaSpinner;
    public static Spinner razredLeta;

    private void fillCitySpinner(View spinId, String[] listKrajev) {
        List<String> spinDestinations = new ArrayList<>();
        for(String destination : listKrajev) spinDestinations.add(destination);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinDestinations);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        destSpinner = (Spinner)spinId;
        destSpinner.setAdapter(adapter);
    }

    private void fillPassengerList() {
        adapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mobileArray);
        listView.setAdapter(adapt);
    }

    public void cancelClicked(View view) {
        System.exit(0);
    }

    public void addPassengerClicked(View view) {
        if(stPotnikovField.getText().toString().length() == 0 || Integer.parseInt(stPotnikovField.getText().toString()) == 0)
            SetStatus.setStatus(statusnaVrstica, stPotnikovField, "Da vnesete podatke o potnikih, morate prej podati njihovo število.", SetStatus.errorColor);
        else {
            statusnaVrstica.setVisibility(View.GONE);
            Intent intent = new Intent(this, PassengerActivity.class);
            intent.putExtra(EXTRA, Integer.parseInt(stPotnikovField.getText().toString()));
            startActivityForResult(intent, REQUEST);
        }
    }

    private boolean checkInputLength(EditText component) {
        if(component.getText().toString().length() == 0) {
            SetStatus.setStatus(statusnaVrstica, component, "Zahtevano polje mora biti izpolnjneno. Prosim vnesite podatke", SetStatus.errorColor);
            return true;
        }
        return false;
    }

    @SuppressLint("DefaultLocale")
    private boolean checkDatePicker(EditText enteredDate, boolean depart) {
        if(enteredDate.getText().toString().length() == 0) {
            SetStatus.setStatus(statusnaVrstica, enteredDate, "Zahtevano polje mora biti izpolnjneno. Prosim vnesite datum", SetStatus.errorColor);
            return true;
        }
        LocalDateClass enteredTime = new LocalDateClass(enteredDate.getText().toString());
        if((enteredTime.enteredDay < localTime.localDan && enteredTime.enteredMonth <= localTime.localMesec && enteredTime.enteredYear == localTime.localLeto) ||
                enteredTime.enteredDay > localTime.localDan && enteredTime.enteredMonth < localTime.localMesec && enteredTime.enteredYear == localTime.localLeto) {
            SetStatus.setStatus(statusnaVrstica, enteredDate, String.format("Vnešeni datum je pretekel. Prosim vnesite datum od %d.%d.%d", localTime.localDan, localTime.localMesec, localTime.localLeto), SetStatus.errorColor);
            return true;
        }
        if(depart) enteredDepart = enteredTime;
        else enteredCome = enteredTime;
        return false;
    }

    private boolean checkNumberOfPassengers() {
        int steviloPotnikov = Integer.parseInt(stPotnikovField.getText().toString());
        if(steviloPotnikov == 0) {
            SetStatus.setStatus(statusnaVrstica, stPotnikovField, "Vnesti morate vsaj enega potnika.", SetStatus.errorColor);
            return true;
        }
        else if(steviloPotnikov > mobileArray.size()) {
            SetStatus.setStatus(statusnaVrstica, stPotnikovField, "Na seznamu je premalo potnikov. Prosim dodajte potnike.", SetStatus.errorColor);
            return true;
        }
        return false;
    }

    private boolean checkAllIpnutData() {
        if(checkInputLength(datumOdhoda) || checkDatePicker(datumOdhoda, true)) { return false; }
        if(povratniLet.isChecked()) {
            if(checkInputLength(datumPrihoda) || checkDatePicker(datumPrihoda, false)) { return false; }
            if((enteredDepart.enteredDay > enteredCome.enteredDay && enteredDepart.enteredMonth == enteredCome.enteredMonth && enteredDepart.enteredYear == enteredCome.enteredYear) ||
                    (enteredDepart.enteredMonth < enteredCome.enteredMonth && enteredDepart.enteredYear == enteredCome.enteredYear)) {
                SetStatus.setStatus(statusnaVrstica, datumPrihoda, "Vnešeni datum vrnitve je neustrezen. Prosim popravite datum.", SetStatus.errorColor);
                return false;
            } else if(enteredDepart.enteredYear > enteredCome.enteredYear) {
                SetStatus.setStatus(statusnaVrstica, datumPrihoda, "Vnešeno leto vrnitve je neustrezno. Prosim popravite datum.", SetStatus.errorColor);
                return false;
            }
        }
        if(checkInputLength(stPotnikovField) || checkNumberOfPassengers()) { return false; }
        return true;
    }

    public void savePurchase(View view) {
        if(checkAllIpnutData()) {
            SetStatus.setStatus(statusnaVrstica, null, "Datum je ustrezno shranjen. ", SetStatus.successColor);
            tabelaKrajev[0] = krajOdhodaSpinner.getSelectedItem().toString();
            tabelaKrajev[1] = krajPrihodaSpinner.getSelectedItem().toString();
            Intent intent = new Intent(this, PurchaseActivity.class);
            intent.putExtra(TABELA_KRAJEV, tabelaKrajev);
            intent.putExtra(STEVILO_POTNIKOV, Integer.parseInt(stPotnikovField.getText().toString()));
            if(povratniLet.isChecked()) povezava = "Dvosmerna";
            else povezava = "Enosmerna";
            intent.putExtra(VRSTA_POVEZAVE, povezava);

            switch(razredLeta.getSelectedItem().toString()) {
                case "Ekonomski": cenaRazreda = 20; break;
                case "Poslovni": cenaRazreda = 80; break;
                case "Prvi": cenaRazreda =  120;break;
            }
            startActivityForResult(intent, REQUEST);
        }
    }

    private void datumOdhodaPicker() {
        datumOdhoda.setInputType(InputType.TYPE_NULL);
        datumOdhoda.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    final int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String dayOM = dayOfMonth+"";
                            String monTh = (monthOfYear+1)+"";
                            if(dayOfMonth < 10) dayOM = "0"+dayOfMonth;
                            if((monthOfYear+1) < 10) monTh = "0"+(monthOfYear+1);
                            datumOdhoda.setText(dayOM + "." + monTh + "." + year);
                        }
                    }, year, month, day).show();
                }
            }
        });
    }

    private void datumPrihodaPicker() {
        datumPrihoda.setInputType(InputType.TYPE_NULL);
        datumPrihoda.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String dayOM = dayOfMonth+"";
                            String monTh = (monthOfYear+1)+"";
                            if(dayOfMonth < 10) dayOM = "0"+dayOfMonth;
                            if((monthOfYear+1) < 10) monTh = "0"+(monthOfYear+1);
                            datumPrihoda.setText(dayOM + "." + monTh + "." + year);
                        }
                    }, year, month, day).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        krajOdhodaSpinner = (Spinner) findViewById(R.id.krajOdhodaSpinner);
        krajPrihodaSpinner = (Spinner) findViewById(R.id.krajPrihodaSpinner);
        fillCitySpinner(findViewById(R.id.krajOdhodaSpinner), krajOdhoda);
        fillCitySpinner(findViewById(R.id.krajPrihodaSpinner), krajPrihoda);
        listView = (ListView) findViewById(R.id.passenger_list);
        fillPassengerList();
        datumOdhoda = (EditText) findViewById(R.id.datumOdhoda);
        datumPrihoda = (EditText) findViewById(R.id.datumPrihoda);
        datumOdhodaPicker();
        datumPrihodaPicker();
        statusnaVrstica = (TextView) findViewById(R.id.statusnaVrstica);
        stPotnikovField = (EditText) findViewById(R.id.steviloPotnikov);
        localTime = new LocalDateClass();
        povratniLet = (CheckBox) findViewById(R.id.povratniLet);
        razredLeta = (Spinner) findViewById(R.id.razredLetaSpinner);

        ((CheckBox)findViewById(R.id.povratniLet)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(!((CompoundButton) view).isChecked()){
                ((EditText)findViewById(R.id.datumPrihoda)).setVisibility(View.GONE);
            } else {
                ((EditText)findViewById(R.id.datumPrihoda)).setVisibility(View.VISIBLE);
            }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCall, int resultCall, Intent data) {
        super.onActivityResult(requestCall, resultCall, data);
        if (requestCall == REQUEST && resultCall == RESULT_OK) {
            boolean preveri = data.getBooleanExtra(PurchaseActivity.RESULT_PURCHASE, false);
            if(preveri) {
                SetStatus.setStatus(statusnaVrstica, null, "Nakup je bil uspešen.", SetStatus.successColor);
                datumOdhoda.setText("");
                datumPrihoda.setText("");
                mobileArray = new ArrayList<>();
                stPotnikovField.setText("");
                povratniLet.setChecked(false);
                fillPassengerList();
            }
            else SetStatus.setStatus(statusnaVrstica, null, "Nakup ni bil uspešen.", SetStatus.errorColor);
        }
    }
}
