package com.example.flywithus;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class PassengerActivity extends AppCompatActivity {
    private LocalDateClass localTime, enteredTime;
    private EditText datumRojstva, imePotnika, priimekPotnika, emailPotnika;
    private Spinner spolPotnika;
    private TextView statusnaVrstica;
    private int mapItemId = 0, steviloPotnikov;
    private List<EditText> editorList;
    public static HashMap<Integer, Potnik> slovarPotnikov = new HashMap<>();
    public static List<Integer> starostPotnikov;

    private boolean checkNumberOfPassengers() {
        if(slovarPotnikov.size() == steviloPotnikov) {
            SetStatus.setStatus(statusnaVrstica, null, "Vnesli ste toliko potnikov, kolikor število je podano.", SetStatus.successColor);
            ((Button) findViewById(R.id.dodajPotnikaButton)).setVisibility(View.GONE);
            return true;
        }
        return false;
    }

    private boolean checkDatePicker() {
        enteredTime = new LocalDateClass(datumRojstva.getText().toString());
        if((enteredTime.enteredDay > localTime.localDan && enteredTime.enteredMonth >= localTime.localMesec && enteredTime.enteredYear == localTime.localLeto) ||
                (enteredTime.enteredDay <= localTime.localDan && enteredTime.enteredMonth > localTime.localMesec && enteredTime.enteredYear == localTime.localLeto)) {
            SetStatus.setStatus(statusnaVrstica, datumRojstva, "Vnešeni datum rojstva je neustrezen. Prosim vnesite ponovno.", SetStatus.errorColor);
            return true;
        }
        int razlikaLeto = Math.abs(localTime.localLeto - enteredTime.enteredYear);
        if(razlikaLeto <= 2) starostPotnikov.add(2);
        else if(razlikaLeto <= 12) starostPotnikov.add(12);
        else starostPotnikov.add(razlikaLeto);
        return false;
    }

    private boolean checkAllInputData() {
        for(EditText component : editorList) {
            if(component.length() < 3) {
                SetStatus.setStatus(statusnaVrstica, component, "Zahtevano polje vsebuje premalo znakov. Vnesite vsaj 3 znake", SetStatus.errorColor);
                return false;
            }
            else if(component.length() > 45) {
                SetStatus.setStatus(statusnaVrstica, component, "Zahtevano polje vsebuje preveč znakov. Vnesete lahko največ 45 znakov", SetStatus.errorColor);
                return false;
            }
            else if(emailPotnika.getId() == component.getId() && !component.getText().toString().contains("@")) {
                SetStatus.setStatus(statusnaVrstica, component, "Email je zapisan v napačnem formatu. Vsebovati mora znak '@'", SetStatus.errorColor);
                return false;
            }
        }
        if(checkDatePicker()) { return false; }
        return true;
    }

    private void addNewPassenger() {
        Potnik potnik = new Potnik();
        potnik.setIme(imePotnika.getText().toString());
        potnik.setPriimek(priimekPotnika.getText().toString());
        potnik.setSpol(spolPotnika.getSelectedItem().toString());
        potnik.setDatumRojstva(datumRojstva.getText().toString());
        potnik.setEmail(emailPotnika.getText().toString());
        slovarPotnikov.put(mapItemId++, potnik);
    }

    public void saveAllPassengers(View view) {
        if(checkAllInputData()) {
            addNewPassenger();
            if(!checkNumberOfPassengers()) SetStatus.setStatus(statusnaVrstica, null, "Vnešeni podatki so bili uspešno shranjeni.", SetStatus.successColor);
            clearAllInputs();
        }
    }

    public void backToMainActivity(View view) {
        for(Potnik potnik : slovarPotnikov.values()) {
            String podatkiPotnika = String.format("%s %s, %s, %s", potnik.getIme(), potnik.getPriimek(), potnik.getDatumRojstva(), potnik.getEmail());
            MainActivity.mobileArray.add(podatkiPotnika);
        }
        MainActivity.listView.setAdapter(MainActivity.adapt);
        finish();
    }

    private void clearAllInputs() {
        imePotnika.setText("");
        priimekPotnika.setText("");
        emailPotnika.setText("");
        datumRojstva.setText("");
        imePotnika.requestFocus();
    }

    private void datumRojstvaPotnika() {
        datumRojstva.setInputType(InputType.TYPE_NULL);
        datumRojstva.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    // date picker dialog
                    new DatePickerDialog(PassengerActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            String dayOM = dayOfMonth+"";
                            String monTh = (monthOfYear+1)+"";
                            if(dayOfMonth < 10) dayOM = "0"+dayOfMonth;
                            if((monthOfYear+1) < 10) monTh = "0"+(monthOfYear+1);
                            datumRojstva.setText(dayOM + "." + monTh + "." + year);
                        }
                    }, year, month, day).show();

                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger);
        mapItemId = 0;
        Intent intent = getIntent();
        steviloPotnikov = intent.getIntExtra(MainActivity.EXTRA, 0);
        slovarPotnikov = new HashMap<>();
        datumRojstva = (EditText) findViewById(R.id.datumRojstva);
        datumRojstvaPotnika();

        statusnaVrstica = (TextView) findViewById(R.id.statusVrstica);
        spolPotnika = (Spinner) findViewById(R.id.spolPotnikaSpinner);
        imePotnika = (EditText) findViewById(R.id.imePotnika);
        priimekPotnika = (EditText) findViewById(R.id.priimekPotnika);
        emailPotnika = (EditText) findViewById(R.id.emailPotnika);
        editorList = new ArrayList<>(Arrays.asList(imePotnika, priimekPotnika, emailPotnika, datumRojstva));
        starostPotnikov = new ArrayList<>();
        localTime = new LocalDateClass();
    }
}
