package com.example.flywithus;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.nio.FloatBuffer;
import java.nio.charset.MalformedInputException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PurchaseActivity extends AppCompatActivity {
    private String[] tabelaKrajev;
    private String vrstaPovezave;
    public static final String RESULT_PURCHASE = "purchase.done";
    private boolean successPurchase = false;
    private int steviloPotnikov;
    private float koncnaCenaKart;
    private TextView destinacijaF, stPotnikovF, cenaKarteF, povezavaF, statusnaVrstica, koncnaCenaField, razredLetaV;
    private List<TextView> listViews;
    private EditText imeField, priimekField, kartica1, kartica2, kartica3, kartica4, karticaCVV;
    private List<EditText> listEdits;
    private HashMap<String, Float> cenaZaDestinacijo = new HashMap<>();

    private void fillDestinationPrice() {
        for(int i = 0; i < MainActivity.krajPrihoda.length; i++)
            cenaZaDestinacijo.put(MainActivity.krajPrihoda[i], MainActivity.cenaDestinacije[i]);
    }

    @SuppressLint("DefaultLocale")
    private void setFinalPrice() {
        float oneTicketPrice = cenaZaDestinacijo.get(tabelaKrajev[1]);
        if(vrstaPovezave.equals("Dvosmerna")) oneTicketPrice *= 2;
        cenaKarteF.setText(String.format("%.2f EUR", oneTicketPrice));
        for(Integer starost : PassengerActivity.starostPotnikov) {
            if(starost == 2) koncnaCenaKart += 0;
            else if(starost == 12) {
                koncnaCenaKart += oneTicketPrice / 2;
                koncnaCenaKart += MainActivity.cenaRazreda;
            }
            else {
                koncnaCenaKart += oneTicketPrice;
                koncnaCenaKart += MainActivity.cenaRazreda;
            }
        }
        koncnaCenaField.setText(String.format("%.2f EUR", koncnaCenaKart));
    }

    @SuppressLint("DefaultLocale")
    private boolean checkTextFieldLength(EditText component, int minLen, int maxLen) {
        if(component.length() < minLen) {
            SetStatus.setStatus(statusnaVrstica, component, String.format("Zahtevano polje vsebuje premalo znakov. Vnesite vsaj %d znake", minLen), SetStatus.errorColor);
            return true;
        }
        else if(component.length() > maxLen) {
            SetStatus.setStatus(statusnaVrstica, component, String.format("Zahtevano polje vsebuje preveč znakov. Vnesete lahko največ %d znakov", maxLen), SetStatus.errorColor);
            return true;
        }
        return false;
    }

    private boolean checkAllInputData() {
        if(checkTextFieldLength(imeField, 3, 40)) { return false; }
        else if(checkTextFieldLength(priimekField, 3, 40)) { return false; }
        else if(checkTextFieldLength(kartica1, 4, 4) || checkTextFieldLength(kartica2, 4, 4) || checkTextFieldLength(kartica3, 4, 4)
                || checkTextFieldLength(kartica4, 4, 4) || checkTextFieldLength(karticaCVV, 3, 3)) { return false; }
        return true;
    }

    public void savePurchase(View view) {
        if(checkAllInputData()) {
            SetStatus.setStatus(statusnaVrstica, null, "Nakup je bil uspešno izveden. Srečno potovanje!", SetStatus.successColor);
            clearAllInputs();
            Intent result = new Intent();
            result.putExtra(RESULT_PURCHASE, true);
            setResult(RESULT_OK, result);
            finish();
        }
    }

    public void cancelClicked(View view) {
        finish();
    }

    private void clearAllInputs() {
        for(TextView component : listViews)
            component.setText("N/A");
        for(EditText component : listEdits)
            component.setText("");
    }

    private void setIntentVariables() {
        Intent intent = getIntent();
        tabelaKrajev = intent.getStringArrayExtra(MainActivity.TABELA_KRAJEV);
        vrstaPovezave = intent.getStringExtra(MainActivity.VRSTA_POVEZAVE);
        steviloPotnikov = intent.getIntExtra(MainActivity.STEVILO_POTNIKOV, 0);
    }

    @SuppressLint("DefaultLocale")
    private void setTextViewString() {
        destinacijaF.setText(String.format("%s -> %s", tabelaKrajev[0], tabelaKrajev[1]));
        stPotnikovF.setText(steviloPotnikov + "");
        povezavaF.setText(vrstaPovezave);
        razredLetaV.setText(String.format("%s razred (+%.2f EUR)", MainActivity.razredLeta.getSelectedItem().toString(), MainActivity.cenaRazreda));
    }

    private void setAllComponnetId() {
        destinacijaF = (TextView) findViewById(R.id.destinacijaField);
        stPotnikovF= (TextView) findViewById(R.id.stPotField);
        cenaKarteF = (TextView) findViewById(R.id.cenaKarteField);
        povezavaF = (TextView) findViewById(R.id.vrstaPovezaveField);
        statusnaVrstica = (TextView) findViewById(R.id.statVrstica);
        imeField = (EditText) findViewById(R.id.vnosnoPoljeIme);
        priimekField = (EditText) findViewById(R.id.vnosnoPoljePriimek);
        kartica1 = (EditText) findViewById(R.id.kartica1);
        kartica2 = (EditText) findViewById(R.id.kartica2);
        kartica3 = (EditText) findViewById(R.id.kartica3);
        kartica4 = (EditText) findViewById(R.id.kartica4);
        karticaCVV = (EditText) findViewById(R.id.karticaCVV);
        koncnaCenaField = (TextView) findViewById(R.id.skupnaCenaField);
        razredLetaV = (TextView) findViewById(R.id.razredLetaView);
        listViews = new ArrayList<>(Arrays.asList(destinacijaF, stPotnikovF, cenaKarteF, povezavaF, koncnaCenaField, razredLetaV));
        listEdits = new ArrayList<>(Arrays.asList(imeField, priimekField, kartica1, kartica2, kartica3, kartica4, karticaCVV));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        koncnaCenaKart = 0;
        setAllComponnetId();
        setIntentVariables();
        setTextViewString();
        fillDestinationPrice();
        setFinalPrice();
    }
}
