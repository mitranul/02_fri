package com.example.flywithus;

public class Potnik {
    private String ime;
    private String priimek;
    private String spol;
    private String datumRojstva;
    private String email;

    Potnik() {}

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPriimek(String priimek) {
        this.priimek = priimek;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public void setDatumRojstva(String datumRojstva) {
        this.datumRojstva = datumRojstva;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIme() {
        return ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public String getSpol() {
        return spol;
    }

    public String getDatumRojstva() {
        return datumRojstva;
    }

    public String getEmail() {
        return email;
    }
}
