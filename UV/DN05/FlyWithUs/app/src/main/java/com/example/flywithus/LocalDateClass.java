package com.example.flywithus;

import android.os.Build;
import java.time.LocalDate;
import androidx.annotation.RequiresApi;

public class LocalDateClass {
    private String localDate;
    public int localDan, localMesec, localLeto;
    public int enteredDay, enteredMonth, enteredYear;

    @RequiresApi(api = Build.VERSION_CODES.O)
    LocalDateClass() {
        localDate = LocalDate.now().toString();
        localDan = Integer.parseInt(localDate.substring(8, 10));
        localMesec = Integer.parseInt(localDate.substring(5, 7));
        localLeto = Integer.parseInt(localDate.substring(0, 4));
    }

    LocalDateClass(String enteredDate) {
        this.enteredDay = Integer.parseInt(enteredDate.substring(0, 2));
        this.enteredMonth = Integer.parseInt(enteredDate.substring(3, 5));
        this.enteredYear = Integer.parseInt(enteredDate.substring(6, 10));
    }
}
