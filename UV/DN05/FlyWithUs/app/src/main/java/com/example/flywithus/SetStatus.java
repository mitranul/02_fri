package com.example.flywithus;

import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SetStatus {
    public static String errorColor = "#ff0000";
    public static String successColor = "#6de171";

    SetStatus(){}

    public static void setStatus(TextView statusnaVrstica, EditText component, String statusMessage, String color) {
        if(component != null)
            component.requestFocus();
        statusnaVrstica.setText(statusMessage);
        statusnaVrstica.setVisibility(View.VISIBLE);
        statusnaVrstica.setBackgroundColor(Color.parseColor(color));
    }

}
