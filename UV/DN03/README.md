### Navodila za 3. Domačo nalogo

Pri zavarovalnici N'č bat so se odločili, da bodo zamenjali dosedanjo aplikacijo, s katero so zajemali podatke za posamezna avtomobilska zavarovanja. Pri pisanju aplikacije so se zanesli na lastne moči in tako je nastala aplikacija, ki jo vidite na spodnji sliki. Na žalost pa so se uporabniki (zavarovalniški agenti) pritožili, da vmesnik aplikacije ni primeren za delo. Načrtovalci vmesnika namreč niso upoštevali principov in navodil načrtovanja uporabniških vmesnikov. Zato so na pomoč poklicali izkušene računalničarje (vas), ki jim bodo pomagali na novo načrtati in implementirati aplikacijo, tako da bo primerna za uporabo.

Vaša naloga je, da na novo načrtujete in implementirate aplikacijo, ki je prikazana spodaj. Vaša aplikacija mora nuditi možnost vnosa vseh podatkov, kot jih nudi tudi prejšnja aplikacija. Nov vmesnik naj bo načrtovan v skladu z desetimi Nielsen-ovimi principi in navodili načrtovanja uporabniških vmesnikov. Tako bo morda potrebno posamezne podobe oziroma gradnike aplikacije zamenjati z drugimi, saj nekateri obstoječi gradniki niso primerno izbrani. Pri izbiranju upoštevajte navodila in pravila izbora grafičnih gradnikov za interakcijo glede na razmere. Gradnike vmesnika tudi pravilno aranžirajte. Pri aranžiranju gradnikov upoštevajte navodila aranžiranja grafičnih gradnikov za interakcijo: tehnike za dosego preprostosti, balansiranje, grupiranje (beli presledki, okvirji), orientacija in poravnavanje. Vmesnik tudi pravilno grafično načrtujte. V skladu z navodili načrtovanja vmesnikov pravilno izbirajte tekste in barve.

Vaša aplikacija naj omogoča tudi ponastavitev vnesenih vrednosti na začetne vrednosti ("Ponastavi vnose") in zapis vnesenih podatkov v podatkovno bazo ("Shrani vnos"), ki pa naj bo kar tekstovna datoteka. Vmesnik naj tudi preverja, če so bili vneseni vsi podatki. Če niso bili, naj vmesnik to sporoči uporabniku. Prav tako naj vmesnik tudi testira pravilnost podatkov (npr. obstoj določenega datuma) in pravilnost vnosa (npr. poštna številka - poštna številka abc123 ne obstaja). Po želji lahko vaš vmesnik tudi dopolnite. 

Oddanemu projektu priložite tekstovno datoteko, v katero napišite, kako ste zadostili vsakemu izmed 10 Nielsenovih principov.


#### Slika vmesnika ####
![alt text](https://ucilnica.fri.uni-lj.si/pluginfile.php/108545/mod_assign/intro/SlikaNaloga3-5.jpg)