﻿Zato, da je TFTP strežnik začel servirati operacijski sistem, sem moram najprej inštalirati in konfigurirati NFS strežnik

### Nastavitev NFS strežnika ###

S spodnjim ukazom, sem najprej naložil pakete NFS strežnika
UKAZ: apt install nfs-kernel-server

Nato sem v mapo /etc/exports dodal spodnji zapis:
	/media/cdrom *(ro)
	
Po dodanem zapisu sem resetiral NFS strežnik z ukazom:
UKAZ: service nfs-kernel-server restart

Nato je bilo potrebno narediti mount, nad inštaliranim .iso file-om, zato, da sem pridobil datoteke, katere bo TFTP serviral:
UKAZ: mkdir /media/cdrom
	  mount /home/linuxmint-19.2-cinnamon-64bit.iso /media/cdrom

Da je bil zgornji postopek uspešen, sem preveril z zaporedjem sledečih ukazov:
	mount -t nfs localhost:/media/cdrom /mnt
	ls /mnt	#tako sem videl, ali se nahajajo notri potrebne datoteke / direktoriji (casper, isolinux, ...)
	umount /mnt
	
## PXELINUX.CFG/DEFAULT DATOTEKA ##
Potem sem moral popraviti "pxelinux.cfg/default" datoteko, in sicer konfiguracijo, ki se bo servirala pri liveboot-u:
Polje popravka:	label live
				menu label Start Linux Mint
				kernel /casper/vmlinuz

Pod tem zapisom sem popravil vrstico s spodnjim zapisom:
append  file=/cdrom/preseed/linuxmint.seed boot=casper netboot=nfs nfsroot=172.17.217.1:/media/cdrom initrd=/casper/initrd.lz --

## JEDRO IN GONILNIKI ##
Potem je bilo še potrebno prekopiranje jedra in gonilnikov iz /media/cdrom v direktorij /srv/tftp/casper:
UKAZ: mkdir casper
	  cp /media/cdrom/casper/vmlinuz ./
	  cp /media/cdrom/casper/initrd.lz ./