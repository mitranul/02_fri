/***********************************************************************
 * Module:  Kategorija.java
 * Author:  IceMan47
 * Purpose: Defines the Class Kategorija
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid 04d8712b-d854-43ac-8ead-fa97da859c0c */
public class Kategorija {
   /** @pdOid 79ded456-4772-45a5-aeb5-ba2cd6e49884 */
   private int kategorijaID;
   /** @pdOid e67450c7-f4f4-4eab-bf98-61334c5e1066 */
   private String naziv;
   
   /** @pdRoleInfo migr=no name=Izdelek assc=association1 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection izdelek;
   
   /** @pdOid 355dc151-8c6d-4275-8b7c-51ad3d844eeb */
   public Kategorija vrniDolocenoKategorijo() {
      // TODO: implement
      return null;
   }
   Kategorija(int id, String naziv) {
       this.kategorijaID = id;
       this.naziv = naziv;
   }
   
   /** @pdOid bc7c8f1a-f5f9-4a8b-a145-21a935c70fa4 */
   public void dodajKategorijo(int id, String nazivK) {
      this.kategorijaID = id;
      this.naziv = nazivK;
   }

    public int getKategorijaID() {
        return kategorijaID;
    }

    public String getNaziv() {
        return naziv;
    }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getIzdelek() {
      if (izdelek == null)
         izdelek = new java.util.HashSet();
      return izdelek;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorIzdelek() {
      if (izdelek == null)
         izdelek = new java.util.HashSet();
      return izdelek.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newIzdelek */
   public void setIzdelek(java.util.Collection newIzdelek) {
      removeAllIzdelek();
      for (java.util.Iterator iter = newIzdelek.iterator(); iter.hasNext();)
         addIzdelek((Izdelek)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newIzdelek */
   public void addIzdelek(Izdelek newIzdelek) {
      if (newIzdelek == null)
         return;
      if (this.izdelek == null)
         this.izdelek = new java.util.HashSet();
      if (!this.izdelek.contains(newIzdelek))
         this.izdelek.add(newIzdelek);
   }
   
   /** @pdGenerated default remove
     * @param oldIzdelek */
   public void removeIzdelek(Izdelek oldIzdelek) {
      if (oldIzdelek == null)
         return;
      if (this.izdelek != null)
         if (this.izdelek.contains(oldIzdelek))
            this.izdelek.remove(oldIzdelek);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllIzdelek() {
      if (izdelek != null)
         izdelek.clear();
   }

}