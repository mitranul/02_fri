/***********************************************************************
 * Module:  Izdelek.java
 * Author:  IceMan47
 * Purpose: Defines the Class Izdelek
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid 746d0b9a-95d2-4949-a801-7f704335f20a */
public class Izdelek {
   /** @pdOid 6e38031b-c0f7-4b3a-858b-09aa43409bdc */
   private int izdelekID;
   /** @pdOid 936fb350-acf1-44ce-816e-abd889ba6106 */
   private String naziv;
   /** @pdOid 2664b644-b943-40c8-a721-2cb9ddcf43db */
   private double cena;
   /** @pdOid 13bcb01e-1aaf-4c5d-8802-db083fd4873f */
   private String opis;
   /** @pdOid 98a73aa0-91ce-443c-8c26-6ae77395e7cb */
   private String kategorija;
   /** @pdOid f547e00d-3dac-4607-8593-ef43a4dcb707 */
   private int zaloga;
   
   private String imageURL;

   /** @pdRoleInfo migr=no name=Postavka assc=association2 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection postavka;
   
   /** @pdOid 43707895-deb1-42b2-a95e-14485b83bba8 */
   public Map<Integer, Izdelek> vrniSeznamIzdelkov() {
      // TODO: implement
      return null;
   }
   
   Izdelek(int id, String naziv, double cena, String opis, String kategorija, int zaloga, String url) {
      this.izdelekID = id;
      this.naziv = naziv;
      this.cena = cena;
      this.opis = opis;
      this.kategorija = kategorija;
      this.zaloga = zaloga;
      this.imageURL = url;
   }
   
   /** @pdOid 452e1210-0080-4b8d-8ef9-091c79b41525 */
   public Izdelek vrniDolocenIzdelek() {
      // TODO: implement
      return null;
   }
   
   public double getCena() {
        return cena;
    }

    public String getOpis() {
        return opis;
    }

    public String getKategorija() {
        return kategorija;
    }

    public int getZaloga() {
        return zaloga;
    }
   
   
   public int getIzdelekID() {
       return this.izdelekID;
   }
   
   public String getNaziv() {
       return this.naziv;
   }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
   
   
   /** @pdOid 7b6cd4d9-4b13-47d3-87c9-804ca6c56bd0 */
   public void dodajIzdelek(int id, String naziv, double cena, String opis, String kategorija, int zaloga) {
      this.izdelekID = id;
      this.naziv = naziv;
      this.cena = cena;
      this.opis = opis;
      this.kategorija = kategorija;
      this.zaloga = zaloga;
   }
   
   /** @pdOid 5b8c6872-f21d-406e-83ab-3e83f61c11fb */
   public Izdelek posodobiIzdelek() {
      // TODO: implement
      return null;
   }
   
   /** @pdOid 7d6e447e-dd97-47d5-9056-704907763e62 */
   public int vrniZalogoIzdelka() {
      // TODO: implement
      return 0;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getPostavka() {
      if (postavka == null)
         postavka = new java.util.HashSet();
      return postavka;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorPostavka() {
      if (postavka == null)
         postavka = new java.util.HashSet();
      return postavka.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newPostavka */
   public void setPostavka(java.util.Collection newPostavka) {
      removeAllPostavka();
      for (java.util.Iterator iter = newPostavka.iterator(); iter.hasNext();)
         addPostavka((Postavka)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newPostavka */
   public void addPostavka(Postavka newPostavka) {
      if (newPostavka == null)
         return;
      if (this.postavka == null)
         this.postavka = new java.util.HashSet();
      if (!this.postavka.contains(newPostavka))
         this.postavka.add(newPostavka);
   }
   
   /** @pdGenerated default remove
     * @param oldPostavka */
   public void removePostavka(Postavka oldPostavka) {
      if (oldPostavka == null)
         return;
      if (this.postavka != null)
         if (this.postavka.contains(oldPostavka))
            this.postavka.remove(oldPostavka);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllPostavka() {
      if (postavka != null)
         postavka.clear();
   }

}