/***********************************************************************
 * Module:  Narocilo.java
 * Author:  IceMan47
 * Purpose: Defines the Class Narocilo
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid e705dfa7-2caf-444c-a66f-1b84dd1050d8 */
public class Narocilo {
   /** @pdOid 18e7d5ef-e995-4ca4-888f-a4e9b8eb670f */
   private int narociloID;
   /** @pdOid e0c2d35d-47a6-4918-94ed-479eb45c2a7a */
   private int kupecID;
   /** @pdOid 2d149c51-5234-4b5c-937f-d8c8c09e94f1 */
   private int nacinPlacilaID;
   /** @pdOid 6e2f75e7-34c4-4287-8c95-b48e5683c694 */
   private int nacinDostaveID;
   /** @pdOid d75b1139-7f81-4c7a-8812-d8ab4102272f */
   private Date rokPlacila;
   /** @pdOid 8cd0dc0f-f922-4e75-a48b-f4a975d26a7d */
   private Date datumNarocila;
   /** @pdOid babc4677-db68-41d5-b807-f3f02e9a2edf */
   private String opombe;
   /** @pdOid 9c9dd004-b166-474d-8ef5-7fe8bca0bb61 */
   private boolean statusNarocila;
   
   /** @pdRoleInfo migr=no name=Postavka assc=association4 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection postavka;
   
   Narocilo(int id, int kID, int placiloID, int dostavaID, Date rokPlacila, String opombe, boolean status) {
       this.narociloID = id;
       this.kupecID = kID;
       this.nacinPlacilaID = placiloID;
       this.nacinDostaveID = dostavaID;
       this.rokPlacila = rokPlacila;
       this.datumNarocila = new Date();
       this.opombe = opombe;
       this.statusNarocila = status;
   }
   
   /** @pdOid 2ccf86e1-8a8e-4e4d-89e2-f38bf006c5f9 */
   public Map<Integer, Narocilo> vrniVsaNarocila() {
      // TODO: implement
      return null;
   }
   
   /** @pdOid 7f33ba66-617a-46e4-99f3-ac01c1d3d5b4 */
   public Narocilo vrniDolocenoNarocilo() {
      return null;
   }
   
   /** @pdOid 750e99f5-e199-4741-b80b-31b509b607a2 */
   public boolean vrniStatusNarocila() {
      return this.statusNarocila;
   }
   
   /** @pdOid 10962c39-6c0b-4178-b48c-59e6cf8516d4 */
   public void dodajNarocilo() {
      // TODO: implement
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getPostavka() {
      if (postavka == null)
         postavka = new java.util.HashSet();
      return postavka;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorPostavka() {
      if (postavka == null)
         postavka = new java.util.HashSet();
      return postavka.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newPostavka */
   public void setPostavka(java.util.Collection newPostavka) {
      removeAllPostavka();
      for (java.util.Iterator iter = newPostavka.iterator(); iter.hasNext();)
         addPostavka((Postavka)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newPostavka */
   public void addPostavka(Postavka newPostavka) {
      if (newPostavka == null)
         return;
      if (this.postavka == null)
         this.postavka = new java.util.HashSet();
      if (!this.postavka.contains(newPostavka))
         this.postavka.add(newPostavka);
   }
   
   /** @pdGenerated default remove
     * @param oldPostavka */
   public void removePostavka(Postavka oldPostavka) {
      if (oldPostavka == null)
         return;
      if (this.postavka != null)
         if (this.postavka.contains(oldPostavka))
            this.postavka.remove(oldPostavka);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllPostavka() {
      if (postavka != null)
         postavka.clear();
   }

}