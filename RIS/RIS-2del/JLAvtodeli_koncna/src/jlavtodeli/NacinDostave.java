/***********************************************************************
 * Module:  NacinDostave.java
 * Author:  IceMan47
 * Purpose: Defines the Class NacinDostave
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid 92ce6150-e4fd-40d7-b865-916d44b0dc60 */
public class NacinDostave {
   /** @pdOid 23a33000-d985-4102-8c23-2a9830a7db0b */
   private int dostavaID;
   /** @pdOid 62e331ea-0b0b-48a3-93aa-43c951f904a0 */
   private String naziv;
   
   /** @pdRoleInfo migr=no name=Narocilo assc=association13 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection narocilo;

    public String getNaziv() {
        return naziv;
    }
   
   NacinDostave(int id, String naziv) {
       this.dostavaID = id;
       this.naziv = naziv;
   }
   
   /** @pdOid ee07d48e-9246-4d01-8a78-c2caee6d2ad2 */
   public NacinDostave vrniIzbranoDostavo() {
      // TODO: implement
      return null;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newNarocilo */
   public void setNarocilo(java.util.Collection newNarocilo) {
      removeAllNarocilo();
      for (java.util.Iterator iter = newNarocilo.iterator(); iter.hasNext();)
         addNarocilo((Narocilo)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newNarocilo */
   public void addNarocilo(Narocilo newNarocilo) {
      if (newNarocilo == null)
         return;
      if (this.narocilo == null)
         this.narocilo = new java.util.HashSet();
      if (!this.narocilo.contains(newNarocilo))
         this.narocilo.add(newNarocilo);
   }
   
   /** @pdGenerated default remove
     * @param oldNarocilo */
   public void removeNarocilo(Narocilo oldNarocilo) {
      if (oldNarocilo == null)
         return;
      if (this.narocilo != null)
         if (this.narocilo.contains(oldNarocilo))
            this.narocilo.remove(oldNarocilo);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllNarocilo() {
      if (narocilo != null)
         narocilo.clear();
   }

}