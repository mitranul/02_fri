/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jlavtodeli;

import java.awt.Color;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays; 
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;

/**
 *
 * @author ice
 */
public class FrameGUI extends javax.swing.JFrame {
    private ArrayList<Kategorija> listKategorij = new ArrayList<Kategorija>(Arrays.asList(new Kategorija(0, "Pnevmatike"), new Kategorija(1, "Akumulator"), new Kategorija(2, "Ostalo"), new Kategorija(3, "Luči")));
    private ArrayList<Kupec> listKupcev = new ArrayList<Kupec>(Arrays.asList(new Kupec(0, "Ice", "Man", new Date(), "031228101", "iceman@gmail.com", "Cesta talcev 12, Ljubljana")));
    private ArrayList<NacinDostave> listDostave = new ArrayList<NacinDostave>(Arrays.asList(new NacinDostave(0, "Osebni prevzem"), new NacinDostave(1, "Dostava na dom")));
    private ArrayList<NacinPlacila> listPlacil = new ArrayList<NacinPlacila>(Arrays.asList(new NacinPlacila(0, "Gotovina"), new NacinPlacila(1, "Kartica")));
    private ArrayList<Narocilo> listNarocil = new ArrayList<Narocilo>();
    private ArrayList<Izdelek> listIzdelek;
    private ArrayList<Izdelek> listKosarica = new ArrayList<Izdelek>();
    private String[] comboDostava = {listDostave.get(0).getNaziv(), listDostave.get(1).getNaziv()};
    private String[] comboPlacila = {listPlacil.get(0).getNaziv(), listPlacil.get(1).getNaziv()};
    public final Color errorColor = Color.decode("#ff726b");
    public final Color successColor = Color.decode("#84e087");
    private boolean karticaIsSet = false;
    private int idNarocila = 0, idPlacila = 0, idDostave = 0;
    int kol1 = 0;
    int kol2 = 0;
    int kol3 = 0;
    String location = "src/jlavtodeli/";
    private ArrayList<Izdelek> setIzdelkiList() {
        return new ArrayList<Izdelek>(
                Arrays.asList(
                        new Izdelek(0, "LETNA PNEVMATIKA CONTINENTAL" , 80.00, "Nove pnevmatike", listKategorij.get(0).getNaziv(), 20, "images/pnevmatika.jpg"),
                        new Izdelek(1, "AKUMULATOR ENERGIZER", 70.00, "AKUMULATOR 45AH D+ 400A ENERGIZER",listKategorij.get(1).getNaziv(), 50,"images/akumulator.jpg"),
                        new Izdelek(2, "THULE KOVČEK", 400.00, "TH635200 THULE TOURING M (200) SIVA TITAN - TITAN AERO", listKategorij.get(2).getNaziv(), 5, "images/kovcek.jpg")
                ));
    }
    /**
     * Creates new form FrameGUI
     */
    
    public FrameGUI() {
        initComponents();
        clearAll();

    }
    
    private void clearAll() {
        listIzdelek = setIzdelkiList();
        setProducts();
        productsPanel.setVisible(true);
        cartPanel.setVisible(false);
        narociloPanel.setVisible(false);
        
        cartItemPanel.setVisible(false);
        cartItemPanel1.setVisible(false);
        cartItemPanel2.setVisible(false);
        cenaValueLabel.setText("0€");
        
        koncnoPanel1.setVisible(false);
        koncnoPanel2.setVisible(false);
        koncnoPanel3.setVisible(false);
        skupnaCenaNarocila.setText("0€");
        
        karticaField1.setText("");
        karticaField2.setText("");
        karticaField3.setText("");
        karticaField4.setText("");
        karticaCVV.setText("");
    }
    
     /** ARRAYLIST KOMPONENT **/
    private ArrayList<JTextComponent> karticaFieldList() { 
        return new ArrayList<>(Arrays.asList(karticaField1, karticaField2, karticaField3, karticaField4, karticaCVV));
    }
    
     private boolean isNumeric(String inputString, Component component, boolean kartica) {
        try {
            int number = Integer.parseInt(inputString);
            if(!kartica) {
                izpisNapake("V polju '" + component.getName() + " so vnešene samo številke. Prosim popravite vnos.", errorColor);
                barvajVnosnoPolje((JTextComponent)component, errorColor);
            }
            return true;
        }catch(NumberFormatException e) {
            if(kartica) {
                izpisNapake("Vnešena številka kartice vsebuje nedovoljene znake. Prosim vpišite samo številke.", errorColor);
                barvajVnosnoPolje((JTextComponent)component, errorColor);
            }
            return false;
        }
    }
    
    private boolean checkCreditCardInputs() {
        for(JTextComponent karticaField : karticaFieldList()) {
            if((preveriVnosnoPolje(karticaField, 4, 4) && !karticaField.getName().equals("karticaCVV")) || (preveriVnosnoPolje(karticaField, 3, 3) && karticaField.getName().equals("karticaCVV"))) return false;
            if(!isNumeric(karticaField.getText(), karticaField, true)) { return false; }
            barvajVnosnoPolje(karticaField, Color.white);
        }
        return true;
    }
    
    public void izpisNapake(String besedilo, Color barva) {
        statusLabel.setText(besedilo);
        statusLabel.setBackground(barva);
        statusLabel.setOpaque(true);
    }
    
    /** FUNKCIJI ZA UREJANJE STATUSNE VRSTICE **/
    private void barvajVnosnoPolje(JTextComponent component, Color color) {
        component.setBackground(color);
        component.grabFocus();
    }
    
    /** VALIDACIJA ŠTEVILO ZNAKOV VNEŠENIH V VNOSNO POLJE **/
    private boolean preveriVnosnoPolje(JTextComponent component, int maxVelikost, int minVelikost) {
        barvajVnosnoPolje((JTextComponent) component, Color.white);
        String vrednostPolja = component.getText();
        if(vrednostPolja.length() == 0) {
            izpisNapake("Polje '"+ component.getName() +"' je prazno. Prosim ponovno vnesite podatek.", errorColor);
            barvajVnosnoPolje(component, errorColor);
            return true;
        }
        else if(vrednostPolja.length() > maxVelikost) {
            izpisNapake("V polju '"+ component.getName() +"' je vnešenih preveč znakov. Vnesete lahko največ "+ maxVelikost +" številke.", errorColor);
            barvajVnosnoPolje(component, errorColor);
            return true;
        }
        else if(vrednostPolja.length() < minVelikost) {
            izpisNapake("V polju '"+ component.getName() +"' je vnešenih premalo znakov. Vnesiti morate najmanj "+ minVelikost +" številke.", errorColor);
            barvajVnosnoPolje(component, errorColor);
            return true;
        }
        return false;
    }
    
    private void spremeniFunkcionalnostField(boolean status) {
        karticaField1.setEnabled(status);
        karticaField2.setEnabled(status);
        karticaField3.setEnabled(status);
        karticaField4.setEnabled(status);
        karticaCVV.setEnabled(status);
        karticaIsSet = status;
    }
    
    private void nacinPlacilaCombo(java.awt.event.ActionEvent evt) {                                   
        switch(nacinPlacilaComboBox.getSelectedItem().toString()) {
            case "Kartica": spremeniFunkcionalnostField(true); break;
            default: spremeniFunkcionalnostField(false); break;
        }
    }                            
    
    private void checkCart() {
        if(listKosarica.size() > 0) {
        for(int i=0;i<listKosarica.size();i++) {
            if(listKosarica.get(i).getIzdelekID() == 0) {
                  cartItemPanel.setVisible(true);
                  koncnoPanel1.setVisible(true);
            }
            else if(listKosarica.get(i).getIzdelekID() == 1) {
                  cartItemPanel1.setVisible(true);
                   koncnoPanel2.setVisible(true);
            }
            else if(listKosarica.get(i).getIzdelekID() == 2) {
                  cartItemPanel2.setVisible(true);
                   koncnoPanel3.setVisible(true);
            }
        }
       }
    }
    
    private void setProducts() {
        nazivDelaLabel.setText(listIzdelek.get(0).getNaziv());
        nazivDelaLabel1.setText(listIzdelek.get(1).getNaziv());
        nazivDelaLabel2.setText(listIzdelek.get(2).getNaziv());
        
        cenaLabel.setText(listIzdelek.get(0).getCena()+"€/kos");
        cenaLabel1.setText(listIzdelek.get(1).getCena()+"€/kos");
        cenaLabel2.setText(listIzdelek.get(2).getCena()+"€/kos");
        
        opisLabel.setText(listIzdelek.get(0).getOpis());
        opisLabel1.setText(listIzdelek.get(1).getOpis());
        opisLabel2.setText(listIzdelek.get(2).getOpis());
        
        kategorijaLabel.setText(listIzdelek.get(0).getKategorija());
        kategorijaLabel1.setText(listIzdelek.get(1).getKategorija());
        kategorijaLabel2.setText(listIzdelek.get(2).getKategorija());
        
        zalogaLabel.setText(listIzdelek.get(0).getZaloga()+" kosov");
        zalogaLabel1.setText(listIzdelek.get(1).getZaloga()+" kosov");
        zalogaLabel2.setText(listIzdelek.get(2).getZaloga()+" kosov");
        
        
      
        imageLabel.setIcon(createImage(listIzdelek.get(0).getImageURL()));
        imageLabel1.setIcon(createImage(listIzdelek.get(1).getImageURL()));
        imageLabel2.setIcon(createImage(listIzdelek.get(2).getImageURL()));
    }
    
    
    private void setCartProducts() {
        nazivCartLabel.setText(listIzdelek.get(0).getNaziv());
        nazivCartLabel1.setText(listIzdelek.get(1).getNaziv());
        nazivCartLabel2.setText(listIzdelek.get(2).getNaziv());
        
      
        opisCartLabel.setText(listIzdelek.get(0).getOpis());
        opisCartLabel1.setText(listIzdelek.get(1).getOpis());
        opisCartLabel2.setText(listIzdelek.get(2).getOpis());
        
        zalogaCartSpinner.setValue(kol1);
        zalogaCartSpinner1.setValue(kol2);
        zalogaCartSpinner2.setValue(kol3);
       
        
        cenaCartLabel.setText((listIzdelek.get(0).getCena() * kol1)+"€");
        cenaCartLabel1.setText((listIzdelek.get(1).getCena() * kol2)+"€");
        cenaCartLabel2.setText((listIzdelek.get(2).getCena() * kol3)+"€");
        
        double koncnaCena = (listIzdelek.get(0).getCena() * kol1) + (listIzdelek.get(1).getCena() * kol2) + (listIzdelek.get(2).getCena() * kol3);
        cenaValueLabel.setText(koncnaCena+"€");
        
      
        imageLabel3.setIcon(createImage(listIzdelek.get(0).getImageURL()));
        imageLabel4.setIcon(createImage(listIzdelek.get(1).getImageURL()));
        imageLabel5.setIcon(createImage(listIzdelek.get(2).getImageURL()));
    }
    
     private void setNarociloProducts() {
        nazivIzdelka1Label.setText(listIzdelek.get(0).getNaziv());
        nazivIzdelka2.setText(listIzdelek.get(1).getNaziv());
        nazivIzdelka3.setText(listIzdelek.get(2).getNaziv());
        
        kolicinaNarocilo1.setText(kol1+"x");
        kolicinaNarocilo2.setText(kol2+"x");
        kolicinaNarocilo3.setText(kol3+"x");
        
        cenaIzdelka1.setText((listIzdelek.get(0).getCena() * kol1)+"€");
        cenaIzdelka2.setText((listIzdelek.get(1).getCena() * kol2)+"€");
        cenaIzdelka3.setText((listIzdelek.get(2).getCena() * kol3)+"€");
        
        double koncnaCena = (listIzdelek.get(0).getCena() * kol1) + (listIzdelek.get(1).getCena() * kol2) + (listIzdelek.get(2).getCena() * kol3);
        
        skupnaCenaNarocila.setText(koncnaCena+"€");
     }
    
    private ImageIcon createImage(String fileName) {
        ImageIcon imageIcon = new ImageIcon(this.getClass().getResource(fileName)); // load the image to a imageIcon
        java.awt.Image image = imageIcon.getImage(); // transform it 
        java.awt.Image newimg = image.getScaledInstance(270, 148,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
     
        return new ImageIcon(newimg);
    } 
    
    private void createCustomer() {
        kupecIme.setText(listKupcev.get(0).getIme() + " " + listKupcev.get(0).getPriimek());
        kupecNaslov.setText(listKupcev.get(0).getNaslov());
        kupecMail.setText(listKupcev.get(0).getEmail());
        kupecTelefon.setText(listKupcev.get(0).getTelefon());
    }
    
    private ComboBoxModel napolniComboDostava() {
        return new DefaultComboBoxModel(comboDostava);
    }
    
    private ComboBoxModel napolniComboPlacila() {
        return new DefaultComboBoxModel(comboPlacila);
    }
    
    private Narocilo createFinalNarocilo(int kupecId) {
        return new Narocilo(idNarocila, kupecId, idPlacila, idDostave, new Date(), "Ni opomb", true);
    }
    
    private void oddajNarocilo(java.awt.event.ActionEvent evt) {
           
        SvBanka_SIM banka = new SvBanka_SIM();
        String bankaMess = "Transakcija 012353414 je bila uspešna!";
        banka.izvediTransakcijoPotrjenegaNarocila(bankaMess);
        SvSistemEPosta_SIM eposta = new SvSistemEPosta_SIM();
        String epostaMess = "Vaše naročilo je bilo poslano na dani e-naslov!";
        eposta.posredujNarociloKupcu(epostaMess);
        pocistiKosaricoButtonActionPerformed(evt);
        clearAll();
        confirmSaveOptionPane.showMessageDialog(null, epostaMess);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        confirmSaveOptionPane = new javax.swing.JOptionPane();
        layerPanel = new javax.swing.JPanel();
        productsPanel = new javax.swing.JPanel();
        cardPanel1 = new javax.swing.JPanel();
        imagePanel = new javax.swing.JPanel();
        imageLabel = new javax.swing.JLabel();
        nazivDelaLabel = new javax.swing.JLabel();
        kategorijaLabel = new javax.swing.JLabel();
        opisLabel = new javax.swing.JLabel();
        cenaLabel = new javax.swing.JLabel();
        zalogaLabel = new javax.swing.JLabel();
        dodajCartButton = new javax.swing.JButton();
        cardPanel2 = new javax.swing.JPanel();
        imagePanel1 = new javax.swing.JPanel();
        imageLabel1 = new javax.swing.JLabel();
        nazivDelaLabel1 = new javax.swing.JLabel();
        kategorijaLabel1 = new javax.swing.JLabel();
        opisLabel1 = new javax.swing.JLabel();
        cenaLabel1 = new javax.swing.JLabel();
        zalogaLabel1 = new javax.swing.JLabel();
        dodajCartButton1 = new javax.swing.JButton();
        cardPanel3 = new javax.swing.JPanel();
        imagePanel2 = new javax.swing.JPanel();
        imageLabel2 = new javax.swing.JLabel();
        nazivDelaLabel2 = new javax.swing.JLabel();
        kategorijaLabel2 = new javax.swing.JLabel();
        opisLabel2 = new javax.swing.JLabel();
        cenaLabel2 = new javax.swing.JLabel();
        zalogaLabel2 = new javax.swing.JLabel();
        dodajCartButton2 = new javax.swing.JButton();
        orodnaPanel = new javax.swing.JPanel();
        prikaziKosaricoButton = new javax.swing.JButton();
        cartPanel = new javax.swing.JPanel();
        cartItemPanel = new javax.swing.JPanel();
        imagePanel3 = new javax.swing.JPanel();
        imageLabel3 = new javax.swing.JLabel();
        nazivCartLabel = new javax.swing.JLabel();
        cenaCartLabel = new javax.swing.JLabel();
        zalogaCartSpinner = new javax.swing.JSpinner();
        opisCartLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cartItemPanel1 = new javax.swing.JPanel();
        imagePanel4 = new javax.swing.JPanel();
        imageLabel4 = new javax.swing.JLabel();
        nazivCartLabel1 = new javax.swing.JLabel();
        cenaCartLabel1 = new javax.swing.JLabel();
        zalogaCartSpinner1 = new javax.swing.JSpinner();
        opisCartLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cartItemPanel2 = new javax.swing.JPanel();
        imagePanel5 = new javax.swing.JPanel();
        imageLabel5 = new javax.swing.JLabel();
        nazivCartLabel2 = new javax.swing.JLabel();
        cenaCartLabel2 = new javax.swing.JLabel();
        zalogaCartSpinner2 = new javax.swing.JSpinner();
        opisCartLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        orodnaPanel1 = new javax.swing.JPanel();
        pocistiKosaricoButton = new javax.swing.JButton();
        nadaljujNakupButton = new javax.swing.JButton();
        nazajButton2 = new javax.swing.JButton();
        kosaricaLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        koncnaCenaLabel = new javax.swing.JLabel();
        cenaValueLabel = new javax.swing.JLabel();
        narociloPanel = new javax.swing.JPanel();
        kupecPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        kupecIme = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        kupecNaslov = new javax.swing.JLabel();
        kupecMail = new javax.swing.JLabel();
        kupecTelefon = new javax.swing.JLabel();
        nacinDostavePanel = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        nacinDostaveComboBox = new javax.swing.JComboBox<>();
        nacinPlacilaPanel = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        nacinPlacilaComboBox = new javax.swing.JComboBox<>();
        karticaField1 = new javax.swing.JTextField();
        karticaField2 = new javax.swing.JTextField();
        karticaField3 = new javax.swing.JTextField();
        karticaField4 = new javax.swing.JTextField();
        karticaCVV = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        narociloPodatkiPanel = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        koncnoPanel1 = new javax.swing.JPanel();
        nazivIzdelka1Label = new javax.swing.JLabel();
        kolicinaNarocilo1 = new javax.swing.JLabel();
        cenaIzdelka1 = new javax.swing.JLabel();
        koncnoPanel2 = new javax.swing.JPanel();
        nazivIzdelka2 = new javax.swing.JLabel();
        kolicinaNarocilo2 = new javax.swing.JLabel();
        cenaIzdelka2 = new javax.swing.JLabel();
        koncnoPanel3 = new javax.swing.JPanel();
        nazivIzdelka3 = new javax.swing.JLabel();
        kolicinaNarocilo3 = new javax.swing.JLabel();
        cenaIzdelka3 = new javax.swing.JLabel();
        koncnoPanelVsota = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        skupnaCenaNarocila = new javax.swing.JLabel();
        orodnaPanel2 = new javax.swing.JPanel();
        nazajButton1 = new javax.swing.JButton();
        koncajNarociloButton = new javax.swing.JButton();
        statusPanel = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        avtorMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JL Avtodeli");

        layerPanel.setLayout(new javax.swing.OverlayLayout(layerPanel));

        cardPanel1.setBackground(java.awt.Color.white);
        cardPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanelLayout = new javax.swing.GroupLayout(imagePanel);
        imagePanel.setLayout(imagePanelLayout);
        imagePanelLayout.setHorizontalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        imagePanelLayout.setVerticalGroup(
            imagePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        nazivDelaLabel.setText("Letna pnevmatika Sailiun");

        kategorijaLabel.setText("Pnevmatike");

        opisLabel.setText("LETNA PNEVMATIKA SAILUN \n205/55R16 91V FR ATREZZO \nELITE\n");

        cenaLabel.setText("42,00 €/kos");

        zalogaLabel.setText("10 kosov");

        dodajCartButton.setText("Dodaj v košarico");
        dodajCartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajIzdelek1(evt);
            }
        });

        javax.swing.GroupLayout cardPanel1Layout = new javax.swing.GroupLayout(cardPanel1);
        cardPanel1.setLayout(cardPanel1Layout);
        cardPanel1Layout.setHorizontalGroup(
            cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cardPanel1Layout.createSequentialGroup()
                        .addGroup(cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(imagePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(cardPanel1Layout.createSequentialGroup()
                                .addGroup(cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nazivDelaLabel)
                                    .addComponent(kategorijaLabel))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(61, 61, 61))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cardPanel1Layout.createSequentialGroup()
                        .addGroup(cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(cardPanel1Layout.createSequentialGroup()
                                .addComponent(cenaLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(zalogaLabel))
                            .addComponent(opisLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(67, 67, 67))))
            .addGroup(cardPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(dodajCartButton, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        cardPanel1Layout.setVerticalGroup(
            cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nazivDelaLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kategorijaLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opisLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(cardPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cenaLabel)
                    .addComponent(zalogaLabel))
                .addGap(48, 48, 48)
                .addComponent(dodajCartButton, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        cardPanel2.setBackground(java.awt.Color.white);
        cardPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanel1Layout = new javax.swing.GroupLayout(imagePanel1);
        imagePanel1.setLayout(imagePanel1Layout);
        imagePanel1Layout.setHorizontalGroup(
            imagePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        imagePanel1Layout.setVerticalGroup(
            imagePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        nazivDelaLabel1.setText("Letna pnevmatika Sailiun");

        kategorijaLabel1.setText("Pnevmatike");

        opisLabel1.setText("LETNA PNEVMATIKA SAILUN \n205/55R16 91V FR ATREZZO \nELITE\n");

        cenaLabel1.setText("42,00 €/kos");

        zalogaLabel1.setText("10 kosov");

        dodajCartButton1.setText("Dodaj v košarico");
        dodajCartButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajIzdelek2(evt);
            }
        });

        javax.swing.GroupLayout cardPanel2Layout = new javax.swing.GroupLayout(cardPanel2);
        cardPanel2.setLayout(cardPanel2Layout);
        cardPanel2Layout.setHorizontalGroup(
            cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cardPanel2Layout.createSequentialGroup()
                        .addGroup(cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(imagePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(cardPanel2Layout.createSequentialGroup()
                                .addGroup(cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nazivDelaLabel1)
                                    .addComponent(kategorijaLabel1))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(61, 61, 61))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cardPanel2Layout.createSequentialGroup()
                        .addGroup(cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(cardPanel2Layout.createSequentialGroup()
                                .addComponent(cenaLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(zalogaLabel1))
                            .addComponent(opisLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(67, 67, 67))))
            .addGroup(cardPanel2Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(dodajCartButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        cardPanel2Layout.setVerticalGroup(
            cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nazivDelaLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kategorijaLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opisLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(cardPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cenaLabel1)
                    .addComponent(zalogaLabel1))
                .addGap(48, 48, 48)
                .addComponent(dodajCartButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        cardPanel3.setBackground(java.awt.Color.white);
        cardPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanel2Layout = new javax.swing.GroupLayout(imagePanel2);
        imagePanel2.setLayout(imagePanel2Layout);
        imagePanel2Layout.setHorizontalGroup(
            imagePanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        imagePanel2Layout.setVerticalGroup(
            imagePanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(imageLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        nazivDelaLabel2.setText("Letna pnevmatika Sailiun");

        kategorijaLabel2.setText("Pnevmatike");

        opisLabel2.setText("LETNA PNEVMATIKA SAILUN \n205/55R16 91V FR ATREZZO \nELITE\n");

        cenaLabel2.setText("42,00 €/kos");

        zalogaLabel2.setText("10 kosov");

        dodajCartButton2.setText("Dodaj v košarico");
        dodajCartButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajIzdelek3(evt);
            }
        });

        javax.swing.GroupLayout cardPanel3Layout = new javax.swing.GroupLayout(cardPanel3);
        cardPanel3.setLayout(cardPanel3Layout);
        cardPanel3Layout.setHorizontalGroup(
            cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cardPanel3Layout.createSequentialGroup()
                        .addGroup(cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(imagePanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(cardPanel3Layout.createSequentialGroup()
                                .addGroup(cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nazivDelaLabel2)
                                    .addComponent(kategorijaLabel2))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(61, 61, 61))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cardPanel3Layout.createSequentialGroup()
                        .addGroup(cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(cardPanel3Layout.createSequentialGroup()
                                .addComponent(cenaLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(zalogaLabel2))
                            .addComponent(opisLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(67, 67, 67))))
            .addGroup(cardPanel3Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(dodajCartButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        cardPanel3Layout.setVerticalGroup(
            cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cardPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nazivDelaLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(kategorijaLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opisLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(cardPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cenaLabel2)
                    .addComponent(zalogaLabel2))
                .addGap(48, 48, 48)
                .addComponent(dodajCartButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        orodnaPanel.setBackground(new java.awt.Color(177, 177, 177));

        prikaziKosaricoButton.setText("Prikaži košarico");
        prikaziKosaricoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prikaziKosaricoButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout orodnaPanelLayout = new javax.swing.GroupLayout(orodnaPanel);
        orodnaPanel.setLayout(orodnaPanelLayout);
        orodnaPanelLayout.setHorizontalGroup(
            orodnaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, orodnaPanelLayout.createSequentialGroup()
                .addContainerGap(858, Short.MAX_VALUE)
                .addComponent(prikaziKosaricoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48))
        );
        orodnaPanelLayout.setVerticalGroup(
            orodnaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orodnaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(prikaziKosaricoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout productsPanelLayout = new javax.swing.GroupLayout(productsPanel);
        productsPanel.setLayout(productsPanelLayout);
        productsPanelLayout.setHorizontalGroup(
            productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productsPanelLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(cardPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(cardPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54)
                .addComponent(cardPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(orodnaPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        productsPanelLayout.setVerticalGroup(
            productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productsPanelLayout.createSequentialGroup()
                .addGroup(productsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cardPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cardPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(orodnaPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        layerPanel.add(productsPanel);

        cartPanel.setPreferredSize(new java.awt.Dimension(930, 450));

        cartItemPanel.setBackground(java.awt.Color.white);
        cartItemPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanel3Layout = new javax.swing.GroupLayout(imagePanel3);
        imagePanel3.setLayout(imagePanel3Layout);
        imagePanel3Layout.setHorizontalGroup(
            imagePanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imageLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        imagePanel3Layout.setVerticalGroup(
            imagePanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel3Layout.createSequentialGroup()
                .addComponent(imageLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 5, Short.MAX_VALUE))
        );

        nazivCartLabel.setText("Ime izdelka");

        cenaCartLabel.setText("100€");

        opisCartLabel.setText("Opis");

        jLabel2.setText("Količina");

        javax.swing.GroupLayout cartItemPanelLayout = new javax.swing.GroupLayout(cartItemPanel);
        cartItemPanel.setLayout(cartItemPanelLayout);
        cartItemPanelLayout.setHorizontalGroup(
            cartItemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nazivCartLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(opisCartLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zalogaCartSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(cenaCartLabel)
                .addGap(46, 46, 46))
        );
        cartItemPanelLayout.setVerticalGroup(
            cartItemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanelLayout.createSequentialGroup()
                .addGroup(cartItemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cartItemPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(imagePanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(cartItemPanelLayout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(cartItemPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazivCartLabel)
                            .addComponent(cenaCartLabel)
                            .addComponent(zalogaCartSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(opisCartLabel)
                            .addComponent(jLabel2))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cartItemPanel1.setBackground(java.awt.Color.white);
        cartItemPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanel4Layout = new javax.swing.GroupLayout(imagePanel4);
        imagePanel4.setLayout(imagePanel4Layout);
        imagePanel4Layout.setHorizontalGroup(
            imagePanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imageLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        imagePanel4Layout.setVerticalGroup(
            imagePanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel4Layout.createSequentialGroup()
                .addComponent(imageLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 5, Short.MAX_VALUE))
        );

        nazivCartLabel1.setText("Ime izdelka");

        cenaCartLabel1.setText("100€");

        opisCartLabel1.setText("Opis");

        jLabel3.setText("Količina");

        javax.swing.GroupLayout cartItemPanel1Layout = new javax.swing.GroupLayout(cartItemPanel1);
        cartItemPanel1.setLayout(cartItemPanel1Layout);
        cartItemPanel1Layout.setHorizontalGroup(
            cartItemPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nazivCartLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(opisCartLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zalogaCartSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(cenaCartLabel1)
                .addGap(46, 46, 46))
        );
        cartItemPanel1Layout.setVerticalGroup(
            cartItemPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanel1Layout.createSequentialGroup()
                .addGroup(cartItemPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cartItemPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(imagePanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(cartItemPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(cartItemPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazivCartLabel1)
                            .addComponent(cenaCartLabel1)
                            .addComponent(zalogaCartSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(opisCartLabel1)
                            .addComponent(jLabel3))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cartItemPanel2.setBackground(java.awt.Color.white);
        cartItemPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout imagePanel5Layout = new javax.swing.GroupLayout(imagePanel5);
        imagePanel5.setLayout(imagePanel5Layout);
        imagePanel5Layout.setHorizontalGroup(
            imagePanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imageLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        imagePanel5Layout.setVerticalGroup(
            imagePanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imagePanel5Layout.createSequentialGroup()
                .addComponent(imageLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 5, Short.MAX_VALUE))
        );

        nazivCartLabel2.setText("Ime izdelka");

        cenaCartLabel2.setText("100€");

        opisCartLabel2.setText("Opis");

        jLabel4.setText("Količina");

        javax.swing.GroupLayout cartItemPanel2Layout = new javax.swing.GroupLayout(cartItemPanel2);
        cartItemPanel2.setLayout(cartItemPanel2Layout);
        cartItemPanel2Layout.setHorizontalGroup(
            cartItemPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(imagePanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(nazivCartLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(opisCartLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zalogaCartSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(cenaCartLabel2)
                .addGap(46, 46, 46))
        );
        cartItemPanel2Layout.setVerticalGroup(
            cartItemPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cartItemPanel2Layout.createSequentialGroup()
                .addGroup(cartItemPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cartItemPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(imagePanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(cartItemPanel2Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(cartItemPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazivCartLabel2)
                            .addComponent(cenaCartLabel2)
                            .addComponent(zalogaCartSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(opisCartLabel2)
                            .addComponent(jLabel4))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        orodnaPanel1.setBackground(new java.awt.Color(177, 177, 177));

        pocistiKosaricoButton.setText("Počisti košarico");
        pocistiKosaricoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pocistiKosaricoButtonActionPerformed(evt);
            }
        });

        nadaljujNakupButton.setText("Nadaljuj nakup");
        nadaljujNakupButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nadaljujNakupButtonActionPerformed(evt);
            }
        });

        nazajButton2.setText("Nazaj");
        nazajButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nazajButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout orodnaPanel1Layout = new javax.swing.GroupLayout(orodnaPanel1);
        orodnaPanel1.setLayout(orodnaPanel1Layout);
        orodnaPanel1Layout.setHorizontalGroup(
            orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orodnaPanel1Layout.createSequentialGroup()
                .addGap(444, 444, 444)
                .addComponent(pocistiKosaricoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(462, Short.MAX_VALUE))
            .addGroup(orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, orodnaPanel1Layout.createSequentialGroup()
                    .addContainerGap(858, Short.MAX_VALUE)
                    .addComponent(nadaljujNakupButton, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(48, 48, 48)))
            .addGroup(orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(orodnaPanel1Layout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(nazajButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(857, Short.MAX_VALUE)))
        );
        orodnaPanel1Layout.setVerticalGroup(
            orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orodnaPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pocistiKosaricoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(orodnaPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(nadaljujNakupButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(orodnaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(orodnaPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(nazajButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        kosaricaLabel.setText("Košarica");

        koncnaCenaLabel.setText("Končna cena:");

        cenaValueLabel.setText("0€");

        javax.swing.GroupLayout cartPanelLayout = new javax.swing.GroupLayout(cartPanel);
        cartPanel.setLayout(cartPanelLayout);
        cartPanelLayout.setHorizontalGroup(
            cartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(orodnaPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(cartPanelLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(cartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cartPanelLayout.createSequentialGroup()
                        .addComponent(koncnaCenaLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cenaValueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cartItemPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(kosaricaLabel)
                    .addComponent(cartItemPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cartItemPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1))
                .addGap(55, 55, 55))
        );
        cartPanelLayout.setVerticalGroup(
            cartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cartPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(kosaricaLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cartItemPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cartItemPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cartItemPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(cartPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(koncnaCenaLabel)
                    .addComponent(cenaValueLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                .addComponent(orodnaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        layerPanel.add(cartPanel);

        narociloPanel.setPreferredSize(new java.awt.Dimension(930, 450));

        kupecPanel.setBackground(new java.awt.Color(208, 208, 208));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel1.setText("Podatki o kupcu");

        jLabel5.setText("Ime in priimek kupca:");

        kupecIme.setText("jLabel3");

        jLabel7.setText("Naslov kupca:");

        jLabel8.setText("E-mail kupca:");

        jLabel9.setText("Telefon kupca:");

        kupecNaslov.setText("jLabel9");

        kupecMail.setText("jLabel10");

        kupecTelefon.setText("jLabel11");

        javax.swing.GroupLayout kupecPanelLayout = new javax.swing.GroupLayout(kupecPanel);
        kupecPanel.setLayout(kupecPanelLayout);
        kupecPanelLayout.setHorizontalGroup(
            kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kupecPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9))
                .addGap(27, 27, 27)
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(kupecTelefon)
                    .addComponent(kupecMail)
                    .addComponent(kupecNaslov)
                    .addComponent(kupecIme))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kupecPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(152, 152, 152))
        );
        kupecPanelLayout.setVerticalGroup(
            kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(kupecPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(kupecIme))
                .addGap(18, 18, 18)
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(kupecNaslov))
                .addGap(18, 18, 18)
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(kupecMail))
                .addGap(18, 18, 18)
                .addGroup(kupecPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(kupecTelefon))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        nacinDostavePanel.setBackground(new java.awt.Color(189, 189, 189));

        jLabel13.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel13.setText("Način dostave");

        jLabel14.setText("Izberite način dostave:");

        nacinDostaveComboBox.setModel(napolniComboDostava());

        javax.swing.GroupLayout nacinDostavePanelLayout = new javax.swing.GroupLayout(nacinDostavePanel);
        nacinDostavePanel.setLayout(nacinDostavePanelLayout);
        nacinDostavePanelLayout.setHorizontalGroup(
            nacinDostavePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nacinDostavePanelLayout.createSequentialGroup()
                .addGroup(nacinDostavePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(nacinDostavePanelLayout.createSequentialGroup()
                        .addGap(238, 238, 238)
                        .addComponent(jLabel13)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(nacinDostavePanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(nacinDostaveComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        nacinDostavePanelLayout.setVerticalGroup(
            nacinDostavePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nacinDostavePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addGroup(nacinDostavePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(nacinDostaveComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        nacinPlacilaPanel.setBackground(new java.awt.Color(201, 201, 201));

        jLabel15.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel15.setText("Način plačila");

        nacinPlacilaComboBox.setModel(napolniComboPlacila());
        nacinPlacilaComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nacinPlacilaComboBoxnacinPlacilaCombo(evt);
            }
        });

        karticaField1.setEnabled(false);
        karticaField1.setName("karticaF1"); // NOI18N

        karticaField2.setEnabled(false);
        karticaField2.setName("karticaF2"); // NOI18N

        karticaField3.setEnabled(false);
        karticaField3.setName("karticaF3"); // NOI18N

        karticaField4.setEnabled(false);
        karticaField4.setName("karticaF4"); // NOI18N

        karticaCVV.setEnabled(false);
        karticaCVV.setName("karticaCVV"); // NOI18N

        jLabel16.setText("CCV:");

        jLabel17.setText("Št. kartice:");

        javax.swing.GroupLayout nacinPlacilaPanelLayout = new javax.swing.GroupLayout(nacinPlacilaPanel);
        nacinPlacilaPanel.setLayout(nacinPlacilaPanelLayout);
        nacinPlacilaPanelLayout.setHorizontalGroup(
            nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nacinPlacilaPanelLayout.createSequentialGroup()
                .addGroup(nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(nacinPlacilaPanelLayout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jLabel15)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nacinPlacilaPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(nacinPlacilaComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(nacinPlacilaPanelLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(karticaField1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nacinPlacilaPanelLayout.createSequentialGroup()
                                .addComponent(karticaField2, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(karticaField3, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(karticaField4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nacinPlacilaPanelLayout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(karticaCVV, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        nacinPlacilaPanelLayout.setVerticalGroup(
            nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nacinPlacilaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15)
                .addGap(19, 19, 19)
                .addGroup(nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nacinPlacilaComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karticaCVV, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(nacinPlacilaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(karticaField1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karticaField2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karticaField3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(karticaField4, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(21, 21, 21))
        );

        narociloPodatkiPanel.setBackground(new java.awt.Color(182, 182, 182));

        jLabel18.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel18.setText("Podatki o naročilu");

        nazivIzdelka1Label.setText("Naziv izdelka:");

        kolicinaNarocilo1.setText("0x");

        cenaIzdelka1.setText("0€");

        javax.swing.GroupLayout koncnoPanel1Layout = new javax.swing.GroupLayout(koncnoPanel1);
        koncnoPanel1.setLayout(koncnoPanel1Layout);
        koncnoPanel1Layout.setHorizontalGroup(
            koncnoPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(koncnoPanel1Layout.createSequentialGroup()
                .addComponent(nazivIzdelka1Label)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(kolicinaNarocilo1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cenaIzdelka1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        koncnoPanel1Layout.setVerticalGroup(
            koncnoPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, koncnoPanel1Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(koncnoPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nazivIzdelka1Label)
                    .addComponent(kolicinaNarocilo1)
                    .addComponent(cenaIzdelka1))
                .addContainerGap())
        );

        nazivIzdelka2.setText("Naziv izdelka:");

        kolicinaNarocilo2.setText("0x");

        cenaIzdelka2.setText("0€");

        javax.swing.GroupLayout koncnoPanel2Layout = new javax.swing.GroupLayout(koncnoPanel2);
        koncnoPanel2.setLayout(koncnoPanel2Layout);
        koncnoPanel2Layout.setHorizontalGroup(
            koncnoPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(koncnoPanel2Layout.createSequentialGroup()
                .addComponent(nazivIzdelka2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(kolicinaNarocilo2, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cenaIzdelka2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        koncnoPanel2Layout.setVerticalGroup(
            koncnoPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, koncnoPanel2Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addGroup(koncnoPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nazivIzdelka2)
                    .addComponent(kolicinaNarocilo2)
                    .addComponent(cenaIzdelka2))
                .addContainerGap())
        );

        nazivIzdelka3.setText("Naziv izdelka:");

        kolicinaNarocilo3.setText("0x");

        cenaIzdelka3.setText("0€");

        javax.swing.GroupLayout koncnoPanel3Layout = new javax.swing.GroupLayout(koncnoPanel3);
        koncnoPanel3.setLayout(koncnoPanel3Layout);
        koncnoPanel3Layout.setHorizontalGroup(
            koncnoPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(koncnoPanel3Layout.createSequentialGroup()
                .addComponent(nazivIzdelka3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(kolicinaNarocilo3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cenaIzdelka3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        koncnoPanel3Layout.setVerticalGroup(
            koncnoPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, koncnoPanel3Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(koncnoPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nazivIzdelka3)
                    .addComponent(kolicinaNarocilo3)
                    .addComponent(cenaIzdelka3))
                .addContainerGap())
        );

        jLabel22.setText("Skupna cena naročila:");

        skupnaCenaNarocila.setText("0€");

        javax.swing.GroupLayout koncnoPanelVsotaLayout = new javax.swing.GroupLayout(koncnoPanelVsota);
        koncnoPanelVsota.setLayout(koncnoPanelVsotaLayout);
        koncnoPanelVsotaLayout.setHorizontalGroup(
            koncnoPanelVsotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(koncnoPanelVsotaLayout.createSequentialGroup()
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(skupnaCenaNarocila, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        koncnoPanelVsotaLayout.setVerticalGroup(
            koncnoPanelVsotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, koncnoPanelVsotaLayout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(koncnoPanelVsotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(skupnaCenaNarocila))
                .addContainerGap())
        );

        javax.swing.GroupLayout narociloPodatkiPanelLayout = new javax.swing.GroupLayout(narociloPodatkiPanel);
        narociloPodatkiPanel.setLayout(narociloPodatkiPanelLayout);
        narociloPodatkiPanelLayout.setHorizontalGroup(
            narociloPodatkiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                .addGroup(narociloPodatkiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(koncnoPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                        .addGap(234, 234, 234)
                        .addComponent(jLabel18)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(koncnoPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(koncnoPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(koncnoPanelVsota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        narociloPodatkiPanelLayout.setVerticalGroup(
            narociloPodatkiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(narociloPodatkiPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(koncnoPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(koncnoPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(koncnoPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(koncnoPanelVsota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        orodnaPanel2.setBackground(new java.awt.Color(177, 177, 177));

        nazajButton1.setText("Nazaj");
        nazajButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nazajButton1ActionPerformed(evt);
            }
        });

        koncajNarociloButton.setText("Končaj naročilo");
        koncajNarociloButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                koncajNarociloButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout orodnaPanel2Layout = new javax.swing.GroupLayout(orodnaPanel2);
        orodnaPanel2.setLayout(orodnaPanel2Layout);
        orodnaPanel2Layout.setHorizontalGroup(
            orodnaPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orodnaPanel2Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(nazajButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(867, Short.MAX_VALUE))
            .addGroup(orodnaPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, orodnaPanel2Layout.createSequentialGroup()
                    .addContainerGap(858, Short.MAX_VALUE)
                    .addComponent(koncajNarociloButton, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(48, 48, 48)))
        );
        orodnaPanel2Layout.setVerticalGroup(
            orodnaPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(orodnaPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nazajButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(orodnaPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(orodnaPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(koncajNarociloButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        javax.swing.GroupLayout narociloPanelLayout = new javax.swing.GroupLayout(narociloPanel);
        narociloPanel.setLayout(narociloPanelLayout);
        narociloPanelLayout.setHorizontalGroup(
            narociloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(orodnaPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(narociloPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(narociloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nacinPlacilaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(kupecPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(narociloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nacinDostavePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(narociloPodatkiPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        narociloPanelLayout.setVerticalGroup(
            narociloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, narociloPanelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(narociloPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(narociloPanelLayout.createSequentialGroup()
                        .addComponent(kupecPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(nacinPlacilaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(narociloPanelLayout.createSequentialGroup()
                        .addComponent(nacinDostavePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(narociloPodatkiPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(orodnaPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        layerPanel.add(narociloPanel);

        getContentPane().add(layerPanel, java.awt.BorderLayout.NORTH);

        statusPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        statusPanel.setPreferredSize(new java.awt.Dimension(1007, 30));

        statusLabel.setBackground(java.awt.Color.red);
        statusLabel.setText("Pripravljen na izbiro izdelkov.");
        statusLabel.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 1066, Short.MAX_VALUE)
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusLabel)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(statusPanel, java.awt.BorderLayout.SOUTH);

        jMenu1.setMnemonic('D');
        jMenu1.setText("Datoteka");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setMnemonic('I');
        jMenuItem1.setText("Izhod");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Pomoč");

        avtorMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        avtorMenuItem.setMnemonic('I');
        avtorMenuItem.setText("Avtorji");
        avtorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avtorMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(avtorMenuItem);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeZalogaSpinner() {
        zalogaCartSpinner.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    //label.setText(zalogaCartSpinner.getValue().toString());
                    kol1=Integer.parseInt(zalogaCartSpinner.getValue().toString());
                }
            });
         zalogaCartSpinner1.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    //label.setText(zalogaCartSpinner.getValue().toString());
                    kol2=Integer.parseInt(zalogaCartSpinner1.getValue().toString());
                }
            });
          zalogaCartSpinner2.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    //label.setText(zalogaCartSpinner.getValue().toString());
                    kol3=Integer.parseInt(zalogaCartSpinner2.getValue().toString());
                }
            });
    }
    
    
    private void prikaziKosaricoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prikaziKosaricoButtonActionPerformed
        productsPanel.setVisible(false);
        cartPanel.setVisible(true);
        checkCart();
        setCartProducts();
        changeZalogaSpinner();
        if(listKosarica.isEmpty()) {
            nadaljujNakupButton.setEnabled(false);
        }
        else {
            nadaljujNakupButton.setEnabled(true);
        }
    }//GEN-LAST:event_prikaziKosaricoButtonActionPerformed

    private void dodajIzdelek1(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajIzdelek1
        listKosarica.add(listIzdelek.get(0));
        kol1++;
        izpisNapake("Izdelek je dodan v košarico.", successColor);
        for(Izdelek izdelek : listKosarica) 
            System.out.println(izdelek.getNaziv() + " " + izdelek.getCena());
    }//GEN-LAST:event_dodajIzdelek1

    private void dodajIzdelek2(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajIzdelek2
       listKosarica.add(listIzdelek.get(1));
       kol2++;
       izpisNapake("Izdelek je dodan v košarico.", successColor);
       for(Izdelek izdelek : listKosarica) 
            System.out.println(izdelek.getNaziv() + " " + izdelek.getCena());
    }//GEN-LAST:event_dodajIzdelek2

    private void dodajIzdelek3(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajIzdelek3
        listKosarica.add(listIzdelek.get(2));
        kol3++;
        izpisNapake("Izdelek je dodan v košarico.", successColor);
        for(Izdelek izdelek : listKosarica) 
            System.out.println(izdelek.getNaziv() + " " + izdelek.getCena());
    }//GEN-LAST:event_dodajIzdelek3

    private void pocistiKosaricoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pocistiKosaricoButtonActionPerformed
        listKosarica = new ArrayList<Izdelek>();
        cartItemPanel.setVisible(false);
        cartItemPanel1.setVisible(false);
        cartItemPanel2.setVisible(false);
        cenaValueLabel.setText("0€");
        kol1=0;
        kol2=0;
        kol3=0;
        nadaljujNakupButton.setEnabled(false);
        
    }//GEN-LAST:event_pocistiKosaricoButtonActionPerformed

    private void nadaljujNakupButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nadaljujNakupButtonActionPerformed
        cartPanel.setVisible(false);
        narociloPanel.setVisible(true);
        checkCart();
        setNarociloProducts();
        createCustomer();
        napolniComboDostava();
        napolniComboPlacila();
    }//GEN-LAST:event_nadaljujNakupButtonActionPerformed

    private void nazajButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nazajButton2ActionPerformed
        productsPanel.setVisible(true);
        cartPanel.setVisible(false);
    }//GEN-LAST:event_nazajButton2ActionPerformed

    private void nazajButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nazajButton1ActionPerformed
        cartPanel.setVisible(true);
        narociloPanel.setVisible(false);
    }//GEN-LAST:event_nazajButton1ActionPerformed

    private void koncajNarociloButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_koncajNarociloButtonActionPerformed
       idDostave = nacinDostaveComboBox.getSelectedIndex();
       idPlacila = nacinPlacilaComboBox.getSelectedIndex();
       if(karticaIsSet) {
           if(checkCreditCardInputs()) {
               izpisNapake("Vaš nakup je bil uspešno izveden.", successColor);
               listNarocil.add(createFinalNarocilo(listKupcev.get(0).getKupecID()));
               idNarocila++;
               oddajNarocilo(evt);
           }
       } else {
           izpisNapake("Vaš nakup je bil uspešno izveden.", successColor);
           listNarocil.add(createFinalNarocilo(listKupcev.get(0).getKupecID()));
           idNarocila++;
           oddajNarocilo(evt);
       }
       
    }//GEN-LAST:event_koncajNarociloButtonActionPerformed

    private void nacinPlacilaComboBoxnacinPlacilaCombo(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nacinPlacilaComboBoxnacinPlacilaCombo
        switch(nacinPlacilaComboBox.getSelectedItem().toString()) {
            case "Kartica": spremeniFunkcionalnostField(true); break;
            default: spremeniFunkcionalnostField(false); break;
        }
    }//GEN-LAST:event_nacinPlacilaComboBoxnacinPlacilaCombo

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
      System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void avtorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avtorMenuItemActionPerformed
      izpisNapake("Žan Jerič in Tim Lunar", successColor);
    }//GEN-LAST:event_avtorMenuItemActionPerformed

  
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrameGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrameGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrameGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrameGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem avtorMenuItem;
    private javax.swing.JPanel cardPanel1;
    private javax.swing.JPanel cardPanel2;
    private javax.swing.JPanel cardPanel3;
    private javax.swing.JPanel cartItemPanel;
    private javax.swing.JPanel cartItemPanel1;
    private javax.swing.JPanel cartItemPanel2;
    private javax.swing.JPanel cartPanel;
    private javax.swing.JLabel cenaCartLabel;
    private javax.swing.JLabel cenaCartLabel1;
    private javax.swing.JLabel cenaCartLabel2;
    private javax.swing.JLabel cenaIzdelka1;
    private javax.swing.JLabel cenaIzdelka2;
    private javax.swing.JLabel cenaIzdelka3;
    private javax.swing.JLabel cenaLabel;
    private javax.swing.JLabel cenaLabel1;
    private javax.swing.JLabel cenaLabel2;
    private javax.swing.JLabel cenaValueLabel;
    private javax.swing.JOptionPane confirmSaveOptionPane;
    private javax.swing.JButton dodajCartButton;
    private javax.swing.JButton dodajCartButton1;
    private javax.swing.JButton dodajCartButton2;
    private javax.swing.JLabel imageLabel;
    private javax.swing.JLabel imageLabel1;
    private javax.swing.JLabel imageLabel2;
    private javax.swing.JLabel imageLabel3;
    private javax.swing.JLabel imageLabel4;
    private javax.swing.JLabel imageLabel5;
    private javax.swing.JPanel imagePanel;
    private javax.swing.JPanel imagePanel1;
    private javax.swing.JPanel imagePanel2;
    private javax.swing.JPanel imagePanel3;
    private javax.swing.JPanel imagePanel4;
    private javax.swing.JPanel imagePanel5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField karticaCVV;
    private javax.swing.JTextField karticaField1;
    private javax.swing.JTextField karticaField2;
    private javax.swing.JTextField karticaField3;
    private javax.swing.JTextField karticaField4;
    private javax.swing.JLabel kategorijaLabel;
    private javax.swing.JLabel kategorijaLabel1;
    private javax.swing.JLabel kategorijaLabel2;
    private javax.swing.JLabel kolicinaNarocilo1;
    private javax.swing.JLabel kolicinaNarocilo2;
    private javax.swing.JLabel kolicinaNarocilo3;
    private javax.swing.JButton koncajNarociloButton;
    private javax.swing.JLabel koncnaCenaLabel;
    private javax.swing.JPanel koncnoPanel1;
    private javax.swing.JPanel koncnoPanel2;
    private javax.swing.JPanel koncnoPanel3;
    private javax.swing.JPanel koncnoPanelVsota;
    private javax.swing.JLabel kosaricaLabel;
    private javax.swing.JLabel kupecIme;
    private javax.swing.JLabel kupecMail;
    private javax.swing.JLabel kupecNaslov;
    private javax.swing.JPanel kupecPanel;
    private javax.swing.JLabel kupecTelefon;
    private javax.swing.JPanel layerPanel;
    private javax.swing.JComboBox<String> nacinDostaveComboBox;
    private javax.swing.JPanel nacinDostavePanel;
    private javax.swing.JComboBox<String> nacinPlacilaComboBox;
    private javax.swing.JPanel nacinPlacilaPanel;
    private javax.swing.JButton nadaljujNakupButton;
    private javax.swing.JPanel narociloPanel;
    private javax.swing.JPanel narociloPodatkiPanel;
    private javax.swing.JButton nazajButton1;
    private javax.swing.JButton nazajButton2;
    private javax.swing.JLabel nazivCartLabel;
    private javax.swing.JLabel nazivCartLabel1;
    private javax.swing.JLabel nazivCartLabel2;
    private javax.swing.JLabel nazivDelaLabel;
    private javax.swing.JLabel nazivDelaLabel1;
    private javax.swing.JLabel nazivDelaLabel2;
    private javax.swing.JLabel nazivIzdelka1Label;
    private javax.swing.JLabel nazivIzdelka2;
    private javax.swing.JLabel nazivIzdelka3;
    private javax.swing.JLabel opisCartLabel;
    private javax.swing.JLabel opisCartLabel1;
    private javax.swing.JLabel opisCartLabel2;
    private javax.swing.JLabel opisLabel;
    private javax.swing.JLabel opisLabel1;
    private javax.swing.JLabel opisLabel2;
    private javax.swing.JPanel orodnaPanel;
    private javax.swing.JPanel orodnaPanel1;
    private javax.swing.JPanel orodnaPanel2;
    private javax.swing.JButton pocistiKosaricoButton;
    private javax.swing.JButton prikaziKosaricoButton;
    private javax.swing.JPanel productsPanel;
    private javax.swing.JLabel skupnaCenaNarocila;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JSpinner zalogaCartSpinner;
    private javax.swing.JSpinner zalogaCartSpinner1;
    private javax.swing.JSpinner zalogaCartSpinner2;
    private javax.swing.JLabel zalogaLabel;
    private javax.swing.JLabel zalogaLabel1;
    private javax.swing.JLabel zalogaLabel2;
    // End of variables declaration//GEN-END:variables
}
