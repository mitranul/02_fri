/***********************************************************************
 * Module:  NacinPlacila.java
 * Author:  IceMan47
 * Purpose: Defines the Class NacinPlacila
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid 75eeebb2-2bdb-45ce-b98f-9cf3dded00b4 */
public class NacinPlacila {
   /** @pdOid e5aadcd9-965f-4997-8380-c688f26ba1ce */
   private int placiloID;
   /** @pdOid 2317a653-9fba-44b6-81d4-bd767cfa77f0 */
   private String naziv;
   
   /** @pdRoleInfo migr=no name=Narocilo assc=association14 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection narocilo;
   
   NacinPlacila(int id, String naziv) {
       this.placiloID = id;
       this.naziv = naziv;
   }

    public String getNaziv() {
        return naziv;
    }
   
   /** @pdOid 6f8ffc95-fcfc-4cff-a5ed-650dad2a21bc */
   public NacinPlacila vrniNacinPlacila() {
      // TODO: implement
      return null;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newNarocilo */
   public void setNarocilo(java.util.Collection newNarocilo) {
      removeAllNarocilo();
      for (java.util.Iterator iter = newNarocilo.iterator(); iter.hasNext();)
         addNarocilo((Narocilo)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newNarocilo */
   public void addNarocilo(Narocilo newNarocilo) {
      if (newNarocilo == null)
         return;
      if (this.narocilo == null)
         this.narocilo = new java.util.HashSet();
      if (!this.narocilo.contains(newNarocilo))
         this.narocilo.add(newNarocilo);
   }
   
   /** @pdGenerated default remove
     * @param oldNarocilo */
   public void removeNarocilo(Narocilo oldNarocilo) {
      if (oldNarocilo == null)
         return;
      if (this.narocilo != null)
         if (this.narocilo.contains(oldNarocilo))
            this.narocilo.remove(oldNarocilo);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllNarocilo() {
      if (narocilo != null)
         narocilo.clear();
   }

}