/***********************************************************************
 * Module:  Kupec.java
 * Author:  IceMan47
 * Purpose: Defines the Class Kupec
 ***********************************************************************/
package jlavtodeli;
import java.util.*;

/** @pdOid fc107c55-32c0-4daa-bdab-747f3ddc52ff */
public class Kupec {
   /** @pdOid c777b3ab-b6aa-4f1a-a16e-520f2faa26cb */
   private int kupecID;
   /** @pdOid 1d891bbd-681b-4bab-91f6-ebe64c8a44ce */
   private String ime;
   /** @pdOid 92344007-039b-490b-8c35-4b992664e792 */
   private String priimek;
   /** @pdOid 79a72578-3589-4db0-8567-52d527854324 */
   private Date datumRojstva;
   /** @pdOid 68ca0aa1-d286-4443-a9f1-6bb34ac59b0f */
   private String telefon;
   /** @pdOid c3112b01-4cdb-4555-bd41-da9f304742d6 */
   private String email;
   /** @pdOid c12a7870-e3e5-4dec-91ad-8c24d15b000c */
   private String naslov;
   /** @pdOid b0bb4da9-90dc-402a-b132-717bd51dc941 */
   private String uporabniskoIme;
   /** @pdOid 05e59393-6454-4667-88ed-6eda8ae602bd */
   private String geslo;
   
   /** @pdRoleInfo migr=no name=Narocilo assc=association3 coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection narocilo;

    public int getKupecID() {
        return kupecID;
    }
   
    public String getIme() {
        return ime;
    }

    public String getPriimek() {
        return priimek;
    }

    public String getEmail() {
        return email;
    }

    public String getNaslov() {
        return naslov;
    }

    public String getTelefon() {
        return telefon;
    }
   
   Kupec(int id, String ime, String priimek, Date datum, String telefon, String mail, String naslov) {
       this.kupecID = id;
       this.ime = ime;
       this.priimek = priimek;
       this.datumRojstva = datum;
       this.telefon = telefon;
       this.email = mail;
       this.naslov = naslov;
   }
   
   /** @pdOid a91faf18-9b8c-4ebf-a9c0-d94cd965b4f9 */
   public Map<Integer, Kupec> vrniSeznamKupcev() {
      // TODO: implement
      return null;
   }
   
   /** @pdOid 13656324-7914-4b0f-9f88-698449c89014 */
   public Kupec vrniDolocenegaKupca() {
      // TODO: implement
      return null;
   }
   
   /** @pdOid 425c1cdc-50ed-47e3-b326-3c6864bf0be2 */
   public void dodajKupca() {
      // TODO: implement
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection getNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorNarocilo() {
      if (narocilo == null)
         narocilo = new java.util.HashSet();
      return narocilo.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newNarocilo */
   public void setNarocilo(java.util.Collection newNarocilo) {
      removeAllNarocilo();
      for (java.util.Iterator iter = newNarocilo.iterator(); iter.hasNext();)
         addNarocilo((Narocilo)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newNarocilo */
   public void addNarocilo(Narocilo newNarocilo) {
      if (newNarocilo == null)
         return;
      if (this.narocilo == null)
         this.narocilo = new java.util.HashSet();
      if (!this.narocilo.contains(newNarocilo))
         this.narocilo.add(newNarocilo);
   }
   
   /** @pdGenerated default remove
     * @param oldNarocilo */
   public void removeNarocilo(Narocilo oldNarocilo) {
      if (oldNarocilo == null)
         return;
      if (this.narocilo != null)
         if (this.narocilo.contains(oldNarocilo))
            this.narocilo.remove(oldNarocilo);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllNarocilo() {
      if (narocilo != null)
         narocilo.clear();
   }

}