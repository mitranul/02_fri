
import java.io.*;
import java.nio.Buffer;
import java.util.*;
import java.lang.*;

public class Gusarji {
    private static int steviloPrimerov = 0;
    private static int steviloParovDvojckov = 0;
    private static int ponovitevPara = 0;
    private static int steviloVprasanj = 0;
    private static ArrayList<String> tabelaVnosa = new ArrayList<>();
    private static StringBuilder zaporedjePara1 = new StringBuilder();
    private static StringBuilder koncnoZaporedje1 = new StringBuilder();
    private static int steviloCrkeS = 1;
    private static int steviloGusarjevX = 0;
    private static Iterator<String> iterator;


    private static StringBuilder izvediOperacijo(StringBuilder zaporedje, String crka, int a, int b) {
        switch(crka) {
            case "F":
                for(int i = a; i <= b; i++)
                    zaporedje.setCharAt(i, '1');
                break;
            case "E":
                for(int i = a; i <= b; i++)
                    zaporedje.setCharAt(i, '0');
                break;
            case "I":
                for(int i = a; i <= b; i++) {
                    if(zaporedje.charAt(i) == '1')
                        zaporedje.setCharAt(i, '0');
                    else if(zaporedje.charAt(i) == '0')
                        zaporedje.setCharAt(i, '1');
                }
                break;
            case "S":
                steviloGusarjevX = 0;
                for(int i = a; i <= b; i++)
                    if(zaporedje.charAt(i) == '1')
                        steviloGusarjevX++;
                System.out.println("Q" + steviloCrkeS + ": " + steviloGusarjevX);
                steviloCrkeS++;
                break;
            default:
                System.err.println("Napaka. Črke so lahko samo F, E, I in S.");
        }
        return zaporedje;
    }

    private static void readInput(String fileName) {
        try{
            File file = new File(fileName);
            Scanner scanner = new Scanner(file);
            steviloPrimerov = Integer.parseInt(scanner.nextLine().trim());

            while(scanner.hasNextLine()) {
                tabelaVnosa.add(scanner.nextLine().trim());
            }
            scanner.close();
        }catch(FileNotFoundException e) {
            System.out.println("File not found " + e);
            System.exit(1);
        }
    }

    public static void main(String[] args)
    {
        readInput("C:\\Users\\IceMan47\\Documents\\bitBucket\\02_fri\\APS2\\seminar2\\Test cases\\3.IN");
        iterator = tabelaVnosa.iterator();

        for(int i = 0; i < steviloPrimerov; i++) {
            steviloParovDvojckov = Integer.parseInt(iterator.next());
            for(int j = 0; j < steviloParovDvojckov; j++) {
                ponovitevPara = Integer.parseInt(iterator.next());
                zaporedjePara1 = new StringBuilder(iterator.next());
                for(int k = 0; k < ponovitevPara; k++)
                    koncnoZaporedje1.append(zaporedjePara1);
            }
            steviloVprasanj = Integer.parseInt(iterator.next());
            System.out.println("Case " + (i+1) + ":");
            for(int k = 0; k < steviloVprasanj; k++) {
                String[] vrstica = iterator.next().split(" ");
                koncnoZaporedje1 = izvediOperacijo(koncnoZaporedje1, vrstica[0].trim(), Integer.parseInt(vrstica[1].trim()), Integer.parseInt(vrstica[2].trim()));
            }
            koncnoZaporedje1 = new StringBuilder();
            steviloCrkeS = 1;
        }
    }
}