package si.unilj.fri.vss.aps2.seminar1;

import java.util.List;
import java.util.Timer;


/**
 * An example implementation of the placement algorithm using a very
 * simple greedy algorithm.
 */
public class DummyPlacementAlgorithm extends PlacementAlgorithm {

    public DummyPlacementAlgorithm(Board board, List<PuzzlePiece> puzzlePieces) {
        super(board, puzzlePieces);
    }

    private float maxValue = 0;
    private Timer timer =  new Timer();

    @Override
    public void placePuzzlePieces() {
        board.clear();
        myProgram(0);
    }

    public void myProgram(int p) {
        if(p == -1) {
            System.err.println(String.format("Final filled: %.2f%%", this.evaluate() * 100.0));
            System.exit(0);
        }

        for(PuzzlePiece piece : puzzlePieces) {
            for(int i = 0; i < (board.height * board.width); i++) {
                int x = i - ((int) (Math.floor(i / board.width)) * board.width);
                int y = (int) (Math.floor(i / board.width));

                if(board.tryPlacePuzzlePiece(piece, board.new Offset(x, y))) {
                    if(this.evaluate() * 100 > maxValue) {
                        maxValue = this.evaluate() * 100;
                        System.out.println(String.format("Current filled: %.2f%%", maxValue));
                        board.printBoard();
                        if(maxValue >= 92.0) p = -1;
                    }
                    myProgram(p);
                    board.removeLastPuzzlePiece();
                }
            }
        }
    }
}