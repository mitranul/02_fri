import java.util.Scanner;

interface Collection {
    static final String ERR_MSG_EMPTY = "Collection is empty.";
    static final String ERR_MSG_FULL = "Collection is full.";

    boolean isEmpty();
    boolean isFull();
    int size();
    String toString();
}

class CollectionException extends Exception {
    public CollectionException(String msg) {
        super(msg);
    }
}

interface Stack1 extends Collection {
    String top() throws CollectionException;
    void push(String x) throws CollectionException;
    String pop() throws CollectionException;
}

interface Sequence1 extends Collection {
    static final String ERR_MSG_INDEX = "Wrong index in sequence.";

    Stack get(int i) throws CollectionException;

    void add(Stack x) throws CollectionException;
}

class Stack implements Stack1 {
    private static final int DEFAULT_CAPACITY = 64;
    private String[] a;
    private int front, back, size;

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == DEFAULT_CAPACITY;
    }

    @Override
    public int size() {
        return size;
    }

    private int next(int index) {
        return (index + 1) % DEFAULT_CAPACITY;
    }

    private int prev(int index) {
        return(DEFAULT_CAPACITY + index - 1) % DEFAULT_CAPACITY;
    }


    @SuppressWarnings("unchecked")
    public Stack() {
        a = new String[DEFAULT_CAPACITY];
        front = 0;
        back = 0;
        size = 0;
    }

    @Override
    public String top() throws CollectionException {
        if(isEmpty()) {
            throw new CollectionException(ERR_MSG_EMPTY);
        }
        return a[prev(back)];
    }

    @Override
    public void push(String x) throws CollectionException {
        if(isFull())
            throw new CollectionException(ERR_MSG_FULL);
        a[back] = x;
        back = next(back);
        size++;
    }

    @Override
    public String pop() throws CollectionException {
        if(isEmpty())
            throw new CollectionException(ERR_MSG_EMPTY);
        back = prev(back);
        String objekt = a[back];
        a[back] = null;     //povozimo element - izbrisemo
        size --;
        return objekt;
    }
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        // sb.append("[");
        if(size > 0)
            sb.append(a[front].toString());

        for(int i = 0; i < size - 1; i++)
            sb.append(" " + a[next(front + i)]);
        // sb.append("]");
        return sb.toString();
    }

    public void reverse() {
        String[] arr = new String[size];
        for(int i = size-1, j=0; i >= 0; i--, j++)
            arr[j] = a[i];

        for(int i = 0; i < size; i++)
            a[i] = arr[i];
    }
}

class Sequence implements Sequence1 {
    private static final int DEFAULT_CAPACITY = 42;
    private Stack[] a;
    private int front, back, size;

    @SuppressWarnings("unchecked")
    public Sequence() {
        a = new Stack[DEFAULT_CAPACITY];
        front = 0;
        back = 0;
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == DEFAULT_CAPACITY;
    }

    @Override
    public int size() {
        return size;
    }

    private int next(int index) {
        return (index + 1) % DEFAULT_CAPACITY;
    }

    private int prev(int index) {
        return(DEFAULT_CAPACITY + index - 1) % DEFAULT_CAPACITY;
    }

    private int index(int index) {
        return (front + index) % DEFAULT_CAPACITY;
    }

    @Override
    public void add(Stack x) throws CollectionException {
        if(isFull())
            throw new CollectionException(ERR_MSG_FULL);
        a[back] = x;
        back = next(back);
        size++;
    }

    @Override
    public Stack get(int i) throws CollectionException {
        if(i < 0 || i >= DEFAULT_CAPACITY)
            throw new CollectionException(ERR_MSG_INDEX);
        return a[index(i)];
    }

    public void clearSequence(int index) throws CollectionException {
        a[index] = new Stack();
    }
}

@SuppressWarnings("all")
class Calculator {
    static Sequence sequence = new Sequence();
    static private boolean condition = false;
    static private boolean commandDisorder = false;
    static private int counter = 0;
    static private boolean notAddRunEl = false;

    Calculator() throws CollectionException {
        for(int i = 0; i < 42; i++) {
            Stack stack = new Stack();
            sequence.add(stack);
        }
    }

    public boolean isNumber(String value) {
        try {
            int d = Integer.parseInt(value);
        }catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }

    public void mainStackNumericOperations(String value) throws CollectionException {
        int f1 = Integer.parseInt(sequence.get(0).pop());
        int f2 = Integer.parseInt(sequence.get(0).pop());
        int rez = 0;
        switch(value) {
            case "+": rez = f2 + f1; break;
            case "-": rez = f2 - f1; break;
            case "*": rez = f2 * f1; break;
            case "/": rez = f2 / f1; break;
            case "%": rez = f2 % f1; break;
        }
        sequence.get(0).push(Integer.toString(rez));
    }

    public void mainStackLogicalOperations(String value) throws CollectionException {
        String f1 = sequence.get(0).pop();
        String f2 = sequence.get(0).pop();
        boolean state = false;
        switch(value) {
            case "<": {
                if(isNumber(f1) && isNumber(f1))
                    state = (Integer.parseInt(f2) < Integer.parseInt(f1));
                else
                    state = (f2.compareTo(f1) < 0);
            }; break;
            case ">": {
                if(isNumber(f1) && isNumber(f1))
                    state = (Integer.parseInt(f2) > Integer.parseInt(f1));
                else
                    state = (f2.compareTo(f1) > 0);
            }; break;
            case "<=": {
                if(isNumber(f1) && isNumber(f1))
                    state = (Integer.parseInt(f2) <= Integer.parseInt(f1));
                else
                    state = (f2.compareTo(f1) <= 0);
            }; break;
            case ">=": {
                if(isNumber(f1) && isNumber(f1))
                    state = (Integer.parseInt(f2) >= Integer.parseInt(f1));
                else
                    state = (f2.compareTo(f1) >= 0);
            }; break;
            case "==": {
                if(isNumber(f1) && isNumber(f1))
                    state = (Integer.parseInt(f2) == Integer.parseInt(f1));
                else
                    state = (f2.equals(f1));
            }; break;
            case "<>": {
                if(isNumber(f1) && isNumber(f1))
                    state = !(Integer.parseInt(f2) == Integer.parseInt(f1));
                else
                    state = !(f2.equals(f1));
            }; break;
        }
        if(state)
            sequence.get(0).push("1");
        else
            sequence.get(0).push("0");
    }

    public void mainStackUse(String value) throws CollectionException {
        String top;
        if(sequence.get(0).isEmpty())
            top = "";
        else
            top = sequence.get(0).top();
        switch(value) {
            case "echo": {
                if(sequence.get(0).size() != 0)
                    System.out.println(top);
                else
                    System.out.println();
            }; break;
            case "pop": sequence.get(0).pop(); break;
            case "dup": sequence.get(0).push(top); break;
            case "dup2": {
                sequence.get(0).pop();
                String e_top = sequence.get(0).pop();
                sequence.get(0).push(e_top);
                sequence.get(0).push(top);
                sequence.get(0).push(e_top);
                sequence.get(0).push(top);
            }; break;
            case "swap": {
                sequence.get(0).pop();
                String e_top = sequence.get(0).pop();
                sequence.get(0).push(top);
                sequence.get(0).push(e_top);
            }; break;
        }
    }

    public void mainStackTopOperators(String value) throws CollectionException {
        String top = sequence.get(0).pop();

        switch(value) {
            case "char": {
                sequence.get(0).push((char)Integer.parseInt(top) + "");
            }; break;
            case "even": {
                if(Math.abs(Integer.parseInt(top)) % 2 == 0)
                    sequence.get(0).push("1");
                else
                    sequence.get(0).push("0");
            }; break;
            case "odd": {
                if(Math.abs(Integer.parseInt(top)) % 2 == 1)
                    sequence.get(0).push("1");
                else
                    sequence.get(0).push("0");
            }; break;
            case "!": {
                int fakulteta = 1;
                for(int i = 1; i <= Integer.parseInt(top); i++)
                    fakulteta *= i;
                sequence.get(0).push(fakulteta+"");
            }; break;
            case "len": sequence.get(0).push(top.length()+""); break;
        }
    }

    public void mainStackOtherOperactions(String value) throws CollectionException {
        String f1 = sequence.get(0).pop();
        String f2 = sequence.get(0).pop();
        switch(value) {
            case ".": {
                sequence.get(0).push(f2 + f1);
            }; break;
            case "rnd": {
                int random =(int)((Math.random() * (Integer.parseInt(f1) - Integer.parseInt(f2))) + Integer.parseInt(f2));
                sequence.get(0).push(random+"");
            }
        }
    }

    public void mainStackConditionOperators(String value) throws CollectionException {
        switch(value) {
            case "then": {
                String top = sequence.get(0).pop();
                condition = !(top.equals("0"));
            }; break;
            case "else": condition = !condition; break;
        }
    }

    public void optionalStackOperators(String value, String[] line, int lineIndex) throws CollectionException {
        int index = Integer.parseInt(sequence.get(0).pop());
        switch(value) {
            case "print": {
                System.out.println(sequence.get(index).toString());
            }; break;
            case "clear": {
                sequence.clearSequence(index);
            }; break;
            case "run": {
                String vrstica = sequence.get(index).toString();
                Scanner sc = new Scanner(vrstica);

                while(sc.hasNext()) {
                    int indexNew = 0;
                    readSwitch(vrstica.split(" "), sc.next(), indexNew++);
                }
            }; break;
            case "loop": {
                int e_top = Integer.parseInt(sequence.get(0).pop());
                String vrstica = sequence.get(index).toString();

                for(int i = 0; i < e_top; i++) {
                    Scanner sc = new Scanner(vrstica);
                    int indexNew = 0;
                    while(sc.hasNext()) {
                        readSwitch(vrstica.split(" "), sc.next(), indexNew++);
                    }
                }
            }; break;
            case "fun": {
                int e_top = Integer.parseInt(sequence.get(0).pop());
                int j = 0;

                for(int i = lineIndex; i < line.length; i++) {
                    if(line[i].equals("fun")) {
                        while(j < e_top) {
                            sequence.get(index).push(line[i+j+1]);
                            j++;
                        }
                    }
                }
                commandDisorder = true;
                counter = e_top;
            }; break;
            case "move": {
                int e_top = Integer.parseInt(sequence.get(0).pop());
                String curr;
                for(int i = 0; i < e_top; i++) {
                    curr = sequence.get(0).pop();
                    sequence.get(index).push(curr);
                }
            }; break;
            case "reverse": {
                sequence.get(index).reverse();
            }; break;
        }
    }

    public void readSwitch(String[] line, String lineElement, int i) throws CollectionException {
        if(lineElement.startsWith("?")) {
            if(condition) {
                lineElement = lineElement.substring(1);
            }
            else
                return;
        }

        switch(lineElement) {
            case "+":
            case "-":
            case "*":
            case "/":
            case "%": mainStackNumericOperations(lineElement); break;

            case "<":
            case ">":
            case "<=":
            case ">=":
            case "==":
            case "<>": mainStackLogicalOperations(lineElement); break;

            case "echo":
            case "pop":
            case "dup":
            case "dup2":
            case "swap": mainStackUse(lineElement); break;

            case "char":
            case "even":
            case "odd":
            case "!":
            case "len": mainStackTopOperators(lineElement); break;

            case "rnd":
            case ".": mainStackOtherOperactions(lineElement); break;

            case "then":
            case "else":mainStackConditionOperators(lineElement); break;

            case "print":
            case "clear":
            case "run":
            case "loop":
            case "fun":
            case "move":
            case "reverse": optionalStackOperators(lineElement, line, i); break;

            case " ":
            case "": break;

            default: sequence.get(0).push(lineElement);
        }
    }

    public void branje() throws CollectionException {
        Scanner sc = new Scanner(System.in);
        String[] line;
        while(sc.hasNextLine()) {
            line = sc.nextLine().split(" ");
            for(int i = 0; i < line.length; i++) {

                readSwitch(line, line[i], i);

                if(commandDisorder) {
                    i += counter;
                    commandDisorder = false;
                }
            }
            sequence = new Sequence();
            for(int i = 0; i < 42; i++) {
                Stack stack = new Stack();
                sequence.add(stack);
            }
            condition = false;
        }
    }
}

@SuppressWarnings("unchecked")
public class Naloga1 {
    public static void main(String[] args) throws CollectionException {
        Calculator calc = new Calculator();
        calc.branje();
    }
}
