public class Izziv2 {

    static int[] generateTable(int n) {
        int[] tabela = new int[n];
        for(int i = 0; i < n; i++)
            tabela[i] = i+1;
        return tabela;
    }

    static int findLinear(int[] a, int v) {
        for(int i = 0; i < a.length; i++)
            if(a[i] == v)
                return i;
        return -1;
    }

    static int findBinary(int[] a, int l, int r, int v) {
        int avg = (l + r) / 2;
        if(r < l)
            return -1;
        if(a[avg] > v)
            return findBinary(a, l, avg - 1, v);
        if(a[avg] < v)
            return findBinary(a, avg + 1, r, v);
        return avg;
    }

    static long timeLinear(int n){
        int[] tabela = generateTable(n);

        long startT = System.nanoTime();
        for(int i = 0; i < 1000; i++)
            findLinear(tabela, (int)(Math.random() * n + 1));

        return (System.nanoTime() - startT) / 1000;
    }

    static long timeBinary(int n) {
        int[] tabela = generateTable(n);

        long startT = System.nanoTime();
        for(int i = 0; i < 1000; i++)
            findBinary(tabela, 0, n-1, (int)(Math.random() * n + 1));
        return (System.nanoTime() - startT) / 1000;
    }

    public static void main(String[] args) {
        System.out.println("   n     |     linearno |   dvojisko |");
        System.out.println("---------+--------------+---------------");
        for(int i = 100000; i <= 1000000; i += 10000) {
            System.out.printf("%8s | %12s | %10s \n", i, timeLinear(i), timeBinary(i));
        }
    }
}
