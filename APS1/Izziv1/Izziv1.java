package Izziv1;

public class Izziv1 {
    public static void main(String[] args) throws CollectionException {
        Stack<String> s = new ArrayDeque<String>();
        Deque<String> d = new ArrayDeque<String>();
        Sequence<String> z = new ArrayDeque<String>();
        s.push("ABC");
        s.push("DEF");
        s.push("GHI");

        System.out.println("Stack: ");
        while(!s.isEmpty()) {
            System.out.print(s.top() + " ");
            d.enqueueFront(s.pop());
        }

        System.out.println("\n\nDeque: ");
        while(!d.isEmpty()) {
            System.out.print(d.back() + " ");
            z.add(d.dequeueBack());
        }

        System.out.println("\n\nSequence: ");
        for(int i = 0; i < z.size(); i++) {
            System.out.print(z.get(i) + " ");
        }
    }
}
