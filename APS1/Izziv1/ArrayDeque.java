package Izziv1;

class ArrayDeque<T> implements Deque<T>, Stack<T>, Sequence<T> {
    private static final int DEFAULT_CAPACITY = 64;
    private T[] a;
    private int front, back, size;

    public ArrayDeque() {
        a = (T[])(new Object[DEFAULT_CAPACITY]);
        front = 0;
        back = 0;
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == DEFAULT_CAPACITY;
    }

    @Override
    public int size() {
        return size;
    }

    private int next(int index) {
        return (index + 1) % DEFAULT_CAPACITY;
    }

    private int prev(int index) {
        return(DEFAULT_CAPACITY + index - 1) % DEFAULT_CAPACITY;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        if(size > 0)
            sb.append(a[front].toString());

        for(int i = 0; i < size - 1; i++)
            sb.append(", " + a[next(front + i)]);
        sb.append("]");
        return sb.toString();
    }

    // STACK
    @Override
    public T top() throws CollectionException {
        if(isEmpty()) {
            throw new CollectionException(ERR_MSG_EMPTY);
        }
        return a[prev(back)];
    }

    @Override
    public void push(T x) throws CollectionException {
        if(isFull())
            throw new CollectionException(ERR_MSG_FULL);
        a[back] = x;
        back = next(back);
        size++;
    }

    @Override
    public T pop() throws CollectionException {
        if(isEmpty())
            throw new CollectionException(ERR_MSG_EMPTY);
        back = prev(back);
        T objekt = a[back];
        a[back] = null;     //povozimo element - izbrisemo
        size --;
        return objekt;
    }

    /** DEQUE **/
    // front -->
    @Override
    public T front() throws CollectionException {
        if(isEmpty()) {
            throw new CollectionException(ERR_MSG_EMPTY);
        }
        return a[front];
    }

    // back --> ista operacija kot top
    @Override
    public T back() throws CollectionException {
        return top();
    }
    // enqueue --> ista operacija kot push
    @Override
    public void enqueue(T x) throws CollectionException {
        push(x);
    }
    // enqueueFront --> ali je dovolj prostora? --> ce je... premaknes front naprej
    @Override
    public void enqueueFront(T x) throws CollectionException {
        if(isFull())
            throw new CollectionException(ERR_MSG_FULL);
        front = prev(front);
        a[front] = x;
        size ++;
    }
    // dequeue --> izločanje elementa z začetka (kej elementov gor)?
    @Override
    public T dequeue() throws CollectionException {
        if(isEmpty())
            throw new CollectionException(ERR_MSG_EMPTY);
        T objekt = a[front];
        a[front] = null;
        size--;
        front = next(front);
        return objekt;
    }

    // dequeueBack --> isto kot pop
    @Override
    public T dequeueBack() throws CollectionException {
        return pop();
    }


    /** SEQUENCE **/
    private int index(int index) {
        return (front + index) % DEFAULT_CAPACITY;
    }

    @Override
    public void add(T x) throws CollectionException {
        push(x);
    }

    @Override
    public T get(int i) throws CollectionException {
        if(i < 0 || i >= DEFAULT_CAPACITY)
            throw new CollectionException(ERR_MSG_INDEX);
        return a[index(i)];
    }
}
