import java.util.Arrays;
import java.util.Scanner;

class SortingAlghoritems {
    static private int[] sortingTable;
    static private String[] line;
    static private int tableSize;
    static private int stepCounter;
    static private int changeCounter;
    static boolean firstRow = true;
    static String operation = "";
    static String sortingAlg = "";
    static String tidiness = "";
    static private int tableIndex = 0;

    SortingAlghoritems() throws Exception {
        this.stepCounter = 0;
        this.changeCounter = 0;
    }

    static void output(int index) {
        for(int m = 0; m < line.length; m++)
            if(m == index)
                System.out.print(line[m] + " | ");
            else
                System.out.print(line[m] + " ");
        System.out.println();
    }

    static void outputQuickSort(int pivotIndex, int left, int right) {
        for(int i = left; i <= right; i++) {
            if(i == pivotIndex)
                System.out.print("| " + line[pivotIndex] + " | ");
            else
                System.out.print(line[i] + " ");
        }
        System.out.println();
    }

    static void outputRadixSort() {
        for(String element : line)
            System.out.print(element + " ");
        System.out.println();
    }

    static void swap(int i, int m) {
        String tmp = line[i];
        line[i] = line[m];
        line[m] = tmp;
        changeCounter += 3;
    }

    static void insertSorting() throws Exception {
        int temp;
        switch(tidiness) {
            case "down": {
                for(int i = 1, j = i; i < line.length; i++, j = i) {
                    changeCounter += 2;
                    temp = Integer.parseInt(line[i]);
                    while(j > 0) {
                        stepCounter++;
                        if(Integer.parseInt(line[j - 1]) < temp) {
                            line[j] = line[j-1];
                            changeCounter++;
                            --j;
                        }
                        else
                            break;
                    }
                    line[j] = temp + "";
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
            case "up": {
                for(int i = 1; i < line.length; i++) {
                    int j = i;
                    temp = Integer.parseInt(line[i]);
                    changeCounter += 2;

                    while(j > 0) {
                        stepCounter++;
                        if(Integer.parseInt(line[j - 1]) > temp) {
                            line[j] = line[j-1];
                            j = j - 1;
                            changeCounter++;
                        }
                        else
                            break;
                    }
                    line[j] = temp + "";
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
        }

    }

    static void selectSorting() throws Exception {
        int minIndex = 0, maxIndex = 0;
        switch(tidiness) {
            case "down": {
                for(int i = 0; i < line.length - 1; i++, maxIndex = i) {
                    for(int j = i + 1; j < line.length; j++) {
                        stepCounter++;
                        if(Integer.parseInt(line[j]) > Integer.parseInt(line[maxIndex]))
                            maxIndex = j;
                    }
                    swap(i, maxIndex);
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
            case "up": {
                for(int i = 0; i < line.length - 1; i++, minIndex = i) {
                    for(int j = i + 1; j < line.length; j++) {
                        stepCounter++;
                        if(Integer.parseInt(line[j]) < Integer.parseInt(line[minIndex]))
                            minIndex = j;
                    }
                    swap(i, minIndex);
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
        }
    }

    static void bubbleSorting() throws Exception {
        int lastSwapIndex = line.length - 2;
        switch(tidiness) {
            case "down": {
                for(int i = 0; i < line.length - 1; i++, lastSwapIndex = line.length - 2) {
                    for(int j = line.length - 1; j > i; j--, stepCounter++) {
                        if(Integer.parseInt(line[j]) > Integer.parseInt(line[j-1])) {
                            lastSwapIndex = j - 1;
                            swap(j, j - 1);
                        }
                    }
                    i = lastSwapIndex;
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
            case "up": {
                for(int i = 0; i < line.length - 1; i++, lastSwapIndex = line.length - 2) {
                    for(int j = line.length - 1; j > i; j--, stepCounter++) {
                        if(Integer.parseInt(line[j]) < Integer.parseInt(line[j-1])) {
                            lastSwapIndex = j - 1;
                            swap(j, j - 1);
                        }
                    }
                    i = lastSwapIndex;
                    if(operation.equals("trace"))
                        output(i);
                }
            } break;
        }
    }

    static void heapSwap(int i, int j, int n) {
        swap(i, j);
        if(tidiness.equals("down"))
            siftUp(n, j);
        else
            siftDown(n, j);
    }

    static void siftDown(int n, int i) {
        int father = i;
        int left = 2 * father + 1;
        int right = 2 * father + 2;

        if ((left < n) && (Integer.parseInt(line[left]) > Integer.parseInt(line[father]))) {
            father = left;
            stepCounter++;
        }
        if ((right < n) && (Integer.parseInt(line[right]) > Integer.parseInt(line[father]))) {
            father = right;
            stepCounter++;
        }
        if (father != i) {
            heapSwap(i, father, n);
            stepCounter++;
        }
    }

    static void siftUp(int n, int i) {
        int father = i;
        int left = 2 * father + 1;
        int right = 2 * father + 2;

        if ((left < n) && (Integer.parseInt(line[left]) < Integer.parseInt(line[father]))) {
            father = left;
            stepCounter++;
        }
        if ((right < n) && (Integer.parseInt(line[right]) < Integer.parseInt(line[father]))) {
            father = right;
            stepCounter++;
        }
        if (father != i) {
            heapSwap(i, father, n);
            stepCounter++;
        }
    }

    static void heapSorting() throws Exception {
        switch(tidiness) {
            case "down": {
                for (int i = line.length / 2 - 1; i >= 0; i--)
                    siftUp(line.length, i);
                if(operation.equals("trace"))
                    output(line.length - 1);

                int i = line.length - 1;
                while(i >= 0) {
                    siftUp(i+1, 0);
                    if(i != line.length - 1 && operation.equals("trace"))
                        output(i);
                    swap(0, i);
                    i--;
                }
                changeCounter -= 3;
            } break;
            case "up": {
                for (int i = line.length / 2 - 1; i >= 0; i--)
                    siftDown(line.length, i);
                if(operation.equals("trace"))
                    output(line.length - 1);

                int i = line.length - 1;
                while(i >= 0) {
                    siftDown(i+1, 0);
                    if(i != line.length - 1 && operation.equals("trace"))
                        output(i);
                    swap(0, i);
                    i--;
                }
                changeCounter -= 3;
            } break;
        }
    }

    static void mergeSorting() throws Exception {

    }

    static int partitionUp(int left, int right) {
        int leftElement = Integer.parseInt(line[left]);
        int l = left, r = right + 1;

        while(true) {
            do {
                l++;
                if(!(l < right)) break;
            }while(Integer.parseInt(line[l]) < leftElement);

            do {
                r--;
            }while(Integer.parseInt(line[r]) > leftElement);
            if(l >= r) break;
            swap(l, r);
        }
        swap(left, r);  // pivot zamenjamo tam kjer je levi oz. desni kazalec
        return r;
    }

    static int partitionDown(int left, int right) {
        int leftElement = Integer.parseInt(line[left]);
        int l = left, r = right + 1;

        while(true) {
            do {
                l++;
                if(!(l < right)) break;
            }while(Integer.parseInt(line[l]) > leftElement);

            do {
                r--;
            }while(Integer.parseInt(line[r]) < leftElement);
            if(l >= r) break;
            swap(l, r);
        }
        swap(left, r);  // pivot zamenjamo tam kjer je levi oz. desni kazalec
        return r;
    }

    static void quickSort(int left, int right) {
        int pivotIndex = 0;
        if(left >= right) return;
        switch(tidiness) {
            case "down": {
                pivotIndex = partitionDown(left, right);
            } break;
            case "up": {
                pivotIndex = partitionUp(left, right);
            } break;
        }
        if(operation.equals("trace"))
            outputQuickSort(pivotIndex, left, right);
        quickSort(left, pivotIndex - 1);
        quickSort(pivotIndex + 1, right);
    }

    static void quickSorting() throws Exception {
        int left = 0, right = line.length - 1;
        quickSort(left, right);
        for(String element : line)
            System.out.print(element + " ");
    }

    static int findMaxElement(int maxElement) {
        for(String element : line)
            if(Integer.parseInt(element) > maxElement)
                maxElement = Integer.parseInt(element);
        return maxElement;
    }

    static void radixSorting() throws Exception {
        int index, element;

        switch(tidiness) {
            case "down": {
            } break;
            case "up": {
                for (int decimalValues = 1, i; findMaxElement(Integer.parseInt(line[0])) / decimalValues > 0; decimalValues *= 10) {
                    int[] comulativeTable = new int[line.length];
                    int[] numberOfNumbers = new int[10];

                    for (i = 0; i < line.length; i++) {
                        index = (Integer.parseInt(line[i]) / decimalValues) % 10;
                        ++numberOfNumbers[index];
                    }
                    for (i = 1; i < 10; i++)
                        numberOfNumbers[i] += numberOfNumbers[i-1];

                    for (i = line.length - 1; i >= 0; i--) {
                        index = (Integer.parseInt(line[i]) / decimalValues) % 10;
                        element = numberOfNumbers[index];
                        comulativeTable[element - 1] = Integer.parseInt(line[i]);
                        --numberOfNumbers[index];
                    }
                    outputRadixSort();
                    for (i = 0; i < line.length; i++)
                        line[i] = comulativeTable[i] + "";
                }
            } break;
        }
        for(String el : line)
            System.out.print(el + " ");
    }

    static void bucketSorting() throws Exception {

    }

    static void readSwitch() throws Exception {
        switch(sortingAlg) {
            case "insert": insertSorting(); break;
            case "select": selectSorting(); break;
            case "bubble": bubbleSorting(); break;
            case "heap": heapSorting(); break;
            case "merge": mergeSorting(); break;
            case "quick": quickSorting(); break;
            case "radix": radixSorting(); break;
            case "bucket": bucketSorting();break;
        }
    }

    static void readCountSwitch() throws Exception {
        for(int i = 0; i < 3; i++) {
            stepCounter = 0;
            changeCounter = 0;
            if(i == 2)
                if(tidiness.equals("down"))
                    tidiness = "up";
                else
                    tidiness = "down";
            switch(sortingAlg) {
                case "insert": insertSorting(); break;
                case "select": selectSorting(); break;
                case "bubble": bubbleSorting(); break;
                case "heap": heapSorting(); break;
                case "merge": mergeSorting(); break;
                case "quick": quickSorting(); break;
                case "radix": radixSorting(); break;
                case "bucket": bucketSorting();break;
            }
            if(i == 2)
                System.out.print(changeCounter + " " + stepCounter);
            else
                System.out.print(changeCounter + " " + stepCounter + " | ");
        }
    }

    static void readSequence() throws  Exception {
        String vrsta;
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()) {
            vrsta = sc.nextLine().replace("  ", " ");

            line = vrsta.split(" ");
            if(firstRow) {
                operation = line[0];
                sortingAlg = line[1];
                tidiness = line[2];
                firstRow = false;
            }
            else {
                switch(operation) {
                    case "trace": {
                        if(!sortingAlg.equals("radix"))
                            System.out.println(vrsta);
                        readSwitch();
                    } break;
                    case "count": readCountSwitch(); break;
                }
            }
        }
    }
}


public class Naloga2 {
    public static void main(String[] args) throws Exception {
        SortingAlghoritems.readSequence();
    }
}
