package Levels;

import Audio.AudioSource;
import Audio.MainAudio;
import Fonts.FontType;
import Fonts.GUIText;
import Models.TexturedModel;
import Objects.*;
import Renderers.GuiRenderer;
import Renderers.MainRenderer;
import Textures.GuiTexture;
import Textures.ModelTexture;
import Textures.TerrainTexture;
import Textures.TerrainTexturePack;
import org.lwjgl.Sys;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import sun.applet.Main;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Level {
    public ObjectLoader loader = new ObjectLoader();
    public int currentCoinIndex = 0;
    public int currentLevel;
    private int coinNumber;
    private Random rnd = new Random();

    /** ELEMENTS POSITION **/
    private int playerStartCoordX;
    private int playerStartCoordZ;
    private int playerStartRotY;
    private int playerStartScale;
    private int[] coinCordX;
    private int[] coinCordZ;

    /** TERRAIN VARIABLES **/
    private String background;
    private String rText;
    private String gText;
    private String bText;
    private String mapName;
    private String rendererMapName;
    //private String carModel;

    /** GENERATING TERRAIN CONFIGURATIONS **/
    private TerrainTexture backgroundTexture;
    private TerrainTexture rTexture;
    private TerrainTexture gTexture;
    private TerrainTexture bTexture;
    private TerrainTexturePack texturePack;
    private TerrainTexture blendMap;

    /** GENERATING TEXTURE MODELS & GUI TEXTURES**/
   private LoadModels loadModels;

    public List<GuiTexture> guis;
    public GuiTexture menuGui;
    public GuiTexture scoreBoard;
    public GuiTexture gameoverGui;
    public GuiTexture winGui;
    public GuiRenderer guiRenderer;

    /** GENERATING SETTINGS FOR FONT **/
    public FontType font;
    public GUIText text;

    /** CONSTRUCTOR **/
    public Level(int numbCoin, int currLevel) {
        this.loader = new ObjectLoader();
        this.coinNumber = numbCoin;
        this.currentLevel = currLevel;
        this.background = genLevelTheme()[0];
        this.rText = "dirt";
        this.gText = "start";
        this.bText = "road";
        this.mapName = genLevelTheme()[1];
        this.rendererMapName = genLevelTheme()[2];
        //this.carModel = genLevelTheme()[3];
        this.playerStartCoordX = playerCoordX();
        this.playerStartCoordZ = playerCoordZ();
        this.playerStartRotY = playerRotY();
        this.playerStartScale = playerScale();
        this.coinCordX = tableCoordX();
        this.coinCordZ = tableCoordZ();

        /** GENERATING TERRAIN CONFIGURATIONS **/
        this.backgroundTexture = new TerrainTexture(loader.loadTexture(background));
        this.rTexture = new TerrainTexture(loader.loadTexture(rText));
        this.gTexture = new TerrainTexture(loader.loadTexture(gText));
        this.bTexture = new TerrainTexture(loader.loadTexture(bText));
        this.texturePack = new TerrainTexturePack(backgroundTexture,rTexture,gTexture,bTexture);
        this.blendMap = new TerrainTexture(loader.loadTexture(mapName));

        /** LOAD MODELS **/
        loadModels = new LoadModels(loader,currLevel);

        /** GUI TEXTURES **/
        guis = new ArrayList<GuiTexture>();
        menuGui = new GuiTexture(loader.loadTexture("menu"),new Vector2f(0.0f,0.0f),new Vector2f(1.0f,1.0f));
        gameoverGui = new GuiTexture(loader.loadTexture("gameover"),new Vector2f(0.0f,0.0f),new Vector2f(1.0f,1.0f));
        winGui = new GuiTexture(loader.loadTexture("win"),new Vector2f(0.0f,0.0f),new Vector2f(1.0f,1.0f));
        scoreBoard = new GuiTexture(loader.loadTexture("block"),new Vector2f(0f,1f),new Vector2f(1f,0.15f));
        guiRenderer = new GuiRenderer(loader);

        /** FONT TEXTURE DECLARATION **/
        font = new FontType(loader.loadTexture("arial"), new File("LWJGL/res/arial.fnt"));
        /*text = new GUIText(guiText, 2, font, new Vector2f(0.25f, 0f), 0.5f, true);
        text.setColour(255, 255, 255);*/
    }

    /** GENERATING NEW TEXT **/
    public String setCoinCounterText(int numberOfCoins) {
        return "Colected coins: " + numberOfCoins + " / " + coinNumber;
    }

    public String setLevelText() {
        return "Level: " + (currentLevel + 1) + " / 3";
    }

    public String setTimerText(long elapsedTime) {
        if((60 - elapsedTime / 1000) < 10)
            return "Time: 00:0" + (60 - elapsedTime / 1000) + " s";
        return "Time: 00:" + (60 - elapsedTime / 1000) + " s";
    }

    public String setErrorText() { return "Previous coin first!"; }

    /** GENERATING LEVEL THEME **/
    private String[] genLevelTheme() {
        if(currentLevel == 0)
            return new String[] {"sand", "map1", "map1"};
        else if (currentLevel == 1)
            return new String[] {"snow", "map3", "map3"};
        return new String[] {"grass", "map2", "map2"};
    }

    /** GENERATING COINS AND PLAYER'S POSITION **/
    private int playerCoordX() {
        if(currentLevel == 0)
            return 722;
        else if(currentLevel == 1)
            return 1368;
        return 376;
    }

    private int playerCoordZ() {
        if(currentLevel == 0)
            return -407;
        else if(currentLevel == 1)
            return -1099;
        return -1272;
    }


    private int[] tableCoordX() {
        if(currentLevel == 0)
            return new int[] {526,168,530,761,915,409,659,981,1436,1286,1005};
        else if(currentLevel == 1)
            return new int[] {1354,1311,1233,1018,1088,941,514,216,155,90,494,361,1052,1421};
        return new int[] {253, 91, 111, 257, 384, 660, 888, 760, 748, 819, 1107, 1335, 1252, 1393, 1264, 1096, 927, 632};
    }

    private int[] tableCoordZ() {
        if(currentLevel == 0)
            return new int[] {-401,-461,-625,-663,-915,-997,-1239,-1282,-923,-578,-383};
        else if(currentLevel == 1)
            return new int[] {-890,-659,-403,-256,-818,-1031,-193,-188,-324,-678,-987,-1380,-1294,-1331};
        return new int[] {-1244, -1175, -792, -373, -860, -986, -754, -431, -163, -95, -154, -214, -895, -1174, -1249, -1302, -1219, -1272};
    }

    private int playerRotY() {
        if(currentLevel == 0)
            return 270;
        else if(currentLevel == 1)
            return 0;
        return 270;
    }

    private int playerScale() {
        if(currentLevel == 0)
            return 3;
        else if(currentLevel == 1)
            return 6;
        return 5;
    }

    /** GENERATING TEXTURES **/
    public Terrain genGameTerrain() {
        return new Terrain(0,-1, loader, texturePack, blendMap);
    }

    public int getCoinNumber() {
        return coinNumber;
    }

    public Player setPlayer() {
        return new Player(loadModels.car,new Vector3f(playerStartCoordX,0,playerStartCoordZ),0,playerStartRotY,0,playerStartScale);
    }

    public List<Entity> genCoins(List<Entity> coins) {
        for(int i = 0; i < coinNumber; i++)
            coins.add(new Entity(loadModels.coin, new Vector3f(coinCordX[i], 0, coinCordZ[i]), 0, 10, 0, 3,true));
        return coins;
    }

    public Vector3f getCoinPosition(int indexOfCoin) {
        return new Vector3f(coinCordX[indexOfCoin], 0, coinCordZ[indexOfCoin]);
    }

    public boolean checkCurrentCoin(Vector3f currentCoinPosition) {
        return (coinCordX[currentCoinIndex] == currentCoinPosition.x && coinCordZ[currentCoinIndex] == currentCoinPosition.z);
    }

    public List<Entity> genObstacles(List<Entity> entities) {
        for(int i = 0; i < 1490; i += 20) {
            entities.add(new Entity(loadModels.wallTree, new Vector3f(1489-i, 0, -14), 0, 0, 0, 15,true));
            entities.add(new Entity(loadModels.wallTree, new Vector3f(14, 0, -14-i), 0, 0, 0, 15,true));
            entities.add(new Entity(loadModels.wallTree, new Vector3f(14+i, 0, -1489), 0, 0, 0, 15,true));
            entities.add(new Entity(loadModels.wallTree, new Vector3f(1489, 0, -14-i), 0, 0, 0, 15,true));
        }

        for(int i = 0;i < 200; i++) {
            if(loadModels.grass != null)
                entities.add(new Entity(loadModels.grass, new Vector3f(rnd.nextFloat()*1470+14, 0, rnd.nextFloat()*-1498-14),0,0,0,2.0f,false));
            if(i % 3 == 0) {
                entities.add(new Entity(loadModels.flower, new Vector3f(rnd.nextFloat()*1470+14, 0, rnd.nextFloat()*-1498-14),0,0,0,3.0f,false));
                entities.add(new Entity(loadModels.lowPolyTree, new Vector3f(rnd.nextFloat()*1470+14, 0, rnd.nextFloat()*-1498-14), 0, 0, 0, 2.5f,true));
            }
            if(i % 7 == 0) {
                entities.add(new Entity(loadModels.stone, new Vector3f(rnd.nextFloat()*1470+14, 0, rnd.nextFloat()*-1498-14), 0, 0, 0, 6.0f,true));
                entities.add(new Entity(loadModels.palm,rnd.nextInt(4), new Vector3f(rnd.nextFloat()*1470+14, 0, rnd.nextFloat()*-1498-14), 0, 0, 0, 2.0f));
                entities.add(new Entity(loadModels.tree, new Vector3f(rnd.nextFloat()*1488+14, 0, rnd.nextFloat()*-1488-14), 0, 0, 0, 4.0f,true));
            }
        }
        return entities;
    }

    public MainRenderer genMapRenderer() {
        return new MainRenderer(loader, rendererMapName);
    }

}
