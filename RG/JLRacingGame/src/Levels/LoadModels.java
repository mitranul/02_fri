package Levels;

import Models.TexturedModel;
import Objects.OBJLoader;
import Objects.ObjectLoader;
import Textures.ModelTexture;

public class LoadModels {
    private ObjectLoader loader;
    private int currentLevel;

    /** ENVIRONMENT TEXTURES **/
    public ModelTexture palmTextureAtlas;
    public TexturedModel palm;
    public TexturedModel grass;
    public TexturedModel flower;
    public ModelTexture texture;

    /** PLAYER */
    public TexturedModel car;

    /** OBSTACLES **/
    public TexturedModel tree;
    public TexturedModel wallTree;
    public TexturedModel lowPolyTree;
    public TexturedModel stone;
    public TexturedModel coin;

    /** DECLARATION OF THE .obj FILENAMES **/
    private String[] natureModels;
    private String carModel;

    /** DECLARATION OF THE .png FILENAMES **/
    private String[] natureTextures;
    private String carTexture;

    LoadModels(ObjectLoader loader, int currLevel) {
        this.loader = loader;
        this.currentLevel = currLevel;
        generateAllTextureTables();
        generateModelsWithTextures();
    }

    /*TODO
     * --- Change the filenames in for second and third level
     * --- if you named .obj and .png files with same name, then we can use only 1 table for each texture type
     * --- be careful with indexes bellow
     * */
    private void generateAllTextureTables() {
        switch(currentLevel) {
            case 0: {
                /** TEXTURE FILENAMES FOR ENVIRONMENT TEXTURES**/
                natureModels = new String[] {  "palmModel",   null,   "grassModel",    "treeModel",   "wallTreeModel",   "lowPolyTreeModel",   "stoneModel" , "coinModel"};
                natureTextures = new String[] {"palmTexture" ,null, "sandTexture", "treeTexture", "wallTreeTexture", "lowPolyTreeTexture", "mud", "goldTexture"};
                carModel = "car1";
                carTexture = "uvCarText1";

            } break;
            case 1: {
                /** TEXTURE FILENAMES FOR ENVIRONMENT TEXTURES**/
                natureModels = new String[] {  "palmModel",   null,   "grassModel",    "treeModel",   "wallTreeModel",   "lowPolyTreeModel",   "stoneModel" , "coinModel"};
                natureTextures = new String[] {"snow" ,null, "flowerTexture", "treeTexture", "wallTreeTexture", "lowPolyTreeTexture", "stoneTexture", "goldTexture"};
                carModel = "car2";
                carTexture = "uvCarText2";
            } break;
            default: {
                /** TEXTURE FILENAMES FOR ENVIRONMENT TEXTURES**/
                natureModels = new String[] {  "palmModel",   "grassModel",   "grassModel",    "treeModel",   "wallTreeModel",   "lowPolyTreeModel",   "stoneModel" , "coinModel"};
                natureTextures = new String[] {"palmTexture" ,"grassTexture", "flowerTexture", "treeTexture", "wallTreeTexture", "lowPolyTreeTexture", "stoneTexture", "goldTexture"};
                carModel = "car3";
                carTexture = "uvCarText3";
            }
        }
    }

    public void generateModelsWithTextures() {
        palmTextureAtlas = new ModelTexture(loader.loadTexture(natureTextures[0]));
        palmTextureAtlas.setNumberOfRows(1);
        palm = new TexturedModel(OBJLoader.loadObjModel(natureModels[0],loader),palmTextureAtlas);
        palm.getTexture().setHasTransparency(true);

        if(natureModels[1] != null || natureTextures[1] != null) {
            grass = new TexturedModel(OBJLoader.loadObjModel(natureModels[1], loader), new ModelTexture(loader.loadTexture(natureTextures[1])));
            grass.getTexture().setHasTransparency(true);
            grass.getTexture().setUseFakeLighting(true);
        }
        else {
            grass = null;
        }

        flower = new TexturedModel(OBJLoader.loadObjModel(natureModels[2],loader),new ModelTexture(loader.loadTexture(natureTextures[2])));
        flower.getTexture().setHasTransparency(true);
        flower.getTexture().setUseFakeLighting(true);


        tree = new TexturedModel(OBJLoader.loadObjModel(natureModels[3], loader), new ModelTexture(loader.loadTexture(natureTextures[3])));
        wallTree = new TexturedModel(OBJLoader.loadObjModel(natureModels[4], loader), new ModelTexture(loader.loadTexture(natureTextures[4])));
        lowPolyTree = new TexturedModel(OBJLoader.loadObjModel(natureModels[5], loader), new ModelTexture(loader.loadTexture(natureTextures[5])));
        stone = new TexturedModel(OBJLoader.loadObjModel(natureModels[6], loader), new ModelTexture(loader.loadTexture(natureTextures[6])));
        coin = new TexturedModel(OBJLoader.loadObjModel(natureModels[7], loader), new ModelTexture(loader.loadTexture(natureTextures[7])));

        car = new TexturedModel(OBJLoader.loadObjModel(carModel,loader),new ModelTexture(loader.loadTexture(carTexture)));

        /*texture = tree.getTexture();
        texture.setShineRange(30);
        texture.setReflectivity(1);*/
    }

}
