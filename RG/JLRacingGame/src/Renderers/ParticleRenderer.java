package Renderers;

import java.awt.*;
import java.beans.VetoableChangeListener;
import java.util.List;
import java.util.Map;

import Animation.Particle;
import Animation.TexturedParticle;
import Models.PlainModel;
import Objects.ObjectLoader;
import Other.Maths;
import Shaders.ParticleShader;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import Objects.Camera;



public class ParticleRenderer {
	
	private static final float[] VERTICES = {-0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, -0.5f};
	
	private PlainModel quad;
	private ParticleShader shader;
	
	public ParticleRenderer(ObjectLoader loader, Matrix4f projectionMatrix){
		quad = loader.loadToVAO(VERTICES, 2 );
		shader = new ParticleShader();
		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}
	
	public void render(Map<TexturedParticle,List<Particle>> particles, Camera camera){
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		prepare();
		for(TexturedParticle texture : particles.keySet()) {
			GL13.glActiveTexture(GL13.GL_TEXTURE0);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D,texture.getTextureID());
			for (Particle particle : particles.get(texture)) {
				updateModelViewMatrix(particle.getPosition(), particle.getRotation(), particle.getScale(), viewMatrix);
				GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, quad.getVertexCount());
			}
		}
		finishRendering();
	}

	private void updateModelViewMatrix(Vector3f position, float rotation, float scale, Matrix4f viewMatrix) {
		Matrix4f modelMat = new Matrix4f();
		Matrix4f.translate(position, modelMat, modelMat);
		// Transponiramo matriko
		modelMat.m00 = viewMatrix.m00;
		modelMat.m01 = viewMatrix.m10;
		modelMat.m02 = viewMatrix.m20;
		modelMat.m10 = viewMatrix.m01;
		modelMat.m11 = viewMatrix.m11;
		modelMat.m12 = viewMatrix.m21;
		modelMat.m20 = viewMatrix.m02;
		modelMat.m21 = viewMatrix.m12;
		modelMat.m22 = viewMatrix.m22;

		Matrix4f.rotate((float) Math.toRadians(rotation), new Vector3f(0, 0, 1), modelMat, modelMat);
		Matrix4f.scale(new Vector3f(scale, scale, scale), modelMat, modelMat);
		Matrix4f modelViewMat = Matrix4f.mul(viewMatrix, modelMat, null);
		shader.loadModelViewMatrix(modelViewMat);
	}


	public void cleanUp(){
		shader.cleanUp();
	}
	
	private void prepare(){
		shader.start();
		GL30.glBindVertexArray(quad.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		//GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
		GL11.glDepthMask(false);
	}
	
	private void finishRendering(){
		GL11.glDepthMask(true);
		GL11.glDisable(GL11.GL_BLEND);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		shader.stop();
	}

}
