package GameLoop;

import Animation.MainParticle;
import Animation.ParticleSystem;
import Animation.TexturedParticle;
import Audio.AudioSource;
import Audio.MainAudio;
import Fonts.GUIText;
import Levels.Level;
import Objects.*;
import Renderers.MainRenderer;

import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import java.util.*;

public class JLRacingGame {
    public static void main(String[] args) throws InterruptedException {
        boolean start = false;
        boolean isGameOver = false;
        boolean isWin = false;
        boolean isNewLevel = false;
        boolean errorTextShowed = false;
        long errorTextStartTime = 0;
        int currentLevel = 0;
        WindowManager.createDisplay();
        List<Entity> entities;
        List<Entity> coins;
        Light light = new Light(new Vector3f(20000,40000,2000),new Vector3f(1,1,1));
        Terrain terrain;
        Player player;
        Camera camera;
        MainRenderer renderer;
        ObjectLoader loader = new ObjectLoader();

        MainAudio.init();
        MainAudio.setListenerData();

        int lv1Music = MainAudio.loadSound("Audio/level1.wav");
        int lv2Music = MainAudio.loadSound("Audio/level2.wav");
        int lv3Music = MainAudio.loadSound("Audio/level3.wav");
        int mMusic = MainAudio.loadSound("Audio/menu.wav");
        int goMusic = MainAudio.loadSound("Audio/gameover.wav");
        int wMusic = MainAudio.loadSound("Audio/win.wav");

        AudioSource level1Music = new AudioSource();
        AudioSource level2Music = new AudioSource();
        AudioSource level3Music = new AudioSource();
        AudioSource menuMusic = new AudioSource();
        AudioSource gameoverMusic = new AudioSource();
        AudioSource winMusic = new AudioSource();

        Level level = new Level(11, currentLevel);

        for(int i = 0; i < currentLevel+1; i++) {
            MainFont.init(level.loader);

            int countCoins = 0;
            int numberOfCoins = level.getCoinNumber();

            GUIText text = new GUIText(level.setCoinCounterText(countCoins), 2, level.font, new Vector2f(-0.1f, 0f), 0.5f, true);
            text.setColour(255, 255, 255);

            GUIText textTimer = new GUIText(level.setTimerText(0), 2, level.font, new Vector2f(0.6f, 0f), 0.5f, true);
            textTimer.setColour(255, 255, 255);

            GUIText textLevel = new GUIText(level.setLevelText(), 2, level.font, new Vector2f(0.25f, 0f), 0.5f, true);
            textLevel.setColour(255, 0, 0);

            GUIText textError= new GUIText("", 2, level.font, new Vector2f(0.7f, 0.5f), 0.25f, true);
            textError.setColour(255, 255, 255);

            /** ENTITIES LIST AND FILLING WITH OBSTACLES **/
            entities = new ArrayList<Entity>();
            entities = level.genObstacles(entities);

            /** COINS TABLE **/
            coins = new ArrayList<Entity>();
            coins = level.genCoins(coins);

            terrain = level.genGameTerrain();

            /** GENERATING PLAYER AND SETTING CAMERA **/
            player = level.setPlayer();
            camera = new Camera(player);

            /** GENERATING MAP RENDERER **/
            renderer = level.genMapRenderer();
            MainParticle.init(level.loader, renderer.getProjectionMatrix());
            TexturedParticle particleTexture = new TexturedParticle(level.loader.loadTexture("particleStar"), 1);
            ParticleSystem pSystem = new ParticleSystem(particleTexture,50, 25, 0.3f, 4, 2.8f);

            /** MENU MUSIC **/
            menuMusic.play(mMusic);

            boolean timerStart = false;
            long startTime = System.currentTimeMillis();
            long elapsedTime = 0;
            long errorTextElapsedTime = 0;


            while (!Display.isCloseRequested()) {
                // START MENU -> BUTTON E
                if(Keyboard.isKeyDown(Keyboard.KEY_E) && !timerStart){
                    start = true;
                    menuMusic.delete();
                    level1Music.play(lv1Music);
                    timerStart = true;
                }
                // ESCAPE BUTTON
                else if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                    //guis.add(gameoverGui);
                    //guiRenderer.render(guis);
                    //guiRenderer.cleanUp();
                    start=false;
                    isGameOver = true;
                    level1Music.delete();
                    level2Music.delete();
                    level3Music.delete();
                }

                // IF BUTTON E IS PRESSED THEN START WITH GAME
                if(start && !isGameOver && !isWin) {
                    // CAMERA AND PLAYER MOVING
                    camera.move();
                    player.move();

                    pSystem.generateParticles(level.getCoinPosition(level.currentCoinIndex));
                    MainParticle.update();
                    //System.out.println(player.getPosition());

                    // LIMITED TIME FOR GAME
                    elapsedTime = (new Date()).getTime() - startTime;
                    MainFont.removeText(textTimer);
                    textTimer = new GUIText(level.setTimerText(elapsedTime), 2, level.font, new Vector2f(0.6f, 0f), 0.5f, true);
                    textTimer.setColour(255, 255, 255);

                    if(elapsedTime > 1000*60) {
                        System.out.println(elapsedTime/1000/60+" min");
                        System.out.println("KONEC");
                        isGameOver = true;
                        gameoverMusic.play(goMusic);
                        //break;
                    }
                    //System.out.println(camera.getPosition());
                    // PROCESS ALL OBJECTS IN TERRAIN
                    renderer.processEntity(player);

                    renderer.processTerrain(terrain);

                    for (Entity entity : entities) {
                        renderer.processEntity(entity);
                    }

                    for (Entity coinEntity : coins) {
                        renderer.processEntity(coinEntity);
                    }

                    // MAIN RENDERER
                    renderer.render(light, camera);

                    for(Entity entity : entities) {
                        if(entity.isCollide()) {
                            player.isCollision(entity);
                        }
                    }

                    for(Entity coinEntity : coins) {
                        if(coinEntity.isCollide()) {
                            if(player.isCollision(coinEntity)) {
                                if(level.checkCurrentCoin(coinEntity.getPosition())) {
                                    System.out.println("Kovanec");
                                    coins.remove(coinEntity);
                                    countCoins++;
                                    level.currentCoinIndex++;

                                    /** NUMBER OF COINS TEXT GENERATED**/
                                    MainFont.removeText(text);
                                    text = new GUIText(level.setCoinCounterText(countCoins), 2, level.font, new Vector2f(-0.1f, 0f), 0.5f, true);
                                    text.setColour(255, 255, 255);

                                    if(countCoins == numberOfCoins) {
                                        isWin = true;
                                        if(currentLevel == 2) {
                                            currentLevel++;
                                            MainFont.removeText(text);
                                            MainFont.removeText(textTimer);
                                            MainFont.removeText(textLevel);
                                            level3Music.delete();
                                            winMusic.play(wMusic);
                                            isWin = true;
                                            break;
                                        }
                                        else {
                                            isNewLevel = true;
                                            currentLevel++;
                                            MainFont.removeText(text);
                                            MainFont.removeText(textTimer);
                                            MainFont.removeText(textLevel);
                                            break;
                                        }
                                    }
                                    System.out.println(countCoins);
                                    break;
                                }
                                else if(!errorTextShowed){
                                    textError= new GUIText(level.setErrorText(), 2, level.font, new Vector2f(0.73f, 0.5f), 0.25f, true);
                                    textError.setColour(255, 0, 0);
                                    errorTextShowed = true;
                                    errorTextStartTime = System.currentTimeMillis();
                                }
                            }
                        }
                    }
                    if(errorTextShowed) {
                        elapsedTime = (new Date()).getTime() - errorTextStartTime;
                        if(elapsedTime > 1000 * 2) {
                            MainFont.removeText(textError);
                            errorTextShowed = false;
                        }
                    }
                    level.guiRenderer.render(level.scoreBoard);
                    MainFont.render();
                }
                else if(isNewLevel) {
                    System.out.println("HERE NEW LEVEL " + currentLevel);
                    if(currentLevel == 1) {
                        isWin = false;
                        renderer.cleanUp();
                        loader.cleanUp();
                        MainFont.cleanUp();
                        level = new Level(14, currentLevel);
                        isNewLevel = false;
                        level1Music.delete();
                        level2Music.play(lv2Music);
                        break;
                    }
                    else if(currentLevel == 2){
                        isWin = false;
                        renderer.cleanUp();
                        loader.cleanUp();
                        level = new Level(17, currentLevel);
                        isNewLevel = false;
                        level2Music.delete();
                        level3Music.play(lv3Music);
                        break;
                    }
                }
                else if(isGameOver) {
                    level.guiRenderer.render(level.gameoverGui);
                    level1Music.delete();
                    level2Music.delete();
                    level3Music.delete();
                    //guiRenderer.render(gameoverGui);
                    /*TimeUnit.SECONDS.sleep(5);
                    break;*/

                }
                else if(isWin) {
                    //guiRenderer.render(winGui);
                    level.guiRenderer.render(level.winGui);
                }
                else {
                    // DRAW MENU GUI
                    //guiRenderer.render(menuGui);
                    level.guiRenderer.render(level.menuGui);
                }

                MainParticle.particlesRender(camera);
                // UPDATE DISPLAY
                WindowManager.updateDisplay();
            }

            // CLEAN MEMORY
            MainParticle.cleanUp();
            //guiRenderer.cleanUp();
            //renderer.cleanUp();
            //shader.cleanUp();
            //level.loader.cleanUp();

            /*if(Display.isCloseRequested() || currentLevel == 3) {
                WindowManager.closeDisplay();
                break;
            }*/
        }
    }
}
