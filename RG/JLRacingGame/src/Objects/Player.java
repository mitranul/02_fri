package Objects;

import Models.TexturedModel;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

public class Player extends Entity{
    private static final float RUN_SPEED = 3;
    private static final float TURN_SPEED = 80;
    public static final float GRAVITY = -50;
    private static final float JUMP_POWER = 30;

    private static final float TERRAIN_HEIGHT = 0;

    private float currSpeed = 0;
    private float currTurnSpeed = 0;
    private float upwardsSpeed = 0;

    private boolean isInAir = false;


    public Player(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        super(model, position, rotX, rotY, rotZ, scale,true);
    }

    public void move() {
        checkInputEvents();
        super.increaseRotation(0,currTurnSpeed * WindowManager.getFrameTimeSeconds(),0);
        float distance = currSpeed * WindowManager.getFrameTimeSeconds();
        float dx = (float)(distance * Math.sin(Math.toRadians(super.getRotY())));
        float dz = (float)(distance * Math.cos(Math.toRadians(super.getRotY())));
        super.increasePosition(dx,0,dz);
        upwardsSpeed += GRAVITY * WindowManager.getFrameTimeSeconds();
        super.increasePosition(0,upwardsSpeed * WindowManager.getFrameTimeSeconds(),0);
        // For stop falling
        if(super.getPosition().y < TERRAIN_HEIGHT) {
            upwardsSpeed = 0;
            isInAir = false;
            super.getPosition().y = TERRAIN_HEIGHT;
        }
    }

    private void jump() {
        if(!isInAir) {
            this.upwardsSpeed = JUMP_POWER;
            isInAir = true;
        }
    }

    private void checkInputEvents() {
        if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
            if(currSpeed<=180) {
                this.currSpeed += RUN_SPEED;
            }
        }
        else if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
            if(currSpeed>=-100) {
                this.currSpeed -= RUN_SPEED / 3;
            }
        }
        else {
            //this.currSpeed = 0;
            if(currSpeed>0) {
                this.currSpeed-=1;
            }

            if(currSpeed<0) {
                this.currSpeed+=1;
            }
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
            this.currTurnSpeed = -TURN_SPEED;
        }
        else if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
            this.currTurnSpeed = TURN_SPEED;
        }
        else {
            this.currTurnSpeed = 0;
        }

        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            //jump();
            if(currSpeed-3>=0) {
                this.currSpeed -= 3;
            }
        }
    }

    public boolean isCollision(Entity entity) {

        if (( this.getPosition().x <= entity.getScale() + entity.getPosition().x && this.getScale() + this.getPosition().x >= entity.getPosition().x ) &&
                ( this.getPosition().y <= entity.getScale() + entity.getPosition().y && this.getScale() + this.getPosition().y >= entity.getPosition().y ) &&
                ( this.getPosition().z <= entity.getScale() + entity.getPosition().z && this.getScale() + this.getPosition().z >= entity.getPosition().z )
        ) {
            //System.out.println("Player is colliding!!!");
            float distance = currSpeed * WindowManager.getFrameTimeSeconds();
            this.increasePosition(-(float)(distance * Math.sin(Math.toRadians(super.getRotY()))),0,-(float)(distance * Math.cos(Math.toRadians(super.getRotY()))));
            if(this.currSpeed > 40) {
                this.currSpeed -= 10;
            }
            return true;
        }
        return false;
    }
}

