package Objects;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;
import org.newdawn.slick.opengl.ImageIOImageData;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class WindowManager {
    private static final int WIDTH = 1280;
    private static final int HEIGHT = 720;
    private static final int FPS = 120;

    // For player
    private static long lastFrameTime;
    private static float deltaTime;

    public static void createDisplay() {
        ContextAttribs atributi = new ContextAttribs(3,2);
        atributi.withForwardCompatible(true);
        atributi.withProfileCore(true);

        try {
            Display.setIcon(new ByteBuffer[] {
                    new ImageIOImageData().imageToByteBuffer(ImageIO.read(new File("assets/icon16.png")), false, false, null),
                    new ImageIOImageData().imageToByteBuffer(ImageIO.read(new File("assets/icon32.png")), false, false, null)
            });
            Display.setDisplayMode(new DisplayMode(WIDTH,HEIGHT));
            Display.create(new PixelFormat(),atributi);
            Display.setTitle("JL Racing Game");
        }
        catch (LWJGLException e) {
            e.printStackTrace();
        }
        catch (IOException err) {
            err.printStackTrace();
        }

        // Whole display
        GL11.glViewport(0,0,WIDTH,HEIGHT);
        lastFrameTime = getCurrentTime();

    }

    public static  void updateDisplay() {
        Display.sync(FPS);
        Display.update();
        long currFrameTime = getCurrentTime();
        // how time frame was rendering
        deltaTime = (currFrameTime - lastFrameTime)/1000f;
        lastFrameTime = currFrameTime;
    }

    public static float getFrameTimeSeconds() {
        return deltaTime;
    }

    public static void closeDisplay() {
        Display.destroy();
    }

    private static long getCurrentTime() {
        return Sys.getTime()*1000 / Sys.getTimerResolution();
    }
}
