package Animation;

public class TexturedParticle {
    private int textureID;
    private int numberOfRows;

    public TexturedParticle(int textureID, int numberOfRows) {
        this.textureID = textureID;
        this.numberOfRows = numberOfRows;
    }

    public int getTextureID() {
        return textureID;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }
}
