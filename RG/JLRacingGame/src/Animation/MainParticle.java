package Animation;

import Objects.ObjectLoader;
import Objects.Camera;
import Renderers.ParticleRenderer;
import org.lwjgl.util.vector.Matrix4f;
import java.util.*;
import java.util.List;

public class MainParticle {
    private static Map<TexturedParticle,List<Particle>> particles = new HashMap<TexturedParticle,List<Particle>>();
    private static ParticleRenderer renderer;

    public static void init(ObjectLoader loader, Matrix4f projectionMat) {
        renderer = new ParticleRenderer(loader, projectionMat);
    }

    public static void update() {
        Iterator<Map.Entry<TexturedParticle, List<Particle>>> mapIterator = particles.entrySet().iterator();
        while (mapIterator.hasNext()) {
            List<Particle> list = mapIterator.next().getValue();
            Iterator<Particle> iterator = list.iterator();
            while (iterator.hasNext()) {
                Particle p = iterator.next();
                boolean stillAlive = p.update();
                if (!stillAlive) {
                    iterator.remove();
                    if (list.isEmpty()) {
                        mapIterator.remove();
                    }
                }
            }
        /*Iterator<Particle> iterator = particles.iterator();
        while(iterator.hasNext()) {
            Particle p = iterator.next();
            boolean stillAlive = p.update();
            if(!stillAlive) {
                iterator.remove();
            }
        }*/
        }
    }

    public static void particlesRender(Camera camera) {
        renderer.render(particles, camera);
    }

    public static void cleanUp() {
        renderer.cleanUp();
    }

    public static void addParticle(Particle particle) {
        List<Particle> list = particles.get(particle.getTexture());
        if(list == null) {
            list = new ArrayList<Particle>();
            particles.put(particle.getTexture(),list);
        }
        list.add(particle);
        //particles.add(particle);
    }

}
