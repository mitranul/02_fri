package Animation;


import Objects.Player;
import Objects.WindowManager;
import org.lwjgl.util.vector.Vector3f;

public class Particle {
    private Vector3f position;
    private Vector3f velocity;  // hitrost in smer vektorja
    private float gravityEff;
    private float lifeLength;
    private float rotation;
    private float scale;

    private TexturedParticle texture;
    private float elapsedTime = 0;

    public Particle(TexturedParticle texture,Vector3f position, Vector3f velocity, float gravityEff, float lifeLength, float rotation, float scale) {
        this.texture = texture;
        this.position = position;
        this.velocity = velocity;
        this.gravityEff = gravityEff;
        this.lifeLength = lifeLength;
        this.rotation = rotation;
        this.scale = scale;
        MainParticle.addParticle(this);
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getRotation() {
        return rotation;
    }

    public float getScale() {
        return scale;
    }

    public TexturedParticle getTexture() {
        return texture;
    }

    protected boolean update() {
        velocity.y += Player.GRAVITY * gravityEff * WindowManager.getFrameTimeSeconds();
        Vector3f change = new Vector3f(velocity);
        change.scale(WindowManager.getFrameTimeSeconds());
        Vector3f.add(change, position, position);
        elapsedTime += WindowManager.getFrameTimeSeconds();
        return elapsedTime < lifeLength;
    }
}
