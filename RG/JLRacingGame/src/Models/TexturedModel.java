package Models;

import Textures.ModelTexture;

public class TexturedModel {
    private PlainModel rawModel;
    private ModelTexture texture;

    public TexturedModel(PlainModel model,ModelTexture texture) {
        this.rawModel = model;
        this.texture = texture;
    }

    public PlainModel getRawModel() {
        return rawModel;
    }

    public ModelTexture getTexture() {
        return texture;
    }
}
