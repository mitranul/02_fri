package Textures;

public class ModelTexture {
    private int textureID;
    private float shineRange = 1;
    private float reflectivity = 0;

    private boolean hasTransparency = false;
    private boolean useFakeLighting = false;

    // for multiple textures on one image
    private int numberOfRows = 1;


    public ModelTexture(int id) {
        this.textureID = id;
    }
    public int getID() {
        return this.textureID;
    }

    public float getShineRange() {
        return shineRange;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public void setShineRange(float shineRange) {
        this.shineRange = shineRange;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public boolean isHasTransparency() {
        return hasTransparency;
    }

    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }

    public boolean isUseFakeLighting() {
        return useFakeLighting;
    }

    public void setUseFakeLighting(boolean useFakeLighting) {
        this.useFakeLighting = useFakeLighting;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
}

