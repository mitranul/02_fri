class Vector4f {
  constructor(x, y, z, h){
    try {
      if(x == null || y == null || z == null || h == null)
        throw "Vector4f class constructor requires 4 input parameters (x, y, z, h)";
      if(!isFinite(x) || !isFinite(y) || !isFinite(z) || !isFinite(h))
        throw "Wrong data type for coordinates of class Vector4f! (NaN)";

      this.vr_x = x;
      this.vr_y = y;
      this.vr_z = z;
      this.homogen = h;
    }catch (err) {
      console.log(err);
    }
  }

  static negate(v) {
    return new Vector4f(v.vr_x * -1, v.vr_y * -1, v.vr_z * -1, 0);
  }

  static add(v1, v2) {
    try {
      if(!v1 instanceof Vector4f || !v2 instanceof Vector4f)
        throw "Vector4f.add() --> vector1 or vector2 is not instance of class Vector4f";

      return new Vector4f(v1.vr_x + v2.vr_x, v1.vr_y + v2.vr_y, v1.vr_z + v2.vr_z, 0);
    }catch(err) {
      console.log(err);
    }
  }

  static scalarProduct(scalar, v) {
    return new Vector4f(v.vr_x * scalar, v.vr_y * scalar, v.vr_z * scalar, 0);
  }

  static dotProduct(v1, v2) {
    try {
      if(!v1 instanceof Vector4f || !v2 instanceof Vector4f)
        throw "Vector4f.dotProduct() --> vector1 or vector2 is not instance of class Vector4f";

      return (v1.vr_x * v2.vr_x) + (v1.vr_y * v2.vr_y) + (v1.vr_z * v2.vr_z);
    }catch(err) {
      console.log(err);
    }
  }

  static crossProduct(v1, v2) {
    try {
      if(!v1 instanceof Vector4f || !v2 instanceof Vector4f)
        throw "Vector4f.dotProduct() --> vector1 or vector2 is not instance of class Vector4f";

      return new Vector4f((v1.vr_y * v2.vr_z) - (v1.vr_z * v2.vr_y), (v1.vr_z * v2.vr_x) - (v1.vr_x * v2.vr_z), (v1.vr_x * v2.vr_y) - (v1.vr_y * v2.vr_x), 0);
    }catch (err) {
      console.log(err);
    }
  }

  static length(v1) {
    return Math.sqrt(Math.pow(v1.vr_x, 2) + Math.pow(v1.vr_y, 2) + Math.pow(v1.vr_z, 2));
  }

  static normalize(v1) {
    try {
      if (this.length(v1) === 0)
        throw "Vector4f.normalize() --> Invalid operation --> dividing by zero!";

      return new Vector4f(v1.vr_x / this.length(v1), v1.vr_y / this.length(v1), v1.vr_z / this.length(v1), 0);
    }catch (err) {
      console.log(err);
    }
  }

  static project(v1, v2) {
    try {
      if(this.length(v2) === 0)
        throw "Vector4f.project() --> Invalid operation --> dividing by zero!";
      if(!v1 instanceof Vector4f || !v2 instanceof Vector4f)
        throw "Vector4f.project() --> vector1 or vector2 is not instance of class Vector4f";

      return this.scalarProduct(this.dotProduct(v1, v2) / Math.pow(this.length(v2), 2), v2);
    }catch (err) {
      console.log(err);
    }
  }

  static cosPhi(v1, v2) {
    try {
      if((this.length(v1) * this.length(v2)) === 0)
        throw "Vector4f.cosPhi() --> Invalid operation --> dividing by zero!";

      return this.dotProduct(v1, v2) / (this.length(v1) * this.length(v2));
    }catch (err) {
      console.log(err);
    }
  }
}

class Matrix4f {
  constructor(table1, table2, table3, table4) {
    try {
      if(table1.length !== 4 || table2.length !== 4 || table3.length !== 4 || table4.length !== 4)
        throw "Matrix4f class constructor requires 4 tables, length of 4!";
      for(let i = 0; i < 4; i++)
        if(!isFinite(table1[i]) || !isFinite(table2[i]) || !isFinite(table3[i]) || !isFinite(table4[i]))
          throw "Matrix4f class constructor requires 4 tables of numeric data types";
      this.matrixTable = [
        table1,
        table2,
        table3,
        table4
      ];

    }catch (err) {
      console.log(err);
    }
  }

  static negate(m) {
    try {
      if(!m instanceof Matrix4f)
        throw "Matrix4f.negate() --> m is not instance of class Matrix4f";

      let matrix = [];
      for(var i = 0; i < 4; i++) {
        let matrix_line = m.matrixTable[i];
        for(var j = 0; j < 4; j++) {
          matrix_line[j] *= -1;
        }
        matrix[i] = matrix_line;
      }
      return new Matrix4f(matrix[0], matrix[1], matrix[2], matrix[3]);
    }catch (err) {
      console.log(err);
    }
  }

  static add(m1, m2) {
    try {
      if(!m1 instanceof Matrix4f || !m2 instanceof Matrix4f)
        throw "Matrix4f.add() --> m1 or m2 is not instance of class Matrix4f";
      let matrix = [];
      for(var i = 0; i < 4; i++) {
        let sum_line = [];
        for(var j = 0; j < 4; j++) {
          sum_line[j] = m1.matrixTable[i][j] + m2.matrixTable[i][j];
        }
        matrix[i] = sum_line;
      }
      return new Matrix4f(matrix[0], matrix[1], matrix[2], matrix[3]);

    }catch (err) {
      console.log(err);
    }
  }

  static transpose(m) {
    try {
      if(!m instanceof Matrix4f)
        throw "Matrix4f.transpose() --> matrix is not instance of class Matrix4f";
      let matrika = m;
      for(let i = 0; i < 4; i++) {
        for(let j = 0; j < 4; j++) {
          if(j > i) {
            let tmp = matrika.matrixTable[i][j];
            matrika.matrixTable[i][j] = matrika.matrixTable[j][i];
            matrika.matrixTable[j][i] = tmp;
          }
        }
      }
      return matrika;
    }catch(err) {
      console.log(err);
    }
  }

  static multiplyScalar(scalar, m) {
    try {
      if(!m instanceof Matrix4f)
        throw "Matrix4f.multiplyScalar() --> matrix is not instance of class Matrix4f";

      let matrix = [];
      for(let i = 0; i < 4; i++) {
        let matrix_line = m.matrixTable[i];
        for(let j = 0; j < 4; j++) {
          matrix_line[j] *= scalar;
        }
        matrix[i] = matrix_line;
      }
      return new Matrix4f(matrix[0], matrix[1], matrix[2], matrix[3]);
    }catch(err) {
      console.log(err);
    }
  }

  static multiply(m1, m2) {
    let matrix = [];
    for(let i = 0; i < 4; i++) {
      let matrix_line = [];
      for(let j = 0; j < 4; j++) {
        let sum_line = 0;
        for(let k = 0; k < 4; k++) {
          sum_line += (m1.matrixTable[i][k] * m2.matrixTable[k][j]);
        }
        matrix_line[j] = sum_line;
      }
      matrix[i] = matrix_line;
    }
    return new Matrix4f(matrix[0], matrix[1], matrix[2], matrix[3]);
  }
}

class Transformation {
  constructor() {
    let spremenljivka = new Matrix4f([1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]);
    this.setMatrix = function(matrika) { spremenljivka = Matrix4f.multiply(matrika, spremenljivka) };
    this.getMatrix = function () { return spremenljivka; };

    this.translate(new Vector4f(1.25, 0, 0, 0));
    this.rotateZ(Math.PI / 3);
    this.translate(new Vector4f(0, 0, 4.15, 0));
    this.translate(new Vector4f(0, 3.14, 0, 0,));
    this.scale(new Vector4f(1.12, 1.12, 1, 0));
    this.rotateY((5 * Math.PI / 8));
  }
  translate(v) { this.setMatrix(new Matrix4f([1, 0, 0, v.vr_x], [0, 1, 0, v.vr_y], [0, 0, 1, v.vr_z], [0, 0, 0, 1])); }

  scale(v) { this.setMatrix(new Matrix4f([v.vr_x, 0, 0, 0], [0, v.vr_y, 0, 0], [0, 0, v.vr_z, 0], [0, 0, 0, 1])); }

  rotateX(angle) { this.setMatrix(new Matrix4f([1, 0, 0, 0], [0, Math.cos(angle), -Math.sin(angle), 0], [0, Math.sin(angle), Math.cos(angle), 0], [0, 0, 0, 1])); }

  rotateY(angle) { this.setMatrix(new Matrix4f([Math.cos(angle), 0, Math.sin(angle), 0], [0, 1, 0, 0], [-Math.sin(angle), 0, Math.cos(angle), 0], [0, 0, 0, 1])); }

  rotateZ(angle) { this.setMatrix(new Matrix4f([Math.cos(angle), -Math.sin(angle), 0, 0], [Math.sin(angle), Math.cos(angle), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1])); }

  transformPoint(v) {
    let newVector = [];
    let getter = this.getMatrix();
    for(let i = 0; i < 4; i++)
      newVector.push((getter.matrixTable[i][0] * v.vr_x) + (getter.matrixTable[i][1] * v.vr_y) + (getter.matrixTable[i][2] * v.vr_z) + (getter.matrixTable[i][3] * v.homogen));
    return new Vector4f(newVector[0],newVector[1], newVector[2], 0);
  }
}

class PointManager {
  constructor() {}

  static readPoints() {
    try {
      let managerTable = document.getElementById("enter-point").value;
      if(managerTable === "")
        throw "PointManager.readPoints() --> Input textarea is empty!";

      let table_points = managerTable.split("v");
      let table_vecotrs = [];
      for (let i = 1; i < table_points.length; i++) {
        let table_row = table_points[i].split(" ");
        if (table_row.length !== 4)
          throw "PointManager.readPoints() --> Point has to many / less coordinates as it should have. The input should be like: v x y z (where x, y, z are the coordinates of the point)!";
        if(!isFinite(parseFloat(table_row[1])) || !isFinite(parseFloat(table_row[1])) || !isFinite(parseFloat(table_row[2])) || !isFinite(parseFloat(table_row[3])))
          throw "PointManager.readPoints() --> Wrong data type for coordinates of the point! (NaN)";
        table_vecotrs[i - 1] = new Vector4f(parseFloat(table_row[1]), parseFloat(table_row[2]), parseFloat(table_row[3]), 1);
      }
      return table_vecotrs;

    }catch (err) {
      console.log(err);
    }
  }

  static writePoints(table) {
    let valueT = document.getElementById('exit-point');
    valueT.value = '';
    for(let i = 0; i < table.length; i++) {
      valueT.value += 'v ' + table[i].vr_x + ' ' + table[i].vr_y + ' ' + table[i].vr_z + '\n';
    }
  }
}

class TransformPoints {
  constructor() {};

  start() {
    let transForm = new Transformation();
    let outputTable = [];

    for(let vector of PointManager.readPoints()) {
      outputTable.push(transForm.transformPoint(vector));
    }
    PointManager.writePoints(outputTable);
  }
}
