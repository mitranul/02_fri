"""An example of a simple HTTP server."""
import json
import mimetypes
import pickle
import socket
from os.path import isdir
from urllib.parse import unquote_plus

PICKLE_DB = "db.pkl"

# Directory containing www data
WWW_DATA = "www-data"

# Represents a table row that holds user data
TABLE_ROW = """
<tr>
    <td>%d</td>
    <td>%s</td>
    <td>%s</td>
</tr>
"""

RESPONSE_BODY = """
<!doctype html>
<h1>%s</h1>
<p>%s</p>
"""

# Header template for a successful HTTP request
HEADER_RESPONSE_200 = """HTTP/1.1 200 OK\r
content-type: %s\r
content-length: %d\r
connection: Close\r
\r
"""

# Template for a 404 (Not found) error
RESPONSE_404 = """HTTP/1.1 404 Not found\r
content-type: %s\r
content-length: %d\r
connection: Close\r
\r
"""

RESPONSE_405 = """HTTP/1.1 405 Method not allowed\r
content-type: %s\r
content-length: %d\r
connection: Close\r
\r
"""

RESPONSE_301 = """HTTP/1.1 301 Moved Permanently\r
content-type: text/html\r
content-length: %d\r
location: %s\r
connection: Close\r
\r
"""

RESPONSE_400 = """HTTP/1.1 400 Bad request\r
content-type: %s\r
content-length: %d\r
connection: Close\r
\r
"""

def save_to_db(first, last):
    """Create a new user with given first and last name and store it into
    file-based database.

    For instance, save_to_db("Mick", "Jagger"), will create a new user
    "Mick Jagger" and also assign him a unique number.

    Do not modify this method."""

    existing = read_from_db()
    existing.append({
        "number": 1 if len(existing) == 0 else existing[-1]["number"] + 1,
        "first": first,
        "last": last
    })
    with open(PICKLE_DB, "wb") as handle:
        pickle.dump(existing, handle)

def read_from_db(criteria=None):
    """Read entries from the file-based DB subject to provided criteria

    Use this method to get users from the DB. The criteria parameters should
    either be omitted (returns all users) or be a dict that represents a query
    filter. For instance:
    - read_from_db({"number": 1}) will return a list of users with number 1
    - read_from_db({"first": "bob"}) will return a list of users whose first
    name is "bob".

    Do not modify this method."""
    if criteria is None:
        criteria = {}
    else:
        # remove empty criteria values
        for key in ("number", "first", "last"):
            if key in criteria and criteria[key] == "":
                del criteria[key]

        # cast number to int
        if "number" in criteria:
            criteria["number"] = int(criteria["number"])

    try:
        with open(PICKLE_DB, "rb") as handle:
            data = pickle.load(handle)

        filtered = []
        for entry in data:
            predicate = True

            for key, val in criteria.items():
                if val != entry[key]:
                    predicate = False

            if predicate:
                filtered.append(entry)

        return filtered
    except (IOError, EOFError):
        return []

# GET POST PARAMETERS
def get_request_params(table_parameters):
    if len(table_parameters) != 2 or (table_parameters[0].split("=")[0] != "first" or table_parameters[1].split("=")[0] != "last"):
        raise ValueError
    return table_parameters[0].strip().split("=")[1], table_parameters[1].strip().split("=")[1]

# GET GET PARAMETERS
def get_request_paramsGET(table_parameters):
    return dict((table_parameters[i].split("=")[0].lower(), table_parameters[i].strip().split("=")[1]) for i in range(len(table_parameters)) if (table_parameters[i].split("=")[0].lower() == "first" or table_parameters[i].split("=")[0].lower() == "last" or table_parameters[i].split("=")[0].lower() == "number") and (len(table_parameters[i].split("=")[1])) > 0)

# PARS ALL INPUT HEADERS (Host, Content-Length (post))
def parse_headers(client, method):
    headers = dict()
    POST_PARAMS = []
    while True:
        line = client.readline().decode("utf-8").strip()
        if not line:
            if not method == "GET":
                line = client.readline(int(headers["content-length"])).decode("utf-8").strip()
                POST_PARAMS = unquote_plus(line).split("&")
            break
        key, value = line.split(":", 1)
        headers[key.strip().lower()] = value.strip()
    return headers, POST_PARAMS

# GENERATE MULTI RESPONSES
def generate_response_message(client, response_body, content_type, response_header, title_message, message):
    body = response_body % ( title_message, message )
    head = response_header % ( content_type, len(body.encode("utf-8")) )
    client.write(head.encode("utf-8"))
    client.write(body.encode("utf-8"))

# GENERATE RESPONSE MESSAGE 200
def generate_response200(client, body_len, content_type, body):
    head = HEADER_RESPONSE_200 % (
        content_type,
        body_len
    )
    client.write(head.encode("utf-8"))
    client.write(body)

# CHECK WHICH GET PARAMETER IS INTERED AS A FILTER
def check_get_params(params_dict):
    frst = lst = numb = False
    database_dict = read_from_db()
    for key in params_dict:
        if key == "first":
            frst = True
        elif key == "last":
            lst = True
        elif key == "number":
            numb = True
    print(frst)
    if frst and lst and not numb:
        database_dict = read_from_db({"first": params_dict.get("first"), "last": params_dict.get("last")})
    elif frst and not lst and not numb:
        database_dict = read_from_db({"first": params_dict.get("first")})
    elif frst and not lst and numb:
        database_dict = read_from_db({"first": params_dict.get("first"), "number": params_dict.get("number")})
    elif not frst and lst and numb:
        database_dict = read_from_db({"last": params_dict.get("last"), "number": params_dict.get("number")})
    elif not frst and lst and not numb:
        database_dict = read_from_db({"last": params_dict.get("last")})
    elif not frst and not lst and numb:
        database_dict = read_from_db({"number": params_dict.get("number")})
    elif frst and lst and numb: database_dict = read_from_db({"first": params_dict.get("first"), "last": params_dict.get("last"), "number": params_dict.get("number")})
    print("DATABASE DICT: ")
    print(database_dict)
    return database_dict

def process_request(connection, address, port):
    client = connection.makefile("wrb")
    line = client.readline().decode("utf-8").strip()
    try:
        method, uri, version = line.split()
        # CHECK IF METHOD IS CORRECT
        if method != "GET" and method != "POST":
            generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_405, "405 Method not allowed", "Method " + method + " is not allowed.")
        # CHECK IF URI AND VERSION OF HTTP PROTOCOL IS CORRECT
        elif (len(uri) <= 0 or uri[0] != "/") or version != "HTTP/1.1":
            raise ValueError
        else:
            headers, req_parameters = parse_headers(client, method)
            if len(headers) <= 0 or "host" not in headers.keys() or (method == "POST" and "content-length" not in headers.keys()):
                raise ValueError
            if method == "GET" and not("?" not in uri):
                req_line = unquote_plus(uri.split("?")[1])
                req_parameters = req_line.split("&")    # tabela parametrov, ki pridejo po metodi GET

            # SAVING DATA TO DATABASE
            if uri == "/app-add":
                if method != "POST": generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_405, "405 Method not allowed", "Method " + method + " is not allowed.")
                else:
                    with open(WWW_DATA + "/" + "app_add.html", "rb") as handle: body = handle.read()
                    req_param1, req_param2 = get_request_params(req_parameters)
                    save_to_db(req_param1, req_param2)
                    # GENERATING RESPONSE 200
                    generate_response200(client, len(body), "text/html", body)

            # GENERATING TABLE BASED ON GET PARAMETERS
            elif "/app-index" in uri:
                if method != "GET": generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_405, "405 Method not allowed", "Method " + method + " is not allowed.")
                else:
                    with open(WWW_DATA + "/" + "app_list.html", "rb") as handle: body = handle.read()
                    html_table = ""
                    database_values = check_get_params(get_request_paramsGET(req_parameters))
                    if "{{students}}" in body.decode("utf-8"):
                        for user in database_values:
                            html_table = html_table + TABLE_ROW % (user.get("number"), user.get("first"), user.get("last"))
                        body = body.decode("utf-8").replace("{{students}}", html_table).encode("utf-8")
                    # GENERATING RESPONSE 200
                    generate_response200(client, len(body), "text/html", body)

            # GENERATING JSON RESPONSE
            elif uri == "/app-json":
                if method != "GET": generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_405, "405 Method not allowed", "Method " + method + " is not allowed.")
                else:
                    json_value = json.dumps(check_get_params(get_request_paramsGET(req_parameters)))
                    # GENERATING RESPONSE 200
                    generate_response200(client, len(json_value), "application/json", json_value.encode("utf-8"))

            else:
                # CHECK IF INPUT URI IS DIR OR FILE
                if uri[-1:] == "/" or isdir(uri[1:]):
                    path = "http://" + headers.get("host") + "/" + uri[1:] + "index.html"
                    body = RESPONSE_BODY % ("301 Moved Permanently", "Moved to location: " + path)
                    response_message = RESPONSE_301 % (len(body.encode("utf-8")), path)
                    client.write(response_message.encode("utf-8"))
                    client.write(body.encode("utf-8"))
                else:
                    # CHECK IF INPUT URI EXISTS AS FILE
                    with open(WWW_DATA + "/" + uri[1:], "rb") as handle:
                        body = handle.read()

                    mime_type = mimetypes.guess_type(uri)[0]
                    if mimetypes.guess_type(uri)[0] is None:
                        mime_type = "application/octet-stream"
                    # GENERATING RESPONSE 200
                    generate_response200(client, len(body), mime_type, body)
                    print("REQUEST WAS SUCCESSFUL")

    except AssertionError as e:
        print("Assertion error: %s" % e)
    except (ValueError, TypeError, KeyError) as e:
        generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_400, "400 Bad request", "Missing / Change required parameters.")
        print("Invalid value: %s" % e)
    except IOError as e:
        generate_response_message(client, RESPONSE_BODY, "text/html", RESPONSE_404, "404 Not Found", "Requested URL was not found on this server.")
        print("Invalid file or directory: %s" % e)
    finally:
        client.close()


def main(port):
    """Starts the server and waits for connections."""
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server.bind(("", port))
    server.listen(1)

    print("Listening on %d" % port)

    while True:
        connection, address = server.accept()
        print("[%s:%d] CONNECTED" % address)
        process_request(connection, address, port)
        connection.close()
        print("[%s:%d] DISCONNECTED" % address)


if __name__ == "__main__":
    main(8080)
