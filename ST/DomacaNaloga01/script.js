/** GLOBAL VARIABLES **/
const avtoPodatki = {
  "Honda": ["Civic Sedan", "Isight", "Acord", "Civic Si Sedan", "Accord Hybrid"],
  "Mercedes": ["CLA Coupe", "C-Class", "E-Class", "CLS Coupe", "S-Class Coupe"],
  "BMW": ["M2", "M3", "M4", "X5", "X3", "X1", "Class 3", "Class 5"],
  "Audi": ["RS6", "RS4", "R8", "A3", "A1", "A6", "A7", "Q3", "Q7"],
  "Renault": ["Clio", "Megan", "Megan RS"],
  "Peugeot": ["P 260", "P 408", "P 407", "P 508"],
  "Opel": ["Astra", "Insignia"],
  "Toyota": ["Yaris", "Sienta", "Porte", "Pirus C"],
  "Seat": ["Cupra Leon", "Leon", "Ibiza", "Azteka"]
};
const zanmkeAvtov = ["Honda", "Mercedes", "BMW", "Audi", "Renault", "Peugeot", "Opel", "Toyota", "Seat"];
const localStorageKeys = ["naziv", "znamka", "model", "cena", "leto",   "opis"];
let localTable = [];
let currentFilterId = "", firstTime = false, insertPage = false;
let naziv, znamka, model, cena, leto, opis, id;

/**SELECT OPTIONS ADDED DYNAMICLY**/
function createSelectOptions(elementID, array) {
  array.forEach(function (item, index) {
    let option = document.createElement("option");
    option.text = item;
    option.value = item;
    elementID.add(option);
  });
}

function changeModelSelection() {
  let modelSelect = document.getElementById("modelAvtomobila");
  modelSelect.options.length = 0;
  createSelectOptions(modelSelect, avtoPodatki[document.querySelector("#znamke").value]);
}

function fillSelection() {
  let zanmkeSelect = document.getElementById("znamke");
  let modelSelect = document.getElementById("modelAvtomobila");
  createSelectOptions(zanmkeSelect, zanmkeAvtov);
  createSelectOptions(modelSelect, avtoPodatki[zanmkeSelect.value]);
}

/** CREATE ALERT MESSAGES **/
function alertMessage(showElementId, hideElementId, hidePrev) {
  if(hidePrev)
    document.getElementById(hideElementId).style.display = "none";
  document.getElementById(showElementId).style.display = "block";
}

/** DELETE A ROW FROM A TABLE ON index.html **/
function returnRowId(thisElement) {
  localStorage.setItem("rowId", $(thisElement).closest("tr").find("td:eq(6)").text());
}

function findPartId(i) {
  for(let key in localTable[i])
    if(key === "id" && localTable[i][key] == localStorage.getItem('rowId'))
      return true;
  return false;
}

function deleteTableRow() {
  localStorage.clear();
  returnRowId(this);
  $(this).closest("tr").remove();
  let newArray = [];
  
  for(let i = 0; i < localTable.length; i++) {
    if(!findPartId(i))
      newArray.push(localTable[i]);
  }
  localStorage.setItem("tabela", JSON.stringify(newArray));
  getData();
  alertMessage("successMessage", "warningMessage", false);
}

/** UPDATE CAR PART VALUES **/
function initiCarPartsVars() {
  naziv = document.querySelector("#nazivAvtodela");
  znamka = document.querySelector("#znamke");
  model = document.querySelector("#modelAvtomobila");
  cena = document.querySelector("#cenaAvtodela");
  leto = document.querySelector("#letoProizvodnje");
  opis = document.querySelector("#opisAvtodela");
}

function loadUpdatePage() {
  returnRowId(this);
  location.href = "update.html";
}

function displayPartValues() {
  for(let i = 0; i < localTable.length; i++) {
    if(findPartId(i)) {
      naziv.value = localTable[i]["naziv"];
      znamka.value = localTable[i]["znamka"];
      changeModelSelection();
      model.value = localTable[i]["model"];
      cena.value = localTable[i]["cena"];
      leto.value = localTable[i]["leto"];
      opis.value = localTable[i]["opis"];
      break;
    }
  }
}

/** CREATE TABLE ON index.html **/
function createTableButton(tdElement, tr, nameClass, innerText, onClickFunction) {
  tdElement = document.createElement("td");
  let buttonElement = document.createElement("BUTTON");
  buttonElement.classList.add("button", nameClass);
  buttonElement.onclick = onClickFunction;
  buttonElement.innerHTML = innerText;
  tdElement.appendChild(buttonElement);
  tr.appendChild(tdElement);
}

function createTableTd(innerElement, key) {
    let tdElement = document.createElement("td");
    tdElement.innerHTML = innerElement;
    switch(key) {
      case "id": tdElement.style.display = "none"; break;
      case "cena": tdElement.innerHTML += "€"; break;
    }
    return tdElement;
}

function addDelToTable() {
  const htmlTable = document.getElementById("avtodeliTable");
  if(localStorage !== undefined) {
    for(let i = 0; i < localTable.length; i++) {
      const tr = document.createElement("tr");
      htmlTable.appendChild(tr);

      for(let key in localTable[i])
        tr.appendChild(createTableTd(localTable[i][key], key));
      
      createTableButton(document.createElement("td"), tr, "button-update", "Uredi", loadUpdatePage);
      createTableButton(document.createElement("td"), tr, "button-delete", "Izbriši", deleteTableRow);
    }
  }else
    console.log("LOCAL STORAGE JE PRAZEN!");
}

/** INSERT NEW CAR PART **/
function clearFormInputs() {
  document.getElementById("form").reset();
}

function addAvtoDel() {
  initiCarPartsVars();
  if(!naziv.checkValidity() || !model.checkValidity() || !znamka.checkValidity() || cena.value === "" || !leto.checkValidity() || opis.value === "")
    alertMessage("warningMessage", "successMessage", true);
  else {
    alertMessage("successMessage", "warningMessage", true);
      const vneseniElementi = {
        naziv: naziv.value,
        znamka: znamka.value,
        model: model.value,
        cena: cena.value,
        leto: leto.value,
        opis: opis.value,
        id: 0
      };

      if(insertPage) {
        vneseniElementi.id = Math.random().toString(36).substring(2, 8) + Math.random().toString(36).substring(2, 8);
        localTable.push(vneseniElementi);
        localStorage.setItem("tabela", JSON.stringify(localTable));
        clearFormInputs();
        changeModelSelection(); //poklice, da resetira <select> za modele avtomobilov (ko se doda novi element, ostane izbran prejšnji model avtomobila)
      } 
      else {
        localStorage.removeItem("tabela");
        let newArray = [];
        for(let i = 0; i < localTable.length; i++) {
          if(findPartId(i)) {
            vneseniElementi.id = localStorage.getItem("rowId");
            newArray.push(vneseniElementi);
          }
          else  
            newArray.push(localTable[i]);
        }
        localStorage.setItem("tabela", JSON.stringify(newArray));
      }
  }
}

/** FILTER BUTTONS **/
function changeActiveElement() {
  let buttons = document.getElementById("filterButtonsContainer").getElementsByClassName("filterButton");
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function(){
      let current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
    });
  }
}

function createFilterButtons(documentId, first, textValue, idName) {
  let button = document.createElement("button");
  if(first)
    button.classList.add("filterButton", "active");
  else 
    button.className = "filterButton";
  button.id = idName.toLowerCase();
  button.onclick = showFilterElements;
  button.innerText = textValue;
  documentId.appendChild(button);
}

function generateFilterButtons() {
  let documentId =  document.getElementById("filterButtonsContainer");
  createFilterButtons(documentId, true, "Prikaži vse", "all");

  zanmkeAvtov.forEach(function(element) {
    createFilterButtons(documentId, false, element, element);
  });
}

/** CREATE FILTER TABLE ON filter.html **/
function listAllCars(htmlTable) {
  for(let i = 0; i < localTable.length; i++) {
    const tr = document.createElement("tr");
    tr.className = "carParts";
    htmlTable.appendChild(tr);

    for(let key in localTable[i])
      tr.appendChild(createTableTd(localTable[i][key], key));
  }
}

function showFilterElements() {
  const htmlTable = document.getElementById("filterTable");

  if(currentFilterId === "") {
    listAllCars(htmlTable);
    currentFilterId = "all";
    firstTime = false;
  }
  else if(currentFilterId !== this.id) {
    $(".carParts").remove();
    currentFilterId = this.id;
    firstTime = true;
  }
  if(firstTime) {
    if(currentFilterId === "all")
      listAllCars(htmlTable);
    else
      for(let i = 0; i < localTable.length; i++) {
        if(localTable[i].znamka.toLowerCase() == currentFilterId) {
          const tr = document.createElement("tr");
          tr.className = "carParts";
          htmlTable.appendChild(tr);
  
          for(let key in localTable[i])
            tr.appendChild(createTableTd(localTable[i][key], key));
        }
      }
    firstTime = false;
  }
}

/** OTHER FUNCTION **/
function getData() {
  localTable = JSON.parse(localStorage.getItem("tabela"));
  if (!localTable)
    localTable = [];
}

/** MAIN EVENT LISTENER **/
document.addEventListener("DOMContentLoaded", () => {
  getData();
  switch("/" + window.location.pathname.split('/').pop().split('#')[0].split('?')[0]) {
    case "/":
    case "/index.html": {
      addDelToTable();
      document.getElementById("successMessage").style.display = "none";
    }; break;
    case "/filter.html": {
      showFilterElements();
      generateFilterButtons();
      changeActiveElement();
    }; break;
    case "/insert.html": {
      fillSelection();
      insertPage = true;
      document.getElementById("insertButton").onclick = addAvtoDel;
    }; break;
    case "/update.html": {
      initiCarPartsVars();
      fillSelection();
      displayPartValues();
      document.getElementById("updateButton").onclick = addAvtoDel;
    }; break;
  }
})