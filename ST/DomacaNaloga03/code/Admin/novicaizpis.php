<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

  	if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
  		$action = mysqli_real_escape_string($conn, $_GET['action']);
  		$id = mysqli_real_escape_string($conn, $_GET['id']);

  		if($action === 'izbris' && is_numeric($id) && $id >= 0){
			$status = 'Ok';
			$message = "Vnešeni zapis je bil izbrisan iz baze.";
  			$query = 'DELETE n.* FROM novica n WHERE n.ObjavaID = ?';
  			$stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          	mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          	mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
          	mysqli_execute($stavek) or $status = 'notOk';

          	if($status == 'Ok'){
          		$query = 'DELETE k.* FROM komentarji k WHERE k.ObjavaID = ?';
          		$stavek = mysqli_stmt_init($conn) or $status = 'notOk';
	          	mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
	          	mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
	          	mysqli_execute($stavek) or $status = 'notOk';
	          	sleep(1);
			}
		}
		else {
			$status = 'notOk';
			$message = "Prišlo je do napake pri brisanju. Pravilnost vnosnih polj ali vnešenih parametrov strani.";
		}
	  }
?>

<html>
	<head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
		    <section>
	            <div class="Desna">
	                <div class="container12">
	                  <div class="Naslov" style="margin-top: 0%;"><span>Zapisi v tabeli novica</span></div>
		                <?php
							/*FUNCTION FROM ../funkcije_selekcije.php*/
							if(isset($status))
								getResult($conn, $status, $message);
							$query = 'SELECT * FROM novica ORDER BY Datum_objave DESC';
							$rezultat = mysqli_query($conn,$query);
							$counter = 1;

							echo '<div style="overflow-x:auto;">';
							echo '<table>
							<tr id="Prva">
								<td id="Counter">Številka</td>
								<td>Datum</td>
								<td>Naslov</td>
								<td>Kratek opis</td>
								<td>Vabilo</td>
								<td>Update</td>
								<td>Izbris</td>
							</tr>
							';

							while($tab = mysqli_fetch_assoc($rezultat)){
								$datum = date_create($tab['Datum_objave']);
							echo '
								<tr>
								<td>'.$counter.'</td>
								<td>'.date_format($datum, 'd. m. Y').'</td>
								<td>'.$tab['Naslov'].'</td>
								<td>'.$tab['KratekOpis'].'</td>';
								
								if($tab['Vabilo'] == 1){
									echo '<td>DA</td>';
								}

								else{
									echo '<td>NE</td>';
								}

								echo '
								<td><a href="novicaupdate.php?id='.$tab['ObjavaID'].'" id="ikoncecheck"><i class="far fa-edit"></i></a></td>
								<td><a href="novicaizpis.php?action=izbris&id='.$tab['ObjavaID'].'" id="ikonce"><i class="far fa-trash-alt"></i></a></td>
								</tr>
								';
								$counter++;
							}
							echo '</table>';
							echo '</div>';
	                    ?>  
	                    <div class="row12">
	                   		<a href="adminpage.php" id="Refresh">Nazaj</a>
	                   </div>
	                </div>
	            </div>
        	</section>
        </div>
	</body>
	<?php
    require("Function.php");
  ?>
</html>