<?php
	require("Odjava.php");
    require("../connect.php");
    require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: novicaizpis.php");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "novica", "ObjavaID")) {
		header("Location: novicaizpis.php");
		exit();
	}

	if(isset($_POST['NovicaUpdate']) && !empty($_POST['NovicaUpdate'])){
		$folder = '../Slike/';
		$naslov = mysqli_real_escape_string($conn, $_POST['ImeNovice']);
		$datum = mysqli_real_escape_string($conn, $_POST['DatumObjave']);
		$slika = mysqli_real_escape_string($conn, imageCall($folder, 'Slika'));
		$vsebina = $_POST['Vsebina'];
		$opis = mysqli_real_escape_string($conn, $_POST['KratekOpis']);

		if($slika == null)
			$slika = getUniversialById($conn, $id, 'Slika', 'novica', 'ObjavaID');
		if(isset($_POST['Vabilo']))
			$vabilo = mysqli_real_escape_string($conn, $_POST['Vabilo']);
		else
			$vabilo = mysqli_real_escape_string($conn, 0);
		
		if(strlen($naslov) > 255 || strlen($datum) <= 0 || $slika === null || strlen($opis) > 255){
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
		else {
			$query = 'UPDATE novica SET Datum_objave = ?, Slika = ?, Naslov = ?, Vsebina = ?, KratekOpis = ?, Vabilo = ? WHERE ObjavaID = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "sssssii", $datum, $slika, $naslov, $vsebina, $opis, $vabilo, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';

			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}	
	}
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
        <script src="tinymce/tinymce.min.js"></script>
    </head>

    <?php
        require_once('texteditorplugin.php');
    ?>

<!--________________________________________________________________________________-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>

        <div class="wrapper12">
            <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
            <section>
                <div class="Desna">
                    <div class="container12">
                        <div class="Naslov"><span>Dodaj novico</span>
                          <a href="novicaizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                        </div>
                        <?php
                          	/*FUNCTION FROM ../funkcije_selekcije.php*/
                          	if(isset($status))
								  getResult($conn, $status, $message);
								  
							$query = 'SELECT * FROM novica WHERE ObjavaID = ?';
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $id);
							mysqli_execute($stavek);
							@$rezultat = mysqli_stmt_get_result($stavek);

							$tab = mysqli_fetch_assoc($rezultat);
							//$selekcija = $tab['Selekcija'];

							echo '
								<form action="novicaupdate.php?id='.$tab['ObjavaID'].'" method="post" enctype="multipart/form-data">
									<div class="row12">
										<div class="col-25">
											<label for="fname">Naslov novice:* </label>
										</div>
										<div class="col-75">
											<input type="text" id="fname" name="ImeNovice" maxlength="255" minlength="10" value="'.$tab['Naslov'].'" required/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label for="date">Datum objave:* </label>
										</div>
										<div class="col-75">
											<input type="date" id="SmallNumber" name="DatumObjave" value="'.$tab['Datum_objave'].'" required/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label for="lname">Naloži sliko:* </label>
										</div>
										<div class="col-75" id = "Slika">
											<img class="card-img-top" style="width: 150px; height: 100px;" src="../Slike/'.$tab['Slika'].'" alt="Card image cap"><br/>
											<input type="file" name="Slika" style="margin-top: 1%;"/><br/><br/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label for="subject">Vsebina:*</label>
										</div>
										<div class="col-75">
											<textarea id="subject" name="Vsebina" style="height:100px">'.$tab['Vsebina'].'</textarea>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label for="subject">Kratek opis:*</label>
										</div>
										<div class="col-75">
											<textarea id="subject1" name="KratekOpis" maxlength="255" minlength="30" style="height:100px">'.$tab['KratekOpis'].'</textarea>
										</div>
									</div>

									<div class="row12" style="margin-top: 2%;">
									<div class="col-25">
										<label for="subject">Novica kot vabilo:*</label>
									</div>
									<div class="col-75">
										<label class="checkCont">
											<input type="checkbox" name="Vabilo" value="1"/>
											<span class="checkmark"></span>
										</label>
									</div>
									</div>

									<div class="row12">
									<input type="submit" name="NovicaUpdate" value="Posodobi podatke">
										
									<a href="novicaupdate.php?id='.$tab['ObjavaID'].'" id="Refresh">Osveži stran</a>
									
									</div> 
										
								</form>

							';
                       	?>
               
                    </div>
                </div>
            </section>
        </div>
    </body>
    <?php
        require("Function.php");
    ?>
</html>