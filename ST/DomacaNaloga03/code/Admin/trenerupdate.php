<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: trenerizpis.php");
		exit();
	}
	/** PREVERI ALI ID, PODAN V $_GET, RES PRIPADA KAKSNEMU TRENRJU **/
	if(!checkUpdatedSite($conn, $id, "trener", "TrenerID")) {
		header("Location: trenerizpis.php");
		exit();
	}

	if(isset($_POST['TrenerUpdate']) && !empty($_POST['TrenerUpdate'])){
		$ime = mysqli_real_escape_string($conn, $_POST['Ime']);
		$priimek = mysqli_real_escape_string($conn, $_POST['Priimek']);
		$gmail = mysqli_real_escape_string($conn, $_POST['gmailTrener']);
		$telefon = mysqli_real_escape_string($conn, $_POST['telefonTrener']);
		$selekcija = mysqli_real_escape_string($conn, $_POST['selekcija']);
		
		if(empty($selekcija) || strlen($ime) > 25 || strlen($priimek) > 30 || strlen($telefon) > 11 || strlen($telefon) < 9 || !strpos($gmail, '@') || strlen($gmail) > 40 || strlen($gmail) < 12) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		} else {
			$query = 'UPDATE trener SET Priimek = ?, Ime = ?, Gmail = ?, Telefonska_stevilka = ?, SelekcijaID = ? WHERE TrenerID = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "ssssii", $priimek, $ime, $gmail, $telefon, $selekcija, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';

			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}		
	}
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
      <div class="wrapper12">
        <?php
          /*INCLUDE NAVBAR FROM navbar.php*/
          require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class = "Naslov"><span>Dodaj trenerja</span>
                        <a href = "trenerizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                        	if(isset($status))
                            	getResult($conn, $status, $message);
							$query = 'SELECT * FROM trener WHERE TrenerID = ?';
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $id);
							mysqli_execute($stavek);
							@$rezultat = mysqli_stmt_get_result($stavek);
							$tab = mysqli_fetch_assoc($rezultat);
	                    
	                    	echo '
	                    		<form action="trenerupdate.php?id='.$tab['TrenerID'].'" method="post">
				                       <div class="row12">
				                              <div class="col-25">
				                                <label for="fname">Ime trenerja:* </label>
				                              </div>
				                              <div class="col-75">
				                                <input type="text" name="Ime" maxlength="25" minlength="2" value="'.$tab['Ime'].'" required>
				                              </div>
				                        </div>

				                        <div class="row12">
				                              <div class="col-25">
				                                <label for="fname">Priimek trenerja:* </label>
				                              </div>
				                              <div class="col-75">
				                                <input type="text" name="Priimek" maxlength="30" minlength="2" value="'.$tab['Priimek'].'" required>
				                              </div>
				                        </div>

				                        <div class="row12">

				                          <div class="col-25">
				                            <label>Gmail</label>
				                          </div>

				                          <div class="col-75">
				                            <input type="text" name="gmailTrener" value="'.$tab['Gmail'].'" maxlength="40" minlength="12" placeholder="janez.novak@gmail.com"/>
				                          </div>

				                        </div>

				                        <div class="row12">

				                          <div class="col-25">
				                            <label>Telefonska številka</label>
				                          </div>

				                          <div class="col-75">
				                            <input id="SmallNumber" type="text" value="'.$tab['Telefonska_stevilka'].'" name="telefonTrener" maxlength="11" minlength="9" required/>
				                          </div>
				                          
				                        </div>

				                        <div class="row12">
				                              <div class="col-25">
				                                <label for = "date">ID Selekcije:* </label>
				                              </div>
				                              <div class="col-75">
				                                <select id="SmallNumber" name="selekcija" >';

					                                $query = "SELECT SelekcijaID, Naziv FROM selekcija ORDER BY SelekcijaID ASC";
					                                $stavek = mysqli_query($conn, $query);

					                                while($teb = mysqli_fetch_assoc($stavek)){
					                                  $query = "SELECT SelekcijaID FROM trener where SelekcijaID = ".$tab['SelekcijaID'];
					                                  $stavk = mysqli_query($conn, $query);
					                                  $result = mysqli_fetch_assoc($stavk);

					                                  if($result['SelekcijaID'] == $teb['SelekcijaID']){
					                                    echo '<option value="'.$teb['SelekcijaID'].'" selected>'.$teb['Naziv'].'</option>';
					                                  }
					                                  else 
					                                    echo '<option value="'.$teb['SelekcijaID'].'">'.$teb['Naziv'].'</option>';
					                                }
					                                echo '
				                              </select>
				                              </div>
				                        </div>

				                        <div class="row12">
				                          <input type="submit" name="TrenerUpdate" value="Posodobi podatke">

				                          <a href="trenerupdate.php?id='.$tab['TrenerID'].'" id="Refresh">Osveži stran</a>
				                        </div>
				                    </form>
	                    	';
	                    ?>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>