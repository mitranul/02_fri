<?php
  require("Odjava.php");
  require("../connect.php");
  require("../funkcije_selekcije.php");

  if(isset($_POST['DodajObvestilo']) && !empty($_POST['DodajObvestilo'])){
    $selekcija = mysqli_real_escape_string($conn, $_POST['selekcija']);
    $vsebina = $_POST['Obvestilo'];
    $datumob = mysqli_real_escape_string($conn, $_POST['Datumob']);
    $naslov = mysqli_real_escape_string($conn, $_POST['Naslov']);

    if(strlen($naslov) > 80 || empty($_POST['Naslov']) || empty($_POST['Datumob']) || empty($_POST['selekcija'])){
      $status = 'notOk';
      $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
    }
    else {
      $query = "INSERT INTO vsebina(SelekcijaID, Naslov, Vsebina, DatumObjave) VALUES(?, ?, ?, ?)";
      $stavek = mysqli_prepare($conn, $query);
      mysqli_stmt_bind_param($stavek, "isss", $selekcija, $naslov, $vsebina, $datumob);
      mysqli_stmt_execute($stavek) or $status = 'notOk';

      if(mysqli_affected_rows($conn) > 0) {
        $status = 'Ok';
        $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
      }
    }
  }
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
        <script src="tinymce/tinymce.min.js"></script>
    </head>
      <?php
          require_once('texteditorplugin.php');
      ?>
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>

      <div class="wrapper12">  
        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class="Naslov"><span>Dodaj obvestilo</span>
                      <a href="obvestilaizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                    </div>
                    <?php
                      /*FUNCTION FROM ../funkcije_selekcije.php*/
                      if(isset($status))
                          getResult($conn, $status, $message);
                    ?>

                    <form action="obvestila.php" method="post">

                        <div class="row12">
                              <div class="col-25">
                                <label for="subject">Naslov:*</label>
                              </div>
                              <div class="col-75">
                                <input type="text" id="SmallNumber" name="Naslov" maxlength="80" minlength="5" placeholder="Napiši naslov.." required></input>
                              </div>
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label for="date">Datum obvestila:* </label>
                              </div>
                              <div class="col-75">
                                <input type="date" id="SmallNumber" name="Datumob" required>
                              </div>
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label for="date">ID Selekcije:* </label>
                              </div>
                              <div class="col-75">
                                <!--input id = "SmallNumber" list = "browsers2" name="selekcija" placeholder="Izberi selekcijo.." required-->
                                <select id="SmallNumber" name="selekcija">
                                <?php
                                  getSelekcija($conn);
                                ?>
                              </select>
                              </div>
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label for="subject">Obvestilo:*</label>
                              </div>
                              <div class="col-75">
                                <textarea id="subject" name="Obvestilo" placeholder="Napiši obvestilo.." style="height:200px"></textarea>
                              </div>
                        </div>

                        <div class="row12">
                          <input type="submit" name="DodajObvestilo" value="Dodaj v bazo">

                          <a href="obvestila.php" id="Refresh">Osveži stran</a>
                        </div>

                    </form>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>