<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_POST['dodajSponzorja']) && !empty($_POST['dodajSponzorja'])){
    $folder = '../Sponzorji/';
		$naziv = mysqli_real_escape_string($conn, $_POST['naziv']);
    $slika = mysqli_real_escape_string($conn, imageCall($folder, 'slika'));

    if($slika === null && strlen($naziv) > 30 && strlen($naziv) < 3) {
      $status = 'notOk';
      $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
    }
    else {
      $query = 'INSERT INTO sponzorji (Naziv, Slika) VALUES (?, ?)';
      $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
      mysqli_stmt_bind_param($stavek, "ss", $naziv, $slika) or $status = 'notOk';
      mysqli_stmt_execute($stavek) or $status = 'notOk';
      
      if(mysqli_affected_rows($conn) > 0) {
        $status = 'Ok';
        $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
      };
    }
	}
?>

<html>
	<head>
        <?php
        	/*REQUEST FROM head.php*/
        	require_once("head.php");
        ?>
    </head>
	
	<body>
		<header>
        	<?php
            	/*INCLUDE HEADER FROM header.php*/
        		require_once("header.php");
        	?>
        </header>

        <div class="wrapper12">
	        <?php
	          /*INCLUDE NAVBAR FROM navbar.php*/
	          require_once("navbar.php");
	        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    
                    <div class = "Naslov"><span>Dodaj sponzorja</span>
                        <a href = "sponzorjiizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                          /*FUNCTION FROM ../funkcije_selekcije.php*/
                          if(isset($status))
                              getResult($conn, $status, $message);
                        ?>
                    
                      <form action="sponzorji.php" method="post" enctype="multipart/form-data">
                       <div class="row12">
                              <div class="col-25">
                                <label>Naziv sponzorja:* </label>
                              </div>
                              <div class="col-75">
                                <input type="text" name="naziv" maxlength="30" minlength="3" placeholder="Vnesite naziv sponzorja" required/>
                              </div>
                        </div>

                        <div class = "row12">
                            <div class="col-25">
                                <label for="lname">Naloži sliko:* </label>
                            </div>
                            <div class = "col-75" id = "Slika">
                                <input type="file" name="slika" style="margin-top: 1%;" required/><br/><br/>
                            </div>
                        </div>

                        <div class="row12">
                          <input type="submit" name="dodajSponzorja" value="Dodaj v bazo">

                          <a href="sponzorji.php" id="Refresh">Osveži stran</a>
                        </div>
                    </form>
                </div>
            </div>
        </section>
      </div>
    </body>

	<?php
		require("Function.php");
	?>
</html>