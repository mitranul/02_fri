<?php
	require("Odjava.php");
	require("../connect.php");
    require("../funkcije_selekcije.php");

    if (isset($_POST['createaccount']) && !empty($_POST['createaccount'])) {
        $message = "Novi uporabnik je bil uspešno dodan v bazo.";
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $passwordS = mysqli_real_escape_string($conn, $_POST['passwordSec']);
        $datum = date("Y-m-d");

        $queryname = 'SELECT Username FROM admin WHERE Username = ?';
        $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $queryname);
        mysqli_stmt_bind_param($stavek, "s", $username);
        mysqli_execute($stavek);
        $rezultat = mysqli_stmt_get_result($stavek);
        $uporabnikBaza = mysqli_fetch_assoc($rezultat);

        if($uporabnikBaza !== null) {
            $status = 'notOk';
            $message = "Vnešeni uporabnik že obstaja. Vnesite drugo uporabniško ime.";
        }
        else if($password === $passwordS) {
            $status = 'Ok';
            if(is_numeric($passwordS) || is_numeric($password) || strlen($password) < 8 || strlen($password) > 20) {
                $status = 'notOk';
                $message = "Vnešeno geslo mora vsebovati 8-20 znakov ([0-9], [A-z]).";
            } else {
                $newPassword = hash("sha256", $password . $datum);
                $query = "INSERT INTO admin(Username, Password, Datum) VALUES(?, ?, ?)";
                $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
                mysqli_stmt_bind_param($stavek, "sss", $username, $newPassword, $datum) or $status = 'notOk';
                mysqli_stmt_execute($stavek) or $status = 'notOk';

                if(mysqli_affected_rows($conn) > 0) {
                    $status = 'Ok';
                    $message = "Uporabnik je bil uspešno dodan v bazo.";
                }
                else {
                    $status = 'notOk';
                    $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
                }
            }

        } else {
            $status = 'notOk';
            $message = "Geslo in ponovljeno geslo se ne ujemata. Vnesite ponovno.";
        }
    }
?>

<html>
	<head>
        <?php
        	/*REQUEST FROM head.php*/
        	require_once("head.php");
        ?>
    </head>
	
	<body>
		<header>
        	<?php
            	/*INCLUDE HEADER FROM header.php*/
        		require_once("header.php");
        	?>
        </header>

        <div class="wrapper12">
	        <?php
	          /*INCLUDE NAVBAR FROM navbar.php*/
	          require_once("navbar.php");
	        ?>
            <section>
                <div class="Desna">
                    <div class="container12">
                        <div class="Naslov"><span>Dodaj novega administratorja</span>
                            <!--a href="funkcijeizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a-->
                            <a href="createnewizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                        </div>
                        <?php
                        /*FUNCTION FROM ../funkcije_selekcije.php*/
                        if(isset($status))
                            getResult($conn, $status, $message);
                        ?>
                        <form action="createnew.php" method="post">
                            <div class="row12">
                                <div class="col-25">
                                    <label>Uporabniško ime:* </label>
                                </div>
                                <div class="col-75">
                                    <input id="SmallNumber" type="text" name="username" maxlength="25" minlength="3" placeholder="Vnesite uporabniško ime" required/>
                                </div>
                            </div>

                            <div class="row12">
                                <div class="col-25">
                                    <label>Geslo uporabnika (min. 8, max. 20):* </label>
                                </div>
                                <div class="col-75">
                                    <input id="SmallNumber" type="password" name="password" maxlength="20" minlength="8" placeholder="Vnesite geslo" required/>
                                </div>
                            </div>

                            <div class="row12">
                                <div class="col-25">
                                    <label>Ponovni geslo:* </label>
                                </div>
                                <div class="col-75">
                                    <input id="SmallNumber" type="password" name="passwordSec" maxlength="20" minlength="8" placeholder="Vnesite geslo" required/>
                                </div>
                            </div>

                            <div class="row12">
                                <input type="submit" name="createaccount" value="Dodaj v bazo">

                                <a href="createnew.php" id="Refresh">Osveži stran</a>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </body>

	<?php
		require("Function.php");
	?>
</html>