<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");
	
  	if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
  		$action = mysqli_real_escape_string($conn, $_GET['action']);
  		$id = mysqli_real_escape_string($conn, $_GET['id']);
		
  		if($action === 'izbris' && is_numeric($id) && $id >= 0){
			$status = 'Ok';
			$message = "Vnešeni zapis je bil izbrisan iz baze.";
  			$query = 'DELETE FROM trener WHERE TrenerID = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk';
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';
			sleep(1);
		}
		else {
			$status = 'notOk';
			$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
		}
  	}
?>

<html>
	<head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
 
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
		    <section>
	            <div class="Desna">
	                <div class="container12">
	                  	<div class="Naslov" style="margin-top: 0%;"><span>Zapisi v tabeli trener</span>
	                  	</div>
	                    <?php
                        	if(isset($status))
                            	getResult($conn, $status, $message);
							$query = 'SELECT t.*, s.Naziv FROM trener t inner join selekcija s on(s.SelekcijaID = t.SelekcijaID) ORDER BY t.TrenerID DESC';
							$rezultat = mysqli_query($conn,$query);
							$counter = 1;

							echo '<div style="overflow-x:auto;">';
							echo '<table>
							<tr id="Prva">
								<td id ="Counter">Številka</td>
								<td>Ime</td>
								<td>Priimek</td>
								<td>Gmail</td>
								<td>Telefon</td>
								<td>Selekcija</td>
								<td>Update</td>
								<td>Izbris</td>
							</tr>
							';

							while($tab = mysqli_fetch_assoc($rezultat)){
							echo '
								<tr>
								<td>'.$counter.'</td>
								<td>'.$tab['Ime'].'</td>
								<td>'.$tab['Priimek'].'</td>
								<td>'.$tab['Gmail'].'</td>
								<td>'.$tab['Telefonska_stevilka'].'</td>
								<td>'.$tab['Naziv'].'</td>
								<td><a href="trenerupdate.php?id='.$tab['TrenerID'].'" id="ikoncecheck"><i class="far fa-edit"></i></a></td>
								<td><a href="trenerizpis.php?action=izbris&id='.$tab['TrenerID'].'" id="ikonce"><i class="far fa-trash-alt"></i></a></td>
								</tr>
								';
								$counter++;
							}
							echo '</table>';
							echo '</div>';
	                    ?>
	                    <div class="row12">
	                   		<a href="trener.php" id="Refresh">Nazaj</a>
	                  	</div>
	                </div>
	            </div>
        	</section>
        </div>
	</body>
	<?php
	    require("Function.php");
	?>
</html>