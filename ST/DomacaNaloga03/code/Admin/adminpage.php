<?php
    require("Odjava.php");
    require("../connect.php");
    require("../funkcije_selekcije.php");

    if(isset($_POST['DodajBazo']) && !empty($_POST['DodajBazo'])){
      $folder = '../Slike/';
      $imenovice = mysqli_real_escape_string($conn, $_POST['ImeNovice']);
      $datum = mysqli_real_escape_string($conn, $_POST['DatumObjave']);
      $slika = mysqli_real_escape_string($conn, imageCall($folder, 'Slika'));
      $kopis = mysqli_real_escape_string($conn, $_POST['KratekOpis']);
      $vsebina = $_POST['Vsebina'];
      
      if(isset($_POST['Vabilo']))
        $vabilo = mysqli_real_escape_string($conn, $_POST['Vabilo']);
      else
        $vabilo = mysqli_real_escape_string($conn, 0);
  
      if($slika == null) {
        $status = 'notOk';
        $message = "Izbrana slika ni bila naložena. Poskusite izbrati drugo sliko.";
      }
      else if(strlen($imenovice) > 255 || strlen($datum) <= 0 || strlen($kopis) > 255){
        $status = 'notOk';
        $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
      }
      else {
        $query = "INSERT INTO novica(Datum_objave, Slika, Naslov, Vsebina, KratekOpis, Vabilo) VALUES(?, ?, ?, ?, ?, ?)";
        $stavek = mysqli_prepare($conn, $query);
        mysqli_stmt_bind_param($stavek, "sssssi", $datum, $slika, $imenovice, $vsebina, $kopis, $vabilo);
        mysqli_stmt_execute($stavek) or $status = 'notOk';
    
        if(mysqli_affected_rows($conn) > 0) {
          $status = 'Ok';
          $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
        }
        else {
          $status = 'notOk';
          $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
        }
      }
    }
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
        <script src="tinymce/tinymce.min.js"></script>
    </head>
      <?php
        require_once('texteditorplugin.php');
      ?>
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>
        
        <div class="wrapper12">
            <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>

            <section>
                <div class = "Desna">
                    <div class="container12">
                        <div class="Naslov"><span>Dodaj novico</span>
                          <a href="novicaizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                        </div>
                        <?php
                          if(isset($status))
                            getResult($conn, $status, $message);
                        ?>
                        
                          <form action="adminpage.php" method="post" enctype="multipart/form-data">
                              <div class="row12">
                                    <div class="col-25">
                                      <label for="fname">Naslov novice:* </label>
                                    </div>
                                    <div class="col-75">
                                      <input type="text" id="fname" name="ImeNovice" maxlength="255" minlength="5" placeholder="Ime novice" required>
                                    </div>
                              </div>

                              <div class="row12">
                                    <div class="col-25">
                                      <label for="date">Datum objave:* </label>
                                    </div>
                                    <div class="col-75">
                                      <input type="date" id="SmallNumber" name="DatumObjave" required>
                                    </div>
                              </div>
                              
                              <div class = "row12">
                                  <div class="col-25">
                                      <label for="lname">Naloži sliko:* </label>
                                  </div>
                                  <div class="col-75" id = "Slika">
                                      <input type="file" name="Slika" style="margin-top: 1%;" required></input><br><br>
                                  </div>
                              </div>

                              <div class="row12">
                                    <div class="col-25">
                                      <label for="subject">Vsebina:*</label>
                                    </div>
                                    <div class="col-75">
                                      <textarea id="subject" name="Vsebina" value="SUSNIK" placeholder="Dodaj vsebino.." style="height:100px"></textarea>
                                    </div>
                              </div>

                              <div class="row12">
                                    <div class="col-25">
                                      <label for="subject">Kratek opis:*</label>
                                    </div>
                                    <div class="col-75">
                                      <textarea id="subject1" name="KratekOpis" maxlength="255" placeholder="Dodaj opis.." style="height:100px"></textarea>
                                    </div>
                              </div>

                              <div class="row12" style="margin-top: 2%;">
                                <div class="col-25">
                                  <label for="subject">Novica kot vabilo:*</label>
                                </div>
                                <div class = "col-75">
                                  <label class="checkCont">
                                    <input type = "checkbox" name="Vabilo" value = "1"/>
                                    <span class="checkmark"></span>
                                  </label>
                                </div>
                              </div>

                              <div class="row12">
                                <input type="submit" name="DodajBazo" value="Dodaj v bazo">
                                  
                                <a href="adminpage.php" id="Refresh">Osveži stran</a>
                              </div> 
                              
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </body>
    <?php
        require("Function.php");
    ?>
</html>