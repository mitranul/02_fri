<?php
	require("Odjava.php");
?>

<aside>
    <div class="Leva">
        <div class="SideBar">
            <a href="adminpage.php"><i class="far fa-newspaper"></i> Novice</a>
            <!--a href = "igralci.php"><i class="fas fa-user"></i> Igralci</a-->
            <!--a href = "tekma.php"><i class="fas fa-futbol"></i> Tekma</a-->
            <a href="trener.php"><i class="far fa-user"></i> Trenerji</a>
            <a href="obvestila.php"><i class="fas fa-bell"></i> Obvestila</a>
            <!--a href = "nasprotnik.php"><i class="fas fa-futbol"></i> Nasprotniki</a-->
            <!--a href = "dodajobrazce.php"><i class="fas fa-clipboard"></i> Obrazci</a-->
            <?php
              $query = 'SELECT count(*) as "Stevilo" FROM komentarji WHERE Preverjeno = ?';
              $y = 0;
              $stavek = mysqli_stmt_init($conn);
              mysqli_stmt_prepare($stavek, $query);
              mysqli_stmt_bind_param($stavek, "i", $y);
              mysqli_execute($stavek);
              @$rezultat = mysqli_stmt_get_result($stavek);

              $tab = mysqli_fetch_assoc($rezultat);

              echo '<a href="komentarji.php"><i class="fas fa-comments"></i> Komentarji ('.$tab['Stevilo'].')</a>';;
            ?>
            <a href="selekcijasezona.php"><i class="fas fa-clipboard-list"></i> Selekcija</a>
            <a href="funkcije.php"><i class="fas fa-clipboard-list"></i> Klubske funkcije</a>
            <a href="sponzorji.php"><i class="fas fa-clipboard-list"></i> Sponzorji</a>
            <a href="createnew.php"><i class="far fa-user"></i> Registracija</a>
        </div>
    </div>
</aside>