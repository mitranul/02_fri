<?php
    require("Odjava.php");
?>

<meta charset = "UTF-8">
<link rel = "shortcut icon" href = "../Slike/logo22.png">
<title>NK Dol-admin page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel = "stylesheet" type = "text/css" href = "../Bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="admin.css">
<link rel="stylesheet" type="text/css" href="admin-responsive.css">
<link rel="stylesheet" type="text/css" href="checkbox.css">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel = "stylesheet" type = "text/css" href = "../fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
<script src = "../fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
<script src = "../Jquery/jquery.min.js"></script>