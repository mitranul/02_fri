<?php 
// Pričnemo sejo
    if (!isset($_SESSION))
        session_start();
    
    // Preverimo, če je uporabnik prijavljen
    // Preverimo, če  uporabnik še ni prijavljen
    if (!isset($_SESSION['prijava_uspesna']) || !$_SESSION['prijava_uspesna']) {
        http_response_code(404);
        exit();
    }
    
    // Preverimo, če se uporabnik želi odjaviti
    if (isset($_GET['logout'])) {
        // Izpraznimo $_SESSION
        $_SESSION = array();
        
        // Pobrišemo piškotek seje
        setcookie(session_name(),'',time()-4200);
        
        // Izbrišemo sejo na strežniku
        session_destroy();
        
        // Preusmerimo uporabnika na prijavno stran
        header("Location: login.php");
        exit();
    } 
?>