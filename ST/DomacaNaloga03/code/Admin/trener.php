<?php
  require("Odjava.php");
  require("../connect.php");
  require("../funkcije_selekcije.php");

  if(isset($_POST['DodajTrenerja']) && !empty($_POST['DodajTrenerja'])){
    $ime = mysqli_real_escape_string($conn, $_POST['Ime']);
    $priimek = mysqli_real_escape_string($conn, $_POST['Priimek']);
    $gmail = mysqli_real_escape_string($conn, $_POST['gmailTrener']);
    $telefon = mysqli_real_escape_string($conn, $_POST['telefonTrener']);
    $selekcija = mysqli_real_escape_string($conn, $_POST['selekcija']);

    if(!strlen($telefon) === 9 && !strlen($telefon === 11)){
      $status = 'notOk';
      $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
    }
    else if(empty($selekcija) || strlen($ime) > 25 || strlen($priimek) > 30 || !strpos($gmail, '@') || strlen($gmail) > 40 || strlen($gmail) < 12) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
    else {
      $query = "INSERT INTO trener(Priimek, Ime,  Gmail, Telefonska_stevilka, SelekcijaID) VALUES(?, ?, ?, ?, ?)";
      $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
      mysqli_stmt_bind_param($stavek, "ssssi", $priimek, $ime, $gmail, $telefon, $selekcija) or $status = 'notOk';
      mysqli_stmt_execute($stavek) or $status = 'notOk';

      if(mysqli_affected_rows($conn) > 0) {
        $status = 'Ok';
        $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
      }
    }
    
  }
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>
      
      <div class="wrapper12">
        <?php
          /*INCLUDE NAVBAR FROM navbar.php*/
          require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class="Naslov"><span>Dodaj trenerja</span>
                        <a href="trenerizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                          /*FUNCTION FROM ../funkcije_selekcije.php*/
                          if(isset($status))
                              getResult($conn, $status, $message);
                        ?>
                    
                      <form action="trener.php" method="post">
                       <div class="row12">
                              <div class="col-25">
                                <label for="fname">Ime trenerja:* </label>
                              </div>
                              <div class="col-75">
                                <input type="text" name="Ime" maxlength="25" minlength="2" placeholder="Vnesite ime" required>
                              </div>
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label for="fname">Priimek trenerja:* </label>
                              </div>
                              <div class="col-75">
                                <input type="text" name="Priimek" maxlength="30" minlength="2" placeholder="Vnesite priimek" required>
                              </div>
                        </div>


                        <div class="row12">

                          <div class="col-25">
                            <label>Gmail</label>
                          </div>

                          <div class="col-75">
                            <input type="text" name="gmailTrener" maxlength="40" minlength="12" placeholder="janez.novak@gmail.com"/>
                          </div>

                        </div>

                        <div class="row12">

                          <div class="col-25">
                            <label>Telefonska številka</label>
                          </div>

                          <div class="col-75">
                            <input id="SmallNumber" type="text" name="telefonTrener" maxlength="11" minlength="9" placeholder="031 676 991 ali 031767991" required/>
                          </div>
                          
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label for = "date">ID Selekcije:* </label>
                              </div>
                              <div class="col-75">
                                <select id="SmallNumber" name="selekcija" >
                                <?php
                                  getSelekcija($conn);
                                ?>
                              </select>
                              </div>
                        </div>

                        <div class="row12">
                          <input type="submit" name = "DodajTrenerja" value="Dodaj v bazo">

                          <a href="trener.php" id="Refresh">Osveži stran</a>
                        </div>

                    </form>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>