<?php
	require("Odjava.php");
    require("../connect.php");
    require("../funkcije_selekcije.php");

    if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: createnewizpis.php");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "admin", "ID_Admin")) {
		header("Location: createnewizpis.php");
		exit();
	}

    if(isset($_POST['createaccount']) && !empty($_POST['createaccount'])) {
        $password = mysqli_real_escape_string($conn, $_POST['password']);
        $passwordS = mysqli_real_escape_string($conn, $_POST['passwordSec']);
        $datum = date("Y-m-d");

        if($password === $passwordS) {
            $status = 'Ok';
            if(is_numeric($passwordS) || is_numeric($password) || strlen($password) < 8 || strlen($password) > 20) {
                $status = 'notOk';
                $message = "Vnešeno geslo mora vsebovati 8-20 znakov ([0-9], [A-z]), vendar ne samo enega tipa (npr. samo številke).";
            } else {
                $newPassword = hash("sha256", $password . $datum);
                $query = "UPDATE admin SET password = ?, datum = ? WHERE ID_Admin = ?";
                $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
                mysqli_stmt_bind_param($stavek, "ssi", $newPassword, $datum, $id) or $status = 'notOk';
                mysqli_stmt_execute($stavek) or $status = 'notOk';
                
                if(mysqli_affected_rows($conn) > 0) {
                    $status = 'Ok';
                    $message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
                }
                else {
                    $status = 'notOk';
                    $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
                }
            }
        } else {
            $status = 'notOk';
            $message = "Geslo in ponovljeno geslo se ne ujemata. Vnesite ponovno.";
        }
    }
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
        <script src="tinymce/tinymce.min.js"></script>
    </head>
        <?php
            require_once('texteditorplugin.php');
        ?>
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>

      <div class="wrapper12">  
        <?php
            /*INCLUDE NAVBAR FROM navbar.php*/
            require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class = "Naslov"><span>Spremeni administratorja</span>
                      <a href = "createnewizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
					<?php
						/*FUNCTION FROM ../funkcije_selekcije.php*/
						if(isset($status))
							getResult($conn, $status, $message);
						$query = 'SELECT * FROM admin WHERE ID_Admin = ?';
						$stavek = mysqli_stmt_init($conn);
						mysqli_stmt_prepare($stavek, $query);
						mysqli_stmt_bind_param($stavek, "i", $id);
						mysqli_execute($stavek);
						@$rezultat = mysqli_stmt_get_result($stavek);
						$tab = mysqli_fetch_assoc($rezultat);
						
						echo '
                        <form action="createnewupdate.php?id='.$tab['ID_Admin'].'" method="post">
                            <div class="row12">
                                <div class="col-25">
                                    <label>Uporabniško ime:* </label>
                                </div>
                                <div class="col-75">
                                    <label><b>'.$tab["Username"].'</b></label>
                                </div>
                            </div>

                            <div class="row12">
                                <div class="col-25">
                                    <label>Geslo uporabnika (min. 8, max. 20):* </label>
                                </div>
                                <div class="col-75">
                                    <input id="SmallNumber" type="password" name="password" maxlength="20" minlength="8" placeholder="Vnesite geslo" required/>
                                </div>
                            </div>

                            <div class="row12">
                                <div class="col-25">
                                    <label>Ponovni geslo:* </label>
                                </div>
                                <div class="col-75">
                                    <input id="SmallNumber" type="password" name="passwordSec" maxlength="20" minlength="8" placeholder="Vnesite geslo" required/>
                                </div>
                            </div>

                            <div class="row12">
                                <input type="submit" name="createaccount" value="Spremeni geslo">
                                <a href="createnewupdate.php?id='.$tab['ID_Admin'].'" id="Refresh">Osveži stran</a>
                            </div>
                        </form>
						';	
					?>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>