<?php
	require('Odjava.php');
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
		$action = mysqli_real_escape_string($conn, $_GET['action']);
		$id = mysqli_real_escape_string($conn, $_GET['id']);
		
		if(!is_numeric($id)&& $id < 0) $status = "notOk";
		else if($action === 'odobreno'){
			$query = 'UPDATE komentarji SET Preverjeno = ? WHERE ID_Komentarja = ?';
			$y = 1;
	        $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          	mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          	mysqli_stmt_bind_param($stavek, "ii", $y, $id) or $status = 'notOk';
          	mysqli_execute($stavek) or $status = 'notOk';
          	sleep(1);
		}
		else if($action === 'zbrisal'){
			$query = 'DELETE FROM komentarji WHERE ID_Komentarja = ?';
			$stavek = mysqli_stmt_init($conn);
          	mysqli_stmt_prepare($stavek, $query);
          	mysqli_stmt_bind_param($stavek, "i", $id);
          	mysqli_execute($stavek) or $status = 'notOk';
          	sleep(1);
		}
		if(mysqli_affected_rows($conn) > 0) {
			$status = 'Ok';
			if($action !== 'zbrisal')
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			else
				$message = "Vnešeni zapis je bil izbrisan iz baze.";
		}
		else {
			$status = 'notOk';
			$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
		}
	}
?>

<html>
    <head> 
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>

    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
        
    	<div class="wrapper12">
	        <aside>
	            <div class = "Leva">
	                <div class = "SideBar">
	                    <a href = "adminpage.php"><i class="far fa-newspaper"></i> Novice</a>
			            <a href = "trener.php"><i class="far fa-user"></i> Trenerji</a>
			            <a href = "obvestila.php"><i class="fas fa-bell"></i> Obvestila</a>
	                    <?php
							$query = 'SELECT count(*) as "Stevilo" FROM komentarji WHERE Preverjeno = ?';
							$y = 0;
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $y);
							mysqli_execute($stavek);
							$rezultat = mysqli_stmt_get_result($stavek);

							$count = mysqli_fetch_assoc($rezultat);

							echo '<a href="komentarji.php"><i class="fas fa-comments"></i> Komentarji ('.$count['Stevilo'].')</a>';
	                    ?>
	                    <a href = "selekcijasezona.php"><i class="fas fa-clipboard-list"></i> Selekcija</a>
	                    <a href="funkcije.php"><i class="fas fa-clipboard-list"></i> Klubske funkcije</a>
	                    <a href="sponzorji.php"><i class="fas fa-clipboard-list"></i> Sponzorji</a>
	                	<a href="createnew.php"><i class="far fa-user"></i> Registracija</a>
					</div>
	            </div>
	        </aside>

	        <section>
		        <div class = "Desna">
	                <div class="container12">
	                    <div class = "Naslov"><span>Pregled komentarjev</span>
	                      <!--a href = "IgralecIzpis.php?selekcija=Clani" class = "Tabela">Izpis v tabeli</a-->
	                    </div>
	                   	<?php
                          /*FUNCTION FROM ../funkcije_selekcije.php*/
                          	if(isset($status))
                            	getResult($conn, $status, $message);
						  	else if($count['Stevilo'] === 0){
								echo'
									<div class="Warrning"><span>OPOZORILO! V tabeli komentarji ni nepreverjenih komentarjev</span></div>
								';
							}
                            else{
		                    	$counter = 1;
		                    	/*KLIČEMO FUNKCIJO IZ DATOTEKE: funkcije_selekcije.php*/
		                    	$rezultat = genKomentar($conn, 0);

		                        echo '<div>';
		                        echo '<table>
							          <tr id="Prva">
							            <td id="Counter">Številka</td>
							            <td>Avtor</td>
							            <td>Datum</td>
							            <td>Vsebina</td>
							            <td>Objava</td>
							            <td>Potrdi</td>
							            <td>Zbriši</td>
							          </tr>
							    ';
	                            while($tab = mysqli_fetch_assoc($rezultat)){
	                            	$datum = date_create($tab['Datum']);
	                            	echo '
		                                <tr>
		                                  <td>'.$counter.'</td>
		                                  <td>'.$tab['Avtor'].'</td>
		                                  <td>'.date_format($datum, 'd. m. Y').'</td>
		                                  <td>'.$tab['Vsebina'].'</td>
		                                  <td>'.$tab['Naslov'].'</td>
		                                  <td><a href="komentarji.php?action=odobreno&id='.$tab['ID_Komentarja'].'" id="ikoncecheck"><i class="fas fa-check"></i></a></td>
		                                  <td><a href="komentarji.php?action=zbrisal&id='.$tab['ID_Komentarja'].'" id="ikonce"><i class="far fa-trash-alt"></i></a></td>
		                                </tr>
		                            ';
		                            $counter++;
	                            }
	                            echo '</table></div>';
	                        }
	                    ?>

	                    <?php
	                    	$counter = 1;
	                    	echo '
	                    		<div class="Naslov" style="margin-top: 5%;"><span>Shranjeni komentarji</span>
			                    </div>';

			                /*KLIČEMO FUNKCIJO IZ DATOTEKE: funkcije_selekcije.php*/
			                $stavek = genKomentar($conn, 1);

			                echo '<div style="overflow-x:auto;">';
			                echo '<table>
					          <tr id="Prva">
					            <td id="Counter">Številka</td>
					            <td>Avtor</td>
					            <td>Datum</td>
					            <td>Vsebina</td>
					            <td>Objava</td>
					          </tr>
						    ';
	                    	

	                    	while($tab = mysqli_fetch_assoc($stavek)){
	                    		$datum = date_create($tab['Datum']);
	                    		echo '
	                                <tr>
	                                  <td>'.$counter.'</td>
	                                  <td>'.$tab['Avtor'].'</td>
	                                  <td>'.date_format($datum, 'd. m. Y').'</td>
	                                  <td>'.$tab['Vsebina'].'</td>
	                                  <td>'.$tab['Naslov'].'</td>
	                                </tr>
	                            ';
	                            $counter++;
	                    	}
	                    	echo '</table></div>';
	                    ?>

	                </div>
	            </div>
        	</section>
    	</div>
    </body>
    <?php
    require("Function.php");
  ?>
</html>