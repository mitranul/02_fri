<?php
	require("Odjava.php");
  require("../connect.php");
  require("../funkcije_selekcije.php");

    if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
      $status = 'Ok';
      $message = "Vnešeni zapis je bil izbrisan iz baze.";
      $action = mysqli_real_escape_string($conn, $_GET['action']);
      $id = mysqli_real_escape_string($conn, $_GET['id']);

      if($action == 'izbrisiSel' && is_numeric($id) && $id >= 0){
        $query = 'DELETE FROM selekcija WHERE SelekcijaID = ?';
        $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
        mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
        mysqli_execute($stavek) or $status = 'notOk';
        
        if($status === 'Ok') {
          $query = 'DELETE t.* FROM trener t WHERE t.SelekcijaID = ?';
          $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
          mysqli_execute($stavek) or $status = 'notOk';

          $query = 'DELETE o.* FROM vsebina o WHERE o.SelekcijaID = ?';
          $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
          mysqli_execute($stavek) or $status = 'notOk';
          sleep(1);
        }
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Pravilnost vnosnih polj ali vnešenih parametrov strani.";
      }
    }
?>

<html>
	<head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
          ?>
		    <section>
	            <div class = "Desna">
	                <div class = "container12">
                    <?php
                      /*FUNCTION FROM ../funkcije_selekcije.php*/
                      if(isset($status))
                          getResult($conn, $status, $message);
                      
                      echo '<div class = "Naslov"><span>Zapisi v tabeli selekcija</span></div>';
                      $query = 'SELECT * FROM selekcija ORDER BY SelekcijaID ASC';
                      $rezultat = mysqli_query($conn,$query);
                      $counter = 1;

                      echo '<div style="overflow-x:auto;">';
                      echo '<table>
                        <tr id = "Prva">
                          <td id ="Counter">Številka</td>
                          <td>Selekcija ID</td>
                          <td>Naziv</td>
                          <td>Gmail</td>
                          <td>Slika</td>
                          <td>Update</td>
                          <td>Izbris</td>
                        </tr>
                      ';

                      while($tab = mysqli_fetch_assoc($rezultat)){
                        echo '
                          <tr>
                            <td>'.$counter.'</td>
                            <td>'.$tab['SelekcijaID'].'</td>
                            <td>'.$tab['Naziv'].'</td>
                            <td>'.$tab['Gmail'].'</td>
                            <td>'.$tab['Slika'].'</td>
                            <td><a href = "selekcijaupdate.php?action=updateSel&id='.$tab['SelekcijaID'].'" id = "ikoncecheck"><i class="far fa-edit"></i></a></td>
                            <td><a href = "selekcijaizpis.php?site=selekcije&action=izbrisiSel&id='.$tab['SelekcijaID'].'" id = "ikonce"><i class="far fa-trash-alt"></i></a></td>
                          </tr>
                          ';
                          $counter++;
                        }
                        echo '</table>
                      </div>';
                      ?>
                    <div class="row12">
                      <a href="selekcijasezona.php" id="Refresh">Nazaj</a>
                    </div>
	                </div>
	            </div>
        	</section>
        </div>
	</body>
  <?php
    require("Function.php");
  ?>
</html>