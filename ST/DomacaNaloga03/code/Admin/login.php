<?php
    // require('BazaDodajanje.php');
    require('../connect.php');

    // Pričnemo sejo
    if (!isset($_SESSION))
        session_start();
    // Preverimo, če je uporabnik že prijavljen
    if (isset($_SESSION['prijava_uspesna']) && $_SESSION['prijava_uspesna']) {
        // Če je že prijavljen, ga preusmerimo na domačo stran
        header("Location: adminpage.php");
        exit();
    }

    if (isset($_POST['login'])) {
        $username = mysqli_real_escape_string($conn, $_POST['Username']);
        $password = mysqli_real_escape_string($conn, $_POST['Password']);

        $queryname = 'SELECT Password FROM admin WHERE Username = ?';
        $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $queryname);
        mysqli_stmt_bind_param($stavek, "s", $username);
        mysqli_execute($stavek);
        $rezultat = mysqli_stmt_get_result($stavek);

        $query = 'SELECT Datum FROM admin WHERE Username = ?';
        $result = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($result, $query);
        mysqli_stmt_bind_param($result, "s", $username);
        mysqli_execute($result);
        $salt = mysqli_stmt_get_result($result);
        $teb = mysqli_fetch_assoc($salt);
        
        if(mysqli_num_rows($rezultat) > 0){
            while($tab = mysqli_fetch_assoc($rezultat)){
                /*echo password_hash($password, PASSWORD_BCRYPT);
                echo '<br/>'.$tab['Password'];
                echo '<br/>'.hash("sha256", $password . $teb['Datum']);*/
                if(hash("sha256", $password . $teb['Datum']) == $tab['Password']){
                    $_SESSION['prijava_uspesna'] = true;
                    $_SESSION['Username'] = $username;
                    sleep(2);
                    header('Location: adminpage.php');
                    exit();
                }
                else{
                    echo '
                    <div class="alert alert-danger" role="alert">
                      <strong>NAPAKA!</strong> Vnešeno uporabniško ime ali geslo je napačano. Poskusite ponovno.
                    </div>
                    ';
                }
            }
        }   
        else
            echo '
            <div class="alert alert-danger" role="alert">
            <strong>NAPAKA!</strong> Vnešeno uporabniško ime ali geslo je napačano. Poskusite ponovno.
            </div>
            ';
    }
?>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="./login.css">
        <link rel="stylesheet" type="text/css" href="../fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
        <script src="../fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="../Slike/logo22.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <header></header>

        <div class="cont1">
            <form action="login.php" method="post">
                  <div class="imgcontainer">
                    <a href="login.php"><img src="../Slike/logo22.png" alt="Avatar" class="avatar"></a>
                  </div>

                  <div class="container12">
                    <i class="fas fa-user"></i>
                    <label><b>Username</b></label>
                    <input class="enter"  type="text" placeholder="Username" name="Username" required></input>
                      

                    <i class="fas fa-unlock-alt"></i>
                    <label><b>Password</b></label>
                    <input class="enter" id="pass" type="password" placeholder="Password" name="Password" required/>

                    <button class="enter" type="submit" name="login">Prijavi se</button>
                  </div>
            </form>
        </div>
    </body>
</html>
