<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

  	if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
  		$action = mysqli_real_escape_string($conn, $_GET['action']);
  		$id = mysqli_real_escape_string($conn, $_GET['id']);

		if($action === 'izbrisi' && is_numeric($id) && $id >= 0){
			$status = 'Ok';
			$message = "Vnešeni zapis je bil izbrisan iz baze.";
  			$query = 'DELETE FROM vsebina WHERE ID_Obvestila = ?';
	        $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          	mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          	mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
          	mysqli_execute($stavek) or $status = 'notOk';
		}
		else {
			$status = 'notOk';
			$message = "Prišlo je do napake pri dodajanju z bazo. Pravilnost vnosnih polj ali vnešenih parametrov strani.";
		}  
  	}
?>

<html>
	<head>
    	<?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
		    <section>
	            <div class="Desna">
	                <div class="container12">
	                  <div class="Naslov" style="margin-top: 0%;"><span>Zapisi v tabeli obvestila</span></div>
		                <?php
							/*FUNCTION FROM ../funkcije_selekcije.php*/
							if(isset($status))
								getResult($conn, $status, $message);
		                    $query = 'SELECT v.*, s.Naziv FROM vsebina v natural join selekcija s ORDER BY v.DatumObjave DESC';
		                    $rezultat = mysqli_query($conn,$query);
		                    $counter = 1;

		                    echo '<div style="overflow-x:auto;">';
		                    echo '<table>
		                      <tr id="Prva">
		                        <td id="Counter">Številka</td>
		                        <td>Datum objave</td>
		                        <td>Vsebina</td>
		                        <td>Selekcija</td>
	                        	<td>Update</td>
		                        <td>Izbris</td>
		                      </tr>
		                    ';

		                    while($tab = mysqli_fetch_assoc($rezultat)){
		                    	$datum = date_create($tab['DatumObjave']);
		                      echo '
		                        <tr>
		                          <td>'.$counter.'</td>
		                          <td>'.date_format($datum, 'd. m. Y').'</td>
		                          <td>'.$tab['Naslov'].'</td>
		                          <td>'.$tab['Naziv'].'</td>
		                          <td><a href="obvestilaupdate.php?id='.$tab['ID_Obvestila'].'" id="ikoncecheck"><i class="far fa-edit"></i></a></td>
		                          <td><a href="obvestilaizpis.php?action=izbrisi&id='.$tab['ID_Obvestila'].'" id="ikonce"><i class="far fa-trash-alt"></i></a></td>
		                        </tr>
		                        ';
		                        $counter++;
		                      }
		                      echo '</table>
		                    </div>';
		                ?>
	                    <div class="row12">
	                   		<a href="obvestila.php" id="Refresh">Nazaj</a>
	                   </div>
	                </div>
	                 <table class="table">
	            </div>
        	</section>
        </div>
	</body>
	<?php
	    require("Function.php");
	  ?>

</html>