<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_POST['dodajFunkcijo']) && !empty($_POST['dodajFunkcijo'])){
		$ime = mysqli_real_escape_string($conn, $_POST['ime']);
		$priimek = mysqli_real_escape_string($conn, $_POST['priimek']);
		$funkcija = mysqli_real_escape_string($conn, $_POST['funkcija']);
		$gmail = mysqli_real_escape_string($conn, $_POST['gmail']);
		$telefon = mysqli_real_escape_string($conn, $_POST['telefon']);

    if(strlen($ime) > 25 || strlen($priimek) > 30 || strlen($funkcija) > 40 || !strpos($gmail, '@') || strlen($gmail) > 45 || strlen($telefon) > 11 || strlen($telefon) < 9) {
      $status = 'notOk';
      $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
    }
    else {
      $query = 'INSERT INTO funkcionarji (Ime, Priimek, Funkcija, Telefon, Gmail) VALUES (?, ?, ?, ?, ?)';
		  $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
	    mysqli_stmt_bind_param($stavek, "sssss", $ime, $priimek, $funkcija, $telefon, $gmail) or $status = 'notOk';
	    mysqli_stmt_execute($stavek) or $status = 'notOk';
	    if(mysqli_affected_rows($conn) > 0) {
        $status = 'Ok';
        $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
      }
    }
	}
?>

<html>
	<head>
        <?php
        	/*REQUEST FROM head.php*/
        	require_once("head.php");
        ?>
    </head>
	
	<body>
		<header>
        	<?php
            	/*INCLUDE HEADER FROM header.php*/
        		require_once("header.php");
        	?>
        </header>

        <div class="wrapper12">
	        <?php
	          /*INCLUDE NAVBAR FROM navbar.php*/
	          require_once("navbar.php");
	        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    
                    <div class = "Naslov"><span>Dodaj funkcijo</span>
                        <a href = "funkcijeizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                          /*FUNCTION FROM ../funkcije_selekcije.php*/
                          if(isset($status))
                              getResult($conn, $status, $message);
                        ?>
                    
                      <form action="funkcije.php" method="post">
                       <div class="row12">
                              <div class="col-25">
                                <label>Ime funkcionarja:* </label>
                              </div>
                              <div class="col-75">
                                <input type="text" name="ime" maxlength="25" minlength="3" placeholder="Vnesite ime" required/>
                              </div>
                        </div>

                        <div class="row12">
                              <div class="col-25">
                                <label>Priimek funkcionarja:* </label>
                              </div>
                              <div class="col-75">
                                <input type="text" name="priimek" maxlength="30" minlength="3" placeholder="Vnesite priimek" required/>
                              </div>
                        </div>


                        <div class="row12">

                          <div class="col-25">
                            <label>Funkcija:*</label>
                          </div>

                          <div class="col-75">
                            <input type="text" name="funkcija" maxlength="40" minlength="5" placeholder="Vnesite funkcijo" required/>
                          </div>

                        </div>

                        <div class="row12">
                          <div class="col-25">
                            <label for = "date">Gmail naslov:* </label>
                          </div>
                          
                          <div class="col-75">
                            <input type="text" name="gmail" maxlength="45" minlength="12" placeholder="janez.novak@gmail.com"/>
                          </div>
                        </div>

                        <div class="row12">
                          <div class="col-25">
                            <label>Telefonska številka:*</label>
                          </div>

                          <div class="col-75">
                            <input id="SmallNumber" type="text" name="telefon" maxlength="11" minlength="9" placeholder="031 676 991 ali 031676991" required/>
                          </div>
                        </div>

                        <div class="row12">
                          <input type="submit" name="dodajFunkcijo" value="Dodaj v bazo">

                          <a href="funkcije.php" id="Refresh">Osveži stran</a>
                        </div>

                    </form>
                </div>
            </div>
        </section>
      </div>
    </body>

	<?php
		require("Function.php");
	?>
</html>