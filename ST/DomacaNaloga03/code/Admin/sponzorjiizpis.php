<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");
	
  	if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])){
  		$action = mysqli_real_escape_string($conn, $_GET['action']);
  		$id = mysqli_real_escape_string($conn, $_GET['id']);

  		if($action === 'izbris' && is_numeric($id) && $id >= 0){
			$status = 'Ok';
			$message = "Vnešeni zapis je bil izbrisan iz baze.";
  			$query = 'DELETE FROM sponzorji WHERE SponzorId = ?';
	        $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
          	mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
          	mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
          	mysqli_execute($stavek) or $status = 'notOk';
          	sleep(1);
  		}else {
			$status = 'notOk';
			$message = "Prišlo je do napake pri dodajanju z bazo. Pravilnost vnosnih polj ali vnešenih parametrov strani.";
		}
  	}
?>

<html>
	<head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
 
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
		    <section>
	            <div class = "Desna">
	                <div class = "container12">
	                	<div class = "Naslov" style = "margin-top: 0%;"><span>Zapisi v tabeli sponzorji</span>
	                	</div>
	                    <?php
                        	if(isset($status))
								getResult($conn, $status, $message);
							
							$query = 'SELECT SponzorId, Naziv, Slika FROM sponzorji ORDER BY SponzorId DESC';
							$rezultat = mysqli_query($conn,$query);
							$counter = 1;

							echo '<div style="overflow-x:auto;">';
							echo '<table>
							<tr id = "Prva">
								<td id ="Counter">Številka</td>
								<td>Naziv</td>
								<td>Slika</td>
								<td>Update</td>
								<td>Izbris</td>
							</tr>
							';

							while($tab = mysqli_fetch_assoc($rezultat)){
							echo '
								<tr>
								<td>'.$counter.'</td>
								<td>'.$tab['Naziv'].'</td>
								<td>'.$tab['Slika'].'</td>
								<td><a href = "sponzorjiupdate.php?id='.$tab['SponzorId'].'" id = "ikoncecheck"><i class="far fa-edit"></i></a></td>
								<td><a href = "sponzorjiizpis.php?action=izbris&id='.$tab['SponzorId'].'" id = "ikonce"><i class="far fa-trash-alt"></i></a></td>
								</tr>
								';
								$counter++;
							}
							echo '</table>';
							echo '</div>';
	                    ?>
	                	<div class = "row12">
	                   		<a href = "sponzorji.php" id = "Refresh">Nazaj</a>
	                   	</div>
	                </div>
	            </div>
        	</section>
        </div>
	</body>
	<?php
	    require("Function.php");
	?>
</html>