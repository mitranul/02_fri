<?php
	require("Odjava.php");
    require("../connect.php");
    require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: obvestilaizpis.php");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "vsebina", "ID_Obvestila")) {
		header("Location: obvestilaizpis.php");
		exit();
	}
	if(isset($_POST['ObvestiloUpdate']) && !empty($_POST['ObvestiloUpdate'])){
		$selekcija = mysqli_real_escape_string($conn, $_POST['selekcija']);
		$naslov = mysqli_real_escape_string($conn, $_POST['Naslov']);
		$vsebina = $_POST['Obvestilo'];
		$datum = mysqli_real_escape_string($conn, $_POST['Datumob']);

		if(strlen($naslov) > 80 || empty($_POST['Naslov']) || empty($_POST['Datumob']) || empty($_POST['selekcija'])) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
		else {
			$query = 'UPDATE vsebina SET SelekcijaID = ?, Naslov = ?, Vsebina = ?, DatumObjave = ? WHERE ID_Obvestila = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "isssi", $selekcija, $naslov, $vsebina, $datum, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';

			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}
	}
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
        <script src="tinymce/tinymce.min.js"></script>
    </head>
        <?php
            require_once('texteditorplugin.php');
        ?>
    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>

      <div class="wrapper12">  
        <?php
            /*INCLUDE NAVBAR FROM navbar.php*/
            require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class = "Naslov"><span>Dodaj obvestilo</span>
                      <a href = "obvestilaizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
					<?php
						/*FUNCTION FROM ../funkcije_selekcije.php*/
						if(isset($status))
							getResult($conn, $status, $message);

						$query = 'SELECT * FROM vsebina WHERE ID_Obvestila = ?';
						$stavek = mysqli_stmt_init($conn);
						mysqli_stmt_prepare($stavek, $query);
						mysqli_stmt_bind_param($stavek, "i", $id);
						mysqli_execute($stavek);
						@$rezultat = mysqli_stmt_get_result($stavek);
						$tab = mysqli_fetch_assoc($rezultat);
						
						echo '
								<form action="obvestilaupdate.php?id='.$tab['ID_Obvestila'].'" method="post">
								<div class="row12">
										<div class="col-25">
										<label for="subject">Naslov:*</label>
										</div>
										<div class="col-75">
										<input type="text" id="SmallNumber" name="Naslov" maxlength="80" minlength="5" value="'.$tab['Naslov'].'" required></input>
										</div>
								</div>

								<div class="row12">
										<div class="col-25">
										<label for="date">Datum obvestila:* </label>
										</div>
										<div class="col-75">
										<input type="date" id="SmallNumber" name="Datumob" value="'.$tab['DatumObjave'].'" required>
										</div>
								</div>

								<div class="row12">
										<div class="col-25">
										<label for="date">ID Selekcije:* </label>
										</div>
										<div class="col-75">
										<select id="SmallNumber" name="selekcija">';

											$query = "SELECT SelekcijaID, Naziv FROM selekcija ORDER BY SelekcijaID ASC";
											$stavek = mysqli_query($conn, $query);

											while($teb = mysqli_fetch_assoc($stavek)){
												$query = "SELECT SelekcijaID FROM vsebina where SelekcijaID = ".$tab['SelekcijaID'];
												$stavk = mysqli_query($conn, $query);
												$result = mysqli_fetch_assoc($stavk);

												if($result['SelekcijaID'] == $teb['SelekcijaID']){
												echo '<option value="'.$teb['SelekcijaID'].'" selected>'.$teb['Naziv'].'</option>';
												}
												else 
												echo '<option value="'.$teb['SelekcijaID'].'">'.$teb['Naziv'].'</option>';
											}
											echo '
										</select>
										</div>
								</div>

								<div class="row12">
										<div class="col-25">
										<label for="subject">Obvestilo:*</label>
										</div>
										<div class="col-75">
										<textarea id="subject" name="Obvestilo" style="height:200px">'.$tab['Vsebina'].'</textarea>
										</div>
								</div>

								<div class="row12">
									<input type="submit" name="ObvestiloUpdate" value="Posodobi podatke">

									<a href="obvestilaupdate.php?id='.$tab['ID_Obvestila'].'" id="Refresh">Osveži stran</a>
								</div>

							</form>
						';	
					?>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>