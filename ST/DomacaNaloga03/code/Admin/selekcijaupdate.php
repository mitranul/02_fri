<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: selekcijaizpis.php?site=selekcije");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "selekcija", "SelekcijaID")) {
		header("Location: selekcijaizpis.php?site=selekcije");
		exit();
	}

  	if(isset($_POST['SelekcijaUpdate']) && !empty($_POST['SelekcijaUpdate'])){
  		$folder = '../Slike/';
  		$naziv = mysqli_real_escape_string($conn, $_POST['Naziv']);
  		$email = mysqli_real_escape_string($conn, $_POST['selekcijagmail']);
  		$logo = mysqli_real_escape_string($conn, imageCall($folder, 'Logo'));

  		if($logo == null)
			  $logo = getUniversialById($conn, $id, 'Slika', 'selekcija', 'SelekcijaID');
		if(!strpos($email, '@') || strlen($email) > 45 || strlen($naziv) > 10) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
		else {
			$query = 'UPDATE selekcija SET Naziv = ?, Gmail = ?, Slika = ? WHERE SelekcijaID = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "sssi", $naziv, $email,$logo, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';

			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}
  	}
	
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>

    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>
        
      <div class="wrapper12">
        <?php
            /*INCLUDE NAVBAR FROM navbar.php*/
            require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
					<div class = "Naslov"><span>Dodaj selekcijo</span>
						<a href="selekcijaizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
					</div>
                      <?php
							/*FUNCTION FROM ../funkcije_selekcije.php*/
							if(isset($status) && isset($_POST['SelekcijaUpdate']))
								getResult($conn, $status, $message);
							
							$query = 'SELECT * FROM selekcija WHERE SelekcijaID = ?';
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $id);
							mysqli_execute($stavek);
							@$rezultat = mysqli_stmt_get_result($stavek);
							$tab = mysqli_fetch_assoc($rezultat);

							echo '
							<form action="selekcijaupdate.php?id='.$tab['SelekcijaID'].'" method="post" enctype="multipart/form-data">

							<div class="row12">
								<div class="col-25">
									<label for="fname">Naziv selekcije:* </label>
								</div>
								<div class="col-75">
									<input type="text" name="Naziv" maxlength="10" value="'.$tab['Naziv'].'" required/>
								</div>
							</div>

							<div class="row12">
								<div class="col-25">
									<label for = "date">Mail račun:* </label>
								</div>
								<div class="col-75">
									<input type="text" maxlength="45" minlength="12" name="selekcijagmail" value="'.$tab['Gmail'].'" placeholder="Vnesi gmail.." required>
								</div>
							</div>

							<br/><div class="row12">
								<div class="col-25">
									<label for="lname">Slika selekcije:* </label>
								</div>
								<div class="col-75" id = "Slika">
									<img class="card-img-top" style="width: 150px; height: 100px;" src="../Slike/'.$tab['Slika'].'" alt="Slike/obcina.png"><br/>
									<input type="file" name="Logo" style="margin-top: 1%;"/><br/><br/>
								</div>
							</div>
							
							<div class="row12">
								<input type="submit" name="SelekcijaUpdate" value="Posodobi podatke"/>

								<a href="selekcijaupdate.php?id='.$tab['SelekcijaID'].'" id="Refresh">Osveži stran</a>
							</div>
								
							</form>';
                      ?>
                </div>
            </div>
        </section>
        </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>