<?php
  require("Odjava.php");
  require("../connect.php");
  require("../funkcije_selekcije.php");

  if(isset($_POST['DodajSel']) && !empty($_POST['DodajSel'])){
    $folder = '../Slike/';
    $koledar = null;
    $email = mysqli_real_escape_string($conn, $_POST['selekcijagmail']);
    $logo = mysqli_real_escape_string($conn, imageCall($folder, 'Slika'));
    $naziv = mysqli_real_escape_string($conn, $_POST['Naziv']);

    if(strpos($email, '@') === false || $logo === null || strlen($email) > 45 || strlen($naziv) > 10 || strlen($naziv) < 4) {
      $status = 'notOk';
      $message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
    }
    else {
      $query = "INSERT INTO selekcija(Naziv, Gmail, Koledar, Slika) VALUES(?, ?, ?, ?)";
      $stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
      mysqli_stmt_bind_param($stavek, "ssss", $naziv, $email, $koledar, $logo) or $status = 'notOk';
      mysqli_stmt_execute($stavek) or $status = 'notOk';
      if(mysqli_affected_rows($conn) > 0) {
        $status = 'Ok';
        $message = "Vnešeni zapis je bil uspešno dodan v bazo.";
      }
      else {
        $status = 'notOk';
        $message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
      }
    }
  }
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>

    <body>
        <header>
          <?php
            /*INCLUDE HEADER FROM header.php*/
            require_once("header.php");
          ?>
        </header>
        
      <div class="wrapper12">
        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
        ?>
        <section>
            <div class="Desna">
                <div class="container12">
                    <div class="Naslov"><span>Dodaj selekcijo</span>
                      <a href="selekcijaizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                    </div>
                      <?php
                          /*FUNCTION FROM ../funkcije_selekcije.php*/
                          if(isset($status))
                              getResult($conn, $status, $message);
                        
                            echo '
                              <form action="selekcijasezona.php?stran=selekcija" method="post" enctype="multipart/form-data">
                                <div class="row12">
                                      <div class="col-25">
                                        <label for="fname">Naziv selekcije:* </label>
                                      </div>
                                      <div class="col-75">
                                        <input type = "text" name="Naziv" maxlength="10" minlength=4 placeholder="Naziv selekcije.." required/>
                                      </div>
                                </div>
                                <div class="row12">
                                      <div class="col-25">
                                        <label for="date">Mail račun:* </label>
                                      </div>
                                      <div class="col-75">
                                        <input type="text" maxlength="45" name="selekcijagmail" placeholder="selekcija.U19@gmail.com" required/>
                                      </div>
                                </div>

                                <div class = "row12">
                                    <div class="col-25">
                                        <label for="lname">Slika selekcije:* </label>
                                    </div>
                                    <div class = "col-75" id = "Slika">
                                        <input type="file" name="Slika" required style="margin-top: 1%;"/><br/><br/>
                                    </div>
                                </div>

                                <div class="row12" style = "margin-top: 0%;">
                                  <div class = "col-50">
                                    <input type="submit" name="DodajSel" value="Dodaj v bazo">
                                  </div>

                                  <div class = "col-50">
                                    <a href="selekcijasezona.php" id="Refresh">Osveži stran</a>
                                  </div>

                                </div>
                                  
                              </form>
                            ';
                      ?>
                </div>
            </div>
        </section>
        </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>