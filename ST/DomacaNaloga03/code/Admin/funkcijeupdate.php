<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: funkcijeizpis.php");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "funkcionarji", "FunkcionarId")) {
		header("Location: funkcijeizpis.php");
		exit();
	}

	if(isset($_POST['funkcijaUpdate']) && !empty($_POST['funkcijaUpdate'])){
		$ime = mysqli_real_escape_string($conn, $_POST['ime']);
		$priimek = mysqli_real_escape_string($conn, $_POST['priimek']);
		$gmail = mysqli_real_escape_string($conn, $_POST['gmail']);
		$telefon = mysqli_real_escape_string($conn, $_POST['telefon']);
		$funkcija = mysqli_real_escape_string($conn, $_POST['funkcija']);

		if(strlen($ime) > 25 || strlen($priimek) > 30 || strlen($funkcija) > 40 || strlen($gmail) > 45 || strlen($telefon) > 11 || strlen($telefon) < 9) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
		else {
			$query = 'UPDATE funkcionarji SET FunkcionarId = ?, Ime = ?, Priimek = ?, Funkcija = ?, Telefon = ?, Gmail = ? WHERE FunkcionarId = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "isssssi", $id, $ime, $priimek, $funkcija, $telefon, $gmail, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';
			
			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}
	}
?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
      <div class="wrapper12">
        <?php
          /*INCLUDE NAVBAR FROM navbar.php*/
          require_once("navbar.php");
        ?>
        <section>
            <div class = "Desna">
                <div class="container12">
                    <div class = "Naslov"><span>Spremeni funkcijo</span>
                        <a href = "funkcijeizpis.php" class = "Tabela" style = "text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                        	if(isset($status))
                            	getResult($conn, $status, $message);
                        ?>
                    
	                    <?php
							$query = 'SELECT * FROM funkcionarji WHERE FunkcionarId = ?';
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $id);
							mysqli_execute($stavek);
							$rezultat = mysqli_stmt_get_result($stavek);

							$tab = mysqli_fetch_assoc($rezultat);
	                    
	                    	echo '
	                    		<form action="funkcijeupdate.php?id='.$tab['FunkcionarId'].'" method="post">
									<div class="row12">
										<div class="col-25">
											<label>Ime funkcionarja:* </label>
										</div>
										<div class="col-75">
											<input type="text" name="ime" maxlength="25" minlength="3" value="'.$tab['Ime'].'" required/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label>Priimek funkcionarja:* </label>
										</div>
										<div class="col-75">
											<input type="text" name="priimek" maxlength="30" minlength="3" value="'.$tab['Priimek'].'" required/>
										</div>
									</div>


									<div class="row12">
										<div class="col-25">
											<label>Funkcija:*</label>
										</div>

										<div class="col-75">
											<input type="text" name="funkcija" maxlength="40" minlength="5" value="'.$tab['Funkcija'].'" required/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label for = "date">Gmail naslov:* </label>
										</div>
										<div class="col-75">
											<input type="text" name="gmail" maxlength="45" minlength="12" value="'.$tab['Gmail'].'"/>
										</div>
									</div>

									<div class="row12">
										<div class="col-25">
											<label>Telefonska številka:*</label>
										</div>
										<div class="col-75">
											<input id="SmallNumber" type="text" name="telefon" maxlength="11" minlength="9" value="'.$tab['Telefon'].'" required/>
										</div>
									</div>

									<div class="row12">
										<input type="submit" name="funkcijaUpdate" value="Dodaj v bazo">

										<a href="funkcijeupdate.php?id='.$tab['FunkcionarId'].'" id="Refresh">Osveži stran</a>
									</div>     
				                </form>
	                    	';
	                    ?>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>