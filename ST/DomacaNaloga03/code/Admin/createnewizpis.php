<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");


    if(isset($_GET['action']) && isset($_GET['id']) && !empty($_GET['id']) && !empty($_GET['action'])) {
        $action = mysqli_real_escape_string($conn, $_GET['action']);
  		$id = mysqli_real_escape_string($conn, $_GET['id']);

  		if($action === 'izbris' && is_numeric($id) && $id >= 0) {
            $query = 'SELECT COUNT(*) as steviloAdminov FROM admin';
            $stavek = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stavek, $query);
            mysqli_execute($stavek);
            $rezultat = mysqli_stmt_get_result($stavek);
            $tab = mysqli_fetch_assoc($rezultat);
            if($tab['steviloAdminov'] > 1) {
                $status = "Ok";
                $message = "Vnešeni zapis je bil izbrisan iz baze.";
                $query = 'DELETE a.* FROM admin a WHERE a.ID_Admin = ?';
                $stavek = mysqli_stmt_init($conn) or $status = 'notOk';
                mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
                mysqli_stmt_bind_param($stavek, "i", $id) or $status = 'notOk';
                mysqli_execute($stavek) or $status = 'notOk';
            }else {
                $status = "notOk";
                $message = "Brisanje je neuspešno. V sistemu mora biti vsaj eden administrator.";
            }
        } else {
            $status = 'notOk';
			$message = "Prišlo je do napake pri brisanju. Pravilnost vnosnih polj ali vnešenih parametrov strani.";
        }
    }
?>

<html>
	<head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
    	<div class="wrapper12">
	        <?php
              /*INCLUDE NAVBAR FROM navbar.php*/
              require_once("navbar.php");
            ?>
		    <section>
	            <div class="Desna">
	                <div class="container12">
	                  <div class="Naslov" style="margin-top: 0%;"><span>Zapisi v tabeli admin</span></div>
		                <?php
							/*FUNCTION FROM ../funkcije_selekcije.php*/
							if(isset($status))
								getResult($conn, $status, $message);
							$query = 'SELECT * FROM admin ORDER BY Datum ASC';
							$rezultat = mysqli_query($conn,$query);
							$counter = 1;

							echo '<div style="overflow-x:auto;">';
							echo '<table>
							<tr id="Prva">
								<td id="Counter">Številka</td>
								<td>Username</td>
								<td>Datum kreiranja</td>
								<td>Spremeni geslo</td>
								<td>Izbris</td>
							</tr>
							';

							while($tab = mysqli_fetch_assoc($rezultat)){
								$datum = date_create($tab['Datum']);
							echo '
								<tr>
                                <td>'.$counter.'</td>
                                <td>'.$tab['Username'].'</td>
								<td>'.date_format($datum, 'd. m. Y').'</td>';

								echo '
								<td><a href="createnewupdate.php?id='.$tab['ID_Admin'].'" id="ikoncecheck"><i class="far fa-edit"></i></a></td>
								<td><a href="createnewizpis.php?action=izbris&id='.$tab['ID_Admin'].'" id="ikonce"><i class="far fa-trash-alt"></i></a></td>
								</tr>
								';
								$counter++;
							}
							echo '</table>';
							echo '</div>';
	                    ?>  
	                    <div class="row12">
	                   		<a href="createnew.php" id="Refresh">Nazaj</a>
	                   </div>
	                </div>
	            </div>
        	</section>
        </div>
	</body>
	<?php
    require("Function.php");
  ?>
</html>