<?php
	require("Odjava.php");
	require("../connect.php");
	require("../funkcije_selekcije.php");

	if(isset($_GET['id']) && is_numeric($_GET['id']) && $_GET['id'] >= 0) {
		$id = mysqli_real_escape_string($conn, $_GET['id']);
	}else {
		header("Location: sponzorjiizpis.php");
		exit();
	}

	if(!checkUpdatedSite($conn, $id, "sponzorji", "SponzorId")) {
		header("Location: sponzorjiizpis.php");
		exit();
	}

	if(isset($_POST['sponzorUpdate']) && !empty($_POST['sponzorUpdate'])){
		$folder = '../Sponzorji/';
		$naziv = mysqli_real_escape_string($conn, $_POST['naziv']);
		$slika = mysqli_real_escape_string($conn, imageCall($folder, 'slika'));

		if($slika == null)
			$slika = getUniversialById($conn, $id, 'Slika', 'sponzorji', 'SponzorId');
		
		if(strlen($naziv) > 30 || strlen($naziv) < 3) {
			$status = 'notOk';
			$message = "Vnešeni podatki so napačni. Preverite, da vnosi ne presegajo danih omejitev.";
		}
		else {
			$query = 'UPDATE sponzorji SET Naziv = ?, Slika = ? WHERE SponzorId = ?';
			$stavek = mysqli_stmt_init($conn) or $status = 'notOk'; 
			mysqli_stmt_prepare($stavek, $query) or $status = 'notOk';
			mysqli_stmt_bind_param($stavek, "ssi", $naziv, $slika, $id) or $status = 'notOk';
			mysqli_execute($stavek) or $status = 'notOk';

			if(mysqli_affected_rows($conn) === 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis enak kot prejšnji.";
			}
			else if(mysqli_affected_rows($conn) > 0) {
				$status = 'Ok';
				$message = "Vnešeni zapis je bil uspešno posodobljen in shranjen v bazo.";
			}
			else {
				$status = 'notOk';
				$message = "Prišlo je do napake pri dodajanju z bazo. Preverite pravilnost vnosnih polj.";
			}
		}
	}

?>

<html>
    <head>
        <?php
          /*REQUEST FROM head.php*/
          require_once("head.php");
        ?>
    </head>
    
    <body>
        <header>
          <?php
          	/*INCLUDE HEADER FROM header.php*/
          	require_once("header.php");
          ?>
        </header>
      
      <div class="wrapper12">
        <?php
          /*INCLUDE NAVBAR FROM navbar.php*/
          require_once("navbar.php");
        ?>
        <section>
            <div class="Desna">
                <div class="container12">
                    <div class="Naslov"><span>Dodaj trenerja</span>
                        <a href="sponzorjiizpis.php" class="Tabela" style="text-decoration: none;">Izpis v tabeli</a>
                    </div>
                        <?php
                        	if(isset($status))
								getResult($conn, $status, $message);
							
							$query = 'SELECT * FROM sponzorji WHERE SponzorId = ?';
							$stavek = mysqli_stmt_init($conn);
							mysqli_stmt_prepare($stavek, $query);
							mysqli_stmt_bind_param($stavek, "i", $id);
							mysqli_execute($stavek);
							$rezultat = mysqli_stmt_get_result($stavek);
							$tab = mysqli_fetch_assoc($rezultat);
	                    
	                    	echo '
	                    		<form action="sponzorjiupdate.php?id='.$tab['SponzorId'].'" method="post" enctype="multipart/form-data">
			                       <div class="row12">
			                              <div class="col-25">
			                                <label for="fname">Naziv sponzorja:* </label>
			                              </div>
			                              <div class="col-75">
			                                <input type="text" name="naziv" maxlength="30" minlength="3" value="'.$tab['Naziv'].'" required/>
			                              </div>
			                        </div>

			                        <div class = "row12">
		                                  <div class="col-25">
		                                      <label for="lname">Naloži sliko:* </label>
		                                  </div>
		                                  <div class="col-75" id="Slika">
		                                      <img class="card-img-top" style="width: 150px; height: 100px;" src="../Sponzorji/'.$tab['Slika'].'" alt="Card image cap"><br/>
						                      <input type="file" name="slika" style="margin-top: 1%;"/><br/><br/>
		                                  </div>
		                            </div>

			                        <div class="row12">
			                          <input type="submit" name="sponzorUpdate" value="Posodobi podatke">

			                          <a href="sponzorjiupdate.php?id='.$tab['SponzorId'].'" id="Refresh">Osveži stran</a>
			                        </div>
			                    </form>
	                    	';
	                    ?>
                </div>
            </div>
        </section>
      </div>
    </body>
  <?php
    require("Function.php");
  ?>
</html>