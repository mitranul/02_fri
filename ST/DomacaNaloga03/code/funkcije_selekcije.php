<?php
    function querySelekcija($conn, $selekcija){
        $tab = getUniversialById($conn, $selekcija, 'Slika', 'selekcija', 'SelekcijaID');
        echo '<img class="card-img-top" src="./Slike/'.$tab.'" alt="Card image cap">';
    }

    function queryTrener($conn, $selekcija){
      $query = 'SELECT * FROM trener WHERE SelekcijaID = ?';
        $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $query);
        mysqli_stmt_bind_param($stavek, "i", $selekcija);
        mysqli_execute($stavek);
        @$rezultat = mysqli_stmt_get_result($stavek);

        $tab = mysqli_fetch_assoc($rezultat);
        echo '<label class="Inside" style="font-weight: bold;">Ime in priimek: </label>';
        echo '<span class="Inside"> '.$tab['Ime'].' '.$tab['Priimek'].'</span></span><br/>';

        echo '<label class="Inside" style="font-weight: bold;">Telefonska številka: </label>';
        echo '<span class="Inside"> '.$tab['Telefonska_stevilka'].'</span></span><br/>';

        echo '<label class="Inside" style="font-weight: bold;">Mail: </label>'; 
        echo '<span class="Inside"> '.$tab['Gmail'].'</span></span>';
    }

    function queryObvestila($conn, $selekcija){
      $query = 'SELECT Naslov, Vsebina, DatumObjave FROM vsebina WHERE SelekcijaID = ? ORDER BY DatumObjave DESC LIMIT 4';
        $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $query);
        mysqli_stmt_bind_param($stavek, "i", $selekcija);
        mysqli_execute($stavek);
        @$rezultat = mysqli_stmt_get_result($stavek);

        while($tab = mysqli_fetch_assoc($rezultat)){
          $datum = date_create($tab['DatumObjave']);
          echo '<small class = "text-muted"><i class="far fa-calendar-alt"></i></small> <span>'.date_format($datum, 'd. m. Y').'</span><br/>';
          echo '<span style = "font-size: 16px; font-weight: bold; color: #ff5c59;">'.$tab['Naslov'].'</span><br/>';
          echo '<span>'.$tab['Vsebina'].'</span><br/>';
        }
    }

    function genNovice($conn,$stran){
          $count = 1;
          $query = 'SELECT ObjavaID, Naslov, KratekOpis, Slika, Datum_objave FROM novica WHERE Vabilo = 0 ORDER BY Datum_objave DESC LIMIT ' .($stran * 6) .','. 6;
          $rezultat = mysqli_query($conn,$query);

          while($tab = mysqli_fetch_assoc($rezultat)){
            $datum = date_create($tab['Datum_objave']);
          
            echo '
                <div class = "col-sm-4 col-xs-4 style = "padding: 10px; margin-bottom: 1%; max-height: 200px;">
                  <div class="card" id = "InsideCards" style = "background-color: #e0e1e4;">

                    <a href = "novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'"><img class="card-img-top" src="./Slike/'.$tab['Slika'].'" alt="Card image cap" style = "max-height: 200px; min-height: 200px;"></a>

                    <div class="card-body" style = "background-color: #fff;">
                      <div style = "padding: 10px;"><i class="far fa-calendar-alt fa-1x"></i><span style = "font-size: 14px; font-weight: 200; color: grey;"> '.date_format($datum, 'd. m. Y').'<br><br></span><span class = "Naslov" style = "text-transform: uppercase; font-size: 21px; font-weight: bold;">'.$tab['Naslov'].'<br/><br/></span><div class = "Koncept" style = "margin-bottom: 7%;"><span style = "color: gray;">'.$tab['KratekOpis'].'</span></div>
                        <a href = "novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'" class="btn btn-primary" id = "NovButton">Preberite več!</a>
                      </div>
                    </div>
                  </div>
                </div>
            ';
          }
    }
    function genFacebookPlugin($urlAdress){
      echo '<script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/sl_SI/sdk.js#xfbml=1&version=v2.12";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, "script", "facebook-jssdk"));
      
      </script>';

      echo '
        <div class="fb-page" data-href="'.$urlAdress.'" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-width="1000px"><blockquote cite="'.$urlAdress.'" class="fb-xfbml-parse-ignore"><a href="'.$urlAdress.'">Facebook</a></blockquote></div>
      ';
    }

    function genSideBar($conn){
      echo '
        <div class = "card"  style = "margin-bottom: 5%; margin-top: 0%;">
            <div class = "card-header">
                <span style = "color: grey; font-size: 20px;">NK DOL NA FACEBOOKU</span>
            </div>';
            genFacebookPlugin('https://www.facebook.com/nogometniklubdol');
            echo '
        </div>

        <div class = "card-header">
            <span style = "color: grey; font-size: 20px;">VEČ NOVIC</span>
        </div>';

          $query = 'SELECT ObjavaID, Naslov, KratekOpis, Slika, Datum_objave FROM novica WHERE Vabilo = ? ORDER BY Datum_objave DESC LIMIT 3';
          $y = 0;
            $stavek = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stavek, $query);
            mysqli_stmt_bind_param($stavek, "i", $y);
            mysqli_execute($stavek);
            @$rezultat = mysqli_stmt_get_result($stavek);

          while($tab = mysqli_fetch_assoc($rezultat)){
            $datum = date_create($tab['Datum_objave']);
            echo '
              <div class = "card-deck">
                <div class = "card">
              <a href = "novica.php?id='.$tab['ObjavaID'].'&nameof='.$tab['Naslov'].'">
                  <div class = "card-body">
                    <h5 class = "card-title" style = "margin-bottom: 0;">'.$tab['Naslov'].'</h5>
                    <p class = "card-text" style = "margin-top: 4%;">'.$tab['KratekOpis'].'</p>
                  </div>
                  <div class = "card-footer">
                    <small class = "text-muted"><i class="far fa-calendar-alt"></i> Objavljeno: <span style = "color: #ffdc11; font-weight: bold;">'.date_format($datum, 'd. m. Y').'</span></small>
                  </div>
                </a>
                </div>
              </div>
            ';
          }
    }

    function getSelekcija($conn){
      $query = 'SELECT * FROM selekcija ORDER BY SelekcijaID ASC';
      $result = mysqli_query($conn, $query);

      while($tab = mysqli_fetch_assoc($result))
          echo '<option value = "'.$tab['SelekcijaID'].'">'.$tab['Naziv'].'</option>';
    }

    function getResult($conn, $status, $message){
      if(isset($status)){
          $stavek = mysqli_real_escape_string($conn, $status);
          if($stavek == 'Ok')
              echo '<div class = "Success"><span>'.$message.'</span></div>';

          else if($stavek == 'notOk')
              echo '<div class = "Warrning"><span>'.$message.'</span></div>';
      }
    }

    function checkUpdatedSite($conn, $id, $tableName, $tableAttribute) {
      $query = "SELECT $tableAttribute FROM $tableName WHERE $tableAttribute = ?";
      $stavek = mysqli_stmt_init($conn);
      mysqli_stmt_prepare($stavek, $query);
      mysqli_stmt_bind_param($stavek, "i", $id);
      mysqli_execute($stavek);
      @$rezultat = mysqli_stmt_get_result($stavek);
      $tab = mysqli_fetch_assoc($rezultat);
      if($tab["$tableAttribute"] === null) {
        return false;
      }
      return true;
    }


    function imageCall($folder, $path){
      $target_dir = $folder;
      $target_file = $target_dir . basename($_FILES[$path]["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      /* Allow certain file formats */
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        return null;
      }
      
      if (move_uploaded_file($_FILES[$path]["tmp_name"], $target_file)) {
          // echo "The file ". basename( $_FILES[$path]["name"]). " has been uploaded.";
          return $_FILES[$path]['name'];
      } else {
          return null;
      }
    }

/*ZA GENERIRANJE TABEL NA ADMIN PAGE-U V ZAVIHKU: komentarji*/
    function genKomentar($conn, $y){
      $query = 'SELECT k.*, n.Naslov FROM komentarji k inner join novica n on(k.ObjavaID = n.ObjavaID) WHERE k.Preverjeno = ? ORDER BY Datum DESC';
      $stavek = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stavek, $query);
        mysqli_stmt_bind_param($stavek, "i", $y);
        mysqli_execute($stavek);
        $rezultat = mysqli_stmt_get_result($stavek);

        return $rezultat;
    }

    function genSlider($conn){
      $query = 'SELECT * FROM novica WHERE Vabilo = 0 ORDER BY Datum_objave DESC LIMIT 3';
      $stavek = mysqli_query($conn, $query);
      return $stavek;
    }

/*IZPIS KATEREKOLI TABELE GLEDE NA ID*/
    function getUniversialById($conn, $id, $attribute, $table, $tableIdAttriute) {
      $query = "SELECT $attribute FROM $table WHERE $tableIdAttriute = ?";
      $stavek = mysqli_stmt_init($conn);
      mysqli_stmt_prepare($stavek, $query);
      mysqli_stmt_bind_param($stavek, "i", $id);
      mysqli_execute($stavek);
      $rezultat = mysqli_stmt_get_result($stavek);
      $tab = mysqli_fetch_assoc($rezultat);

      if(mysqli_affected_rows($conn) > 0)
        return $tab[$attribute];
      else
        return 'null';
    }
?>