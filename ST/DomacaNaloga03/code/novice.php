<?php
  require("funkcije_selekcije.php");
  require("connect.php");

  $page = 0;
  if(isset($_GET['page']) && !empty($_GET['page'])) {
    $page = mysqli_real_escape_string($conn, $_GET['page']);
    if(!is_numeric($page)) {
      header("Location: novice.php?page=0");
      exit();
    }
    else if($page >= 0)
      $page = mysqli_real_escape_string($conn, $page);
    else{
      header("Location: novice.php?page=0");
      exit();
    }
  }
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>NK Dol</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="MainStyle.css">
        <link rel="stylesheet" type="text/css" href="NovicaPodrobno.css">
        <link rel="stylesheet" type="text/css" href="inside-div.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="./Slike/logo22.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
        <script src="fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
    </head>
    
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="index.php"><img src = "Slike/logo22.png" alt="Logo" style="width:60px; height: 60px;"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php">DOMOV <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="novice.php?page=0">NOVICE</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      SELEKCIJE
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <?php
                        $query = 'SELECT Naziv FROM selekcija ORDER BY SelekcijaID ASC';
                        $rezultat = mysqli_query($conn,$query);
                        
                        while($tab = mysqli_fetch_assoc($rezultat)){
                          echo '<a class = "dropdown-item" href = "selekcije.php?id='.$tab['Naziv'].'">SELEKCIJA '.$tab['Naziv'].'</a>';
                        }
                      ?>
                    </div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="kontakt.php">KONTAKT</a>
                  </li>
                </ul>
              </div>
          </nav>
        
        <div class="container_novica" style="margin-bottom: 3%;">
          <div class="container_inside">
            <div class="row">
              <div class="col-md-12 col-xs-12">

                <div class="Brano" style="margin-bottom: 3%;">
                  <h1 class="Aktualno">NOVICE</h1>
                </div>
                  
                    
                    <div class="row" id="NoviceSite">
                      <?php
                          if(isset($napaka) && $napaka === true)
                            die("Številka strani ne obstaja. Prosim vrnite se na domačo stran.");
                          else {
                            genNovice($conn, $page);
                      ?>
                    </div>

                      <?php
                          $query = "SELECT ceil(count(*)/6) as strani from novica where Vabilo = ?";
                          $y = 0;
                          $stavek = mysqli_stmt_init($conn);
                          mysqli_stmt_prepare($stavek, $query);
                          mysqli_stmt_bind_param($stavek, "i", $y);
                          mysqli_execute($stavek);
                          @$rezultat = mysqli_stmt_get_result($stavek);
                          $tab = mysqli_fetch_assoc($rezultat);

                          echo '<nav aria-label="Page navigation example">
                                <ul class="pagination justify-content-center">';

                          for ($i=0; $i < $tab['strani']; $i++) {
                              if($i == $page)
                                echo '<li class="page-item"><a id="ChangePage" class="page-link activicu" href="novice.php?page='.$i.'">'.($i+1).'</a></li>';

                              else
                                echo '<li class="page-item"><a id="ChangePage" class="page-link" href="novice.php?page='.$i.'">'.($i+1).'</a></li>';
                          }
                          echo '</ul></nav>';
                        }
                      ?>
              </div>
            </div>
          </div>
        </div>
    </body>
    <?php
      /*INCLUDE FROM footer.php*/
      require_once("footer.php");
    ?>
</html>
