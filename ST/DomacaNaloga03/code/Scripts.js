function preklici(){
	document.getElementById("ime").value="";
	document.getElementById("komentar").value="";
}

function komentiraj(){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function(){
		if (this.readyState == 4 && this.status == 200) {
			if(this.responseText != '<span>OBVESTILO! Komentar bo objavljen po pregledu administratorja.</span>'){
				document.getElementById("muci").className = 'Warrning';	
			}
      		document.getElementById("muci").innerHTML = this.responseText;
      		$("#muci").fadeIn();
      		setTimeout(function(){
      			$("#muci").fadeOut();
      		}, 3000);
    	}
	};

	if(document.getElementById("ime").value != ""){
		var ime = document.getElementById("ime").value;
		var komentar = document.getElementById("komentar").value;
		var novicaid = getParameterByName("id");
		document.getElementById("ime").value="";
		document.getElementById("komentar").value="";
		xhttp.open("GET","ProcessKomentar.php?"+"fname="+ime+"&koment="+komentar+"&id="+novicaid,true);
		xhttp.send();
	}
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}