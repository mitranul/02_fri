<?php 
	 require("connect.php");
   require("funkcije_selekcije.php");

	if(!isset($_GET['id']) || empty($_GET['id']) || !is_numeric($_GET['id'])){
		header("Location: novice.php?page=0");
		exit();
	}

	$novica = mysqli_real_escape_string($conn, $_GET['id']);
  $nameof = mysqli_real_escape_string($conn, $_GET['nameof']);
  
	$sql = "SELECT * FROM novica WHERE ObjavaID = ?";
  $stavek = mysqli_stmt_init($conn);
  mysqli_stmt_prepare($stavek, $sql);
  mysqli_stmt_bind_param($stavek, "i", $novica);
  mysqli_execute($stavek);
  @$rezultat = mysqli_stmt_get_result($stavek);
  $tab = mysqli_fetch_assoc($rezultat);

  if($tab === null) {
    header("Location: novice.php?page=0");
		exit();
  }

	if(isset($_POST['Komentiraj']) && !empty($_POST['Komentiraj'])){
		$ime = mysqli_real_escape_string($conn, $_POST['Ime']);
		$komentar = mysqli_real_escape_string($conn, $_POST['Komentar']);
		$datum = mysqli_real_escape_string($conn, date("Y/m/d") );

		$query = "INSERT INTO komentarji(Avtor, Datum, Vsebina, ObjavaID) VALUES(?, ?, ?, ?)";
		$stavek = mysqli_prepare($conn, $query) or $status = 'notOk';
    mysqli_stmt_bind_param($stavek, "sssi", $ime, $datum, $komentar, $novica);
    mysqli_stmt_execute($stavek) or $status = 'notOk';
	}
  else if(isset($_POST['Preklici']) && !empty($_POST['Preklici'])){
    header("novica.php?id=$novica&nameof=$nameof");
  }

?>

<html>
	<head>
		  <?php
          echo '<title>'.$tab['Naslov'].'</title>';
        ?>
        <meta charset = "UTF-8">
        <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="MainStyle.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <link rel="stylesheet" type="text/css" href="NovicaPodrobno.css">
        <link rel="stylesheet" type="text/css" href="inside-div.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="./Slike/logo22.png">
        <script src="Jquery/jquery.min.js"></script>
        <script src="Scripts.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
        <script src="fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
	</head>

	<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="index.php"><img src = "Slike/logo22.png" alt="Logo" style="width:60px; height: 60px;"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php">DOMOV <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="novice.php?page=0">NOVICE</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      SELEKCIJE
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <?php
                        $query1='SELECT Naziv FROM selekcija ORDER BY SelekcijaID ASC';
                        $rezultat1=mysqli_query($conn, $query1);
                        
                        while($teb=mysqli_fetch_assoc($rezultat1)){
                          echo '<a class="dropdown-item" href="selekcije.php?id='.$teb['Naziv'].'">SELEKCIJA '.$teb['Naziv'].'</a>';
                        }
                      ?>
                    </div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="kontakt.php">KONTAKT</a>
                  </li>
                </ul>
              </div>
          </nav>

          <div class="container_novica">
          	<div class="container_inside">
          		<div class="row">

          			<div class="col-md-8 col-xs-12">

          				<div class="card">
      							<?php
                      $datum = date_create($tab['Datum_objave']);
      								echo '<img class="card-img-top" src="Slike/'.$tab['Slika'].'" alt="Card image cap">';
      								echo '<div class="inside">';
      									echo "<h1 style='font-size: 32px; font-weight: bold; color: #333'>" . $tab['Naslov'] ."</h1>";
      									echo '<i class="far fa-calendar-alt fa-1x"></i><span style="font-size: 14px;"> '.date_format($datum, 'd. m. Y').'</span><br/><br/>';
      									echo "<p>" . $tab['Vsebina'] . "</p>";
      								echo '</div>';
      						 	?>
          				</div>

          				<div class="card" style="margin-top: 5%;">
          					<div class="Brano">
			                  <h1 class="Aktualno">KOMENTARJI</h1>
			                </div>
			                <div class="cont1">
			                	<div class="Koledar" style="padding: 0px 10px 10px 10px;">
                            <?php
                                $query = 'SELECT count(*) as "Stevilo" FROM komentarji WHERE Preverjeno = ?';
                                $y = 1;
                                $stavek = mysqli_stmt_init($conn);
                                mysqli_stmt_prepare($stavek, $query);
                                mysqli_stmt_bind_param($stavek, "i", $y);
                                mysqli_execute($stavek);
                                @$rezultat = mysqli_stmt_get_result($stavek);

                                $count = mysqli_fetch_assoc($rezultat);
                                echo'
    		                          <div style="font-size: 18px;">
    		                            <form action="novica.php?id='.$tab['ObjavaID'].'" method = "post">
    		                              <input type="text" name="Ime" maxlength = "35" id="ime" placeholder="Ime" required><br/><br/>
    		                              <textarea name="Komentar" id="komentar" maxlength="200" placeholder="Komentiraj" style="height: 100px" required></textarea>
    		                            </form>
                                ';
                                echo '<button type="button" onclick="komentiraj()" id="komentiraj" style="float: left;">Komentiraj</button>';
                                echo '<button type="button" onclick="preklici()" id="preklici" style="float: right;">Prekliči</button>';

                                echo '<br/><br/><hr/>';
                                  
                                echo '<div class="Success" id="muci" style="display: none;"></div>';
  		                          echo '</div>';
                            ?>
		                        </div>

                        <div class = "Koledar">
                          <?php
                            if($count['Stevilo'] > 0){
                                $query1 = "SELECT * FROM komentarji WHERE ObjavaID = ? AND Preverjeno = ?";
                                $y = 1;
                                $stavek = mysqli_stmt_init($conn);
                                mysqli_stmt_prepare($stavek, $query1);
                                mysqli_stmt_bind_param($stavek, "ii", $novica, $y);
                                mysqli_execute($stavek);
                                @$rezultat = mysqli_stmt_get_result($stavek);

                                while($tab = mysqli_fetch_assoc($rezultat)){
                                  $datum = date_create($tab['Datum']);

                                  echo '
                                    <div class="card-deck" id="KomentarjiPrikaz">
                                      <div class="card">
                                          <div class="card-body">
                                              <img src="Slike/avatar.jpg" alt="Avatar" class="avatar" style="margin-right: 1%;">
                                                <h4 style="margin: 0;">'.$tab['Avtor'].'</h4>
                                                <p style="font-size: 11px;"><i class="far fa-calendar-alt fa-1x"></i> '.date_format($datum, 'd. m. Y').'</p>

                                              <div class="Koledar" style="padding: 3px 10px 3px 10px">
                                                <span>'.$tab['Vsebina'].'</span>
                                              </div>
                                          </div>
                                      </div>
                                    </div>';
                                }
                            }
                          ?>
                        </div>
			                </div>
          				</div>

          			</div>

          			<div class="col-md-4 col-xs-12">

          				<div class="card-header" style="margin-top: 0%;">
		                    <span style="color: grey; font-size: 20px;">VEČ NOVIC</span>
		              </div>
      	       				<?php
      	       					$query = "SELECT * FROM novica WHERE ObjavaID != ? AND Vabilo = ? ORDER BY Datum_objave DESC LIMIT 4";
                        $vabilo = 0;
                        $stavek = mysqli_stmt_init($conn);
                        mysqli_stmt_prepare($stavek, $query);
                        mysqli_stmt_bind_param($stavek, "ii", $novica, $vabilo);
                        mysqli_execute($stavek);
      	       					$rezultat = mysqli_stmt_get_result($stavek);

      	       					while($tab = mysqli_fetch_assoc($rezultat)){
                          $datum = date_create($tab['Datum_objave']);
      	       						echo '
                            <div class="card-deck">
                              <div class="card">
                                <a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.$tab['Naslov'].'">
                                  <div class="card-body">
                                    <h5 class="card-title" style="margin-bottom: 0;">'.$tab['Naslov'].'</h5>
                                    <p class="card-text" style="margin-top: 4%;">'.$tab['KratekOpis'].'</p>
                                  </div>
                                  
                                  <div class="card-footer">
                                    <small class="text-muted"><i class="far fa-calendar-alt"></i> Objavljeno: <span style="color: #ffdc11; font-weight: bold;">'.date_format($datum, 'd. m. Y').'</span></small>
                                  </div>
                                </a>
                              </div>
                            </div>
                          ';
                        }
      	       				?>
	          		</div>
          		</div>
          	</div>
          </div>
	</body>
	
  <?php
    /*INCLUDE FROM footer.php*/
    require_once("footer.php");
  ?>
</html>