<?php
  require("funkcije_selekcije.php");
  require("connect.php");
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>NK Dol</title>
        <meta charset = "UTF-8">
        <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="MainStyle.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <link rel="stylesheet" type="text/css" href="NovicaPodrobno.css">
        <link rel="stylesheet" type="text/css" href="inside-div.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="./Slike/logo22.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
        <script src="fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
    </head>

    <script>
      function callFacebookPlugin(){
        
      }
    </script>

  <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="index.php"><img src="Slike/logo22.png" alt="Logo" style="width:60px; height: 60px;"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item active">
                    <a class="nav-link" href="index.php">DOMOV <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="novice.php?page=0">NOVICE</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      SELEKCIJE
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <?php
                        $query = 'SELECT Naziv FROM selekcija ORDER BY SelekcijaID ASC';
                        $rezultat = mysqli_query($conn,$query);
                        
                        while($tab = mysqli_fetch_assoc($rezultat)){
                          echo '<a class = "dropdown-item" href = "selekcije.php?id='.$tab['Naziv'].'">SELEKCIJA '.$tab['Naziv'].'</a>';
                        }
                      ?>
                    </div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="kontakt.php">KONTAKT</a>
                  </li>
                </ul>
              </div>
          </nav>

          <!--img src="Slike/slide1.jpg" style="width: 100%; margin-bottom: 0%;"-->
          <div class="neki" style="max-width: 100%;">

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">

                  <?php
                    $novica = genSlider($conn);
                    $stevec = 0;

                    while($tab = mysqli_fetch_assoc($novica)){
                        if($stevec == 0){
                          echo '
                          <div class="carousel-item active">
                            <a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'"><img class="d-block w-100" id = "slidesimg" src = "./Slike/'.$tab['Slika'].'" alt="First slide"></a>
                            
                            <div class="carousel-caption d-xs-block" id = "naslovBlok">
                              <span id="naslov">'.$tab['Naslov'].'</span>
                              <p class="besedilo">'.$tab['KratekOpis'].'</p>
                            </div>
                          </div>
                          ';
                        }
                        else{
                          echo '
                             <div class="carousel-item">
                              <a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'"><img class="d-block w-100" id="slidesimg" src = "./Slike/'.$tab['Slika'].'" alt="Second slide"></a>

                              <div class="carousel-caption d-md-block" id="naslovBlok">
                                <span id="naslov">'.$tab['Naslov'].'</span>
                                <p class="besedilo">'.$tab['KratekOpis'].'</p>
                              </div>
                            </div>
                          ';
                        }
                        $stevec++;
                      }
                  ?>
                    
                 </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div>
          </div>

    <div class="container_novica" style = "margin-top: 3%;">
        <div class="container_inside">
            <div class="row">

                <div class="col-md-8 col-xs-12">
                    <div class="card"  style = "background-color: #e0e1e4; border: none;">
                        <div class="Brano">
                            <div class="Before1"></div><h1 class="Aktualno">AKTUALNO</h1>
                        </div>

                        <div class="card-deck" id="NoviceSite">
                          <?php
                            $query = 'SELECT ObjavaID, Naslov, KratekOpis, Slika, Datum_objave FROM novica WHERE Vabilo = ? ORDER BY Datum_objave DESC LIMIT 2 OFFSET 1';
                            // $rezultat = mysqli_query($conn,$query);
                            $y = 0;
                            $stavek = mysqli_stmt_init($conn);
                            mysqli_stmt_prepare($stavek, $query);
                            mysqli_stmt_bind_param($stavek, "i", $y);
                            mysqli_execute($stavek);
                            $rezultat = mysqli_stmt_get_result($stavek);

                            while($tab = mysqli_fetch_assoc($rezultat)){
                              $datum = date_create($tab['Datum_objave']);
                              echo '
                                <div class="card" id="InsideCards">
                                  <div class="card-header">
                                    <span style="color: grey; font-size: 24px;">'.$tab['Naslov'].'</span>
                                  </div>

                                  <div class="zoom"><a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'"><img class="card-img-top" src="Slike/'.$tab['Slika'].'" alt="Card image cap" style="max-height: 200px;"></a></div>

                                  <div class="card-body">
                                    <div style="margin: 0px; padding: 10px;"><i class="far fa-calendar-alt fa-1x"></i><span style="font-size: 14px; font-weight: 200; color: grey;"> '.date_format($datum, 'd. m. Y').'<br><br></span><span class = "Naslov">'.$tab['Naslov'].'<br></span><div class = "Koncept" style = "margin-bottom: 4%;"><span style = "color: gray;">'.$tab['KratekOpis'].'</span></div></div>
                                  <a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.$tab['Naslov'].'" class="btn btn-primary" id="NovButton">Preberite več!</a>
                                </div>
                                </div>
                              ';
                            }
                          ?>
                        </div>

                        <div class="card-deck" id="NoviceSite">
                          <?php
                            $query = 'SELECT ObjavaID, Naslov, KratekOpis, Slika, Datum_objave FROM novica WHERE Vabilo = ? ORDER BY Datum_objave DESC LIMIT 2 OFFSET 3';
                            $y = 0;
                            $stavek = mysqli_stmt_init($conn);
                            mysqli_stmt_prepare($stavek, $query);
                            mysqli_stmt_bind_param($stavek, "i", $y);
                            mysqli_execute($stavek);
                            $rezultat = mysqli_stmt_get_result($stavek);

                            while($tab = mysqli_fetch_assoc($rezultat)){
                              $datum = date_create($tab['Datum_objave']);
                              echo '
                                <div class="card" id="InsideCards">
                                  <div class="card-header">
                                    <span style="color: grey; font-size: 24px;">'.$tab['Naslov'].'</span>
                                  </div>

                                  <div class="zoom"><a href = "novica.php?id='.$tab['ObjavaID'].'&nameof='.urlencode($tab['Naslov']).'"><img class="card-img-top" src="Slike/'.$tab['Slika'].'" alt="Card image cap" style="max-height: 200px;"></a></div>

                                  <div class="card-body">
                                    <div style="margin: 0px; padding: 10px;"><i class="far fa-calendar-alt fa-1x"></i><span style="font-size: 14px; font-weight: 200; color: grey;"> '.date_format($datum, 'd. m. Y').'<br><br></span><span class = "Naslov">'.$tab['Naslov'].'<br></span><div class = "Koncept" style = "margin-bottom: 4%;"><span style = "color: gray;">'.$tab['KratekOpis'].'</span></div></div>
                                  <a href="novica.php?id='.$tab['ObjavaID'].'&nameof='.$tab['Naslov'].'" class="btn btn-primary" id="NovButton">Preberite več!</a>
                                </div>
                                </div>
                              ';
                            }
                          ?>
                        </div>
                        <div class="card-deck" id="NoviceSite" style="margin-top: 5%;">
                            <div class="card">
                                <div class="card-header">
                                    <span style="color: grey; font-size: 20px;">KODEKS IGRALCEV NK DOL</span>
                                </div>
                                <div class="cont1" id="NonePadding" style="padding: 0">
                                  <div class="Koldedar"  id="NonePadding">
                                    <img class="d-block w-100" src="./Slike/nkdolPravila.jpg" alt="Card image cap">
                                  </div>
                              </div>
                              
                            </div>
                        </div>

                        </div>
                    </div>

                    <div class = "col-md-4 col-xs-12">
                        <div class="card"  style = "margin-bottom: 5%;">
                            <div class = "card-header">
                              <span style = "color: grey; font-size: 20px;">NK DOL NA FACEBOOKU</span>
                            </div>

                            <div class = "Koledar">
                              <?php
                                genFacebookPlugin('https://www.facebook.com/nogometniklubdol');
                              ?>
                            </div>
                        </div>

                        <div class="card"  style="margin-bottom: 5%;">
                            <div class="card-header">
                              <span style="color: grey; font-size: 20px;">NOGOMETNI TABOR</span>
                            </div>

                            <div class="Koledar">
                              <?php
                                genFacebookPlugin('https://www.facebook.com/nogometnitaborvnaravi');
                              ?>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <span style="color: grey; font-size: 20px;">KLUBSKI DONATORJI</span>
                            </div>

                            <?php
                                $query = 'SELECT Slika FROM sponzorji';
                                $rezultat = mysqli_query($conn, $query);

                                while($tab = mysqli_fetch_assoc($rezultat)){
                                    echo '
                                        <div class="card-deck">
                                            <div class="card">
                                                <div class="card-body">
                                                    <img src="Sponzorji/'.$tab['Slika'].'" style="width: 50%; height: auto; margin-left: 24%;">
                                                </div>
                                            </div>
                                        </div>
                                    ';
                                }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
      </body>

    <?php
      /*INCLUDE FROM footer.php*/
      require_once("footer.php");
    ?>
</html>
