<?php
	require("connect.php");

    	$status = 'Ok';
		$ime = mysqli_real_escape_string($conn, $_GET['fname']);
		$komentar = mysqli_real_escape_string($conn, $_GET['koment']);
		$novicaid = mysqli_real_escape_string($conn, $_GET['id']);
		$datum = mysqli_real_escape_string($conn, date("Y/m/d") );

		$query = "INSERT INTO komentarji(Avtor, Datum, Vsebina, ObjavaID) VALUES(?, ?, ?, ?)";
		$stavek = mysqli_prepare($conn, $query);
    	mysqli_stmt_bind_param($stavek, "sssi", $ime, $datum, $komentar, $novicaid);

    	mysqli_stmt_execute($stavek) or $status = 'notOk';

    if($status == 'Ok')
    	echo '<span>OBVESTILO! Komentar bo objavljen po pregledu administratorja.</span>';
    
    else
    	echo '<span>OPOZORILO! Prišljo je do napake.</span>';
?>