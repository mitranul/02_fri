<?php
  require("connect.php");
  require_once("funkcije_selekcije.php");

  if(isset($_GET['id']) && !empty($_GET['id'])) {
    $naziv = mysqli_real_escape_string($conn, $_GET['id']);
  } else {
    $naslov = "Napaka";
  }

?>

<!DOCTYPE HTML>
<html>
    <head>
        <title>NK Dol</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="MainStyle.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <link rel="stylesheet" type="text/css" href="NovicaPodrobno.css">
        <link rel="stylesheet" type="text/css" href="inside-div.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="./Slike/logo22.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="fontAwesome/fontawesome-free-5.0.9/web-fonts-with-css/css/fontawesome-all.css">
        <script src="fontAwesome/fontawesome-free-5.0.9/svg-with-js/js/fontawesome-all.js"></script>
    </head>
    
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="index.php"><img src="Slike/logo22.png" alt="Logo" style="width:60px; height: 60px;"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php">DOMOV <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="novice.php?page=0">NOVICE</a>
                  </li>
                  <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      SELEKCIJE
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <?php
                        $query = 'SELECT Naziv FROM selekcija ORDER BY SelekcijaID ASC';
                        $rezultat = mysqli_query($conn,$query);
                        
                        while($tab = mysqli_fetch_assoc($rezultat)){
                          echo '<a class="dropdown-item" href = "selekcije.php?id='.$tab['Naziv'].'">SELEKCIJA '.$tab['Naziv'].'</a>';
                        }
                      ?>
                    </div>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="kontakt.php">KONTAKT</a>
                  </li>
                </ul>
              </div>
        </nav>
        
        <div class="container_novica">
            <div class="container_inside" style="margint-top: 50px;">
                <div class="row">

                    <div class="col-md-8 col-xs-12">
                        <div class="card">
                            <?php
                              $query = 'SELECT * FROM selekcija WHERE Naziv = ?';
                              $stavek = mysqli_stmt_init($conn);
                              mysqli_stmt_prepare($stavek, $query);
                              mysqli_stmt_bind_param($stavek, "s", $naziv);
                              mysqli_execute($stavek);
                              $rezultat = mysqli_stmt_get_result($stavek);
                              $selekcija = mysqli_fetch_assoc($rezultat);
                              if(sizeof($selekcija) <= 0) $naslov = "Napaka";
                              else $naslov = "Selekcija ".$selekcija['Naziv'];
                              
                              echo '
                                  <div class = "Brano">
                                      <div class = "Before1"></div><h1 class = "Aktualno">'.$naslov.'</h1>
                                  </div>
                              ';
                                    
                            ?>
                            
                            <div class = cont1>
                                <?php
                                  if($naslov === "Napaka") {
                                    die("Izbrana selekcija ne obstaja. Vrnite se na domačo stran in poskusite ponovno");
                                  }
                                  else {
                                    querySelekcija($conn, $selekcija['SelekcijaID']);
                                    echo '
                                      <div class = "Brano" style = "margin-top: 5%;">
                                        <h1 class = "Aktualno">TRENER</h1>
                                      </div>

                                      <div class="Koledar">';
                                        queryTrener($conn, $selekcija['SelekcijaID']);
                                    echo '
                                      </div>
                                    ';

                                    echo '
                                      <div class = "Brano" style = "margin-top: 3%;">
                                          <h1 class = "Aktualno">OBVESTILO</h1>
                                      </div>

                                      <div class = "Koledar">';
                                          queryObvestila($conn, $selekcija['SelekcijaID']);
                                      echo '
                                      </div>
                                      ';
                                  }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class = "col-md-4 col-xs-12">
                        <?php
                          genSideBar($conn);
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </body>
    
    <?php
      /*INCLUDE FROM footer.php*/
      require_once("footer.php");
    ?>

</html>