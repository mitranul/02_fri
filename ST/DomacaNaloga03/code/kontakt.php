<?php
  require("connect.php");
?>


<!DOCTYPE HTML>
<html>
    <head>
        <title>NK Dol</title>
        
        <meta charset = "UTF-8">
        <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="MainStyle.css">
        <link rel="stylesheet" type="text/css" href="home.css">
        <link rel="stylesheet" type="text/css" href="NovicaPodrobno.css">
        <link rel="stylesheet" type="text/css" href="inside-div.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="shortcut icon" href="./Slike/logo22.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
              <a class="navbar-brand" href="index.php"><img src = "Slike/logo22.png" alt="Logo" style="width:60px; height: 60px;"></a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="index.php">DOMOV <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="novice.php?page=0">NOVICE</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      SELEKCIJE
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <?php
                        $query = 'SELECT Naziv FROM selekcija ORDER BY SelekcijaID ASC';
                        $rezultat = mysqli_query($conn,$query);
                        
                        while($tab = mysqli_fetch_assoc($rezultat)){
                          echo '<a class="dropdown-item" href="selekcije.php?id='.$tab['Naziv'].'">SELEKCIJA '.$tab['Naziv'].'</a>';
                        }
                      ?>
                    </div>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="kontakt.php">KONTAKT</a>
                  </li>
                </ul>
              </div>
          </nav>

          <div class="container_novica" style="margin-bottom: 4%;">
            <div class="container_inside">
              <div class="row">
                <div class="col-md-12 col-xs-12">

                  <div class="card">

                    <div class="Brano">
                      <h1 class="Aktualno">VIZITKA</h1>
                    </div>

                    <div class="cont1">
                      <div class="Koledar">
                          <span>Nogometni klub Dol</span><br><span>Zaboršt pri Dolu 90</span><br><span>1262 Dol pri Ljubljani</span><br><br>
                          <span>E-pošta:<a class="Mail" href="mailto:info@nk-dol.si"> <span style = "font-weight: bold; color: #ffdc11;">info@nk-dol.si</span></span></a><br><br>
                          <span>Matična številka: <span style="font-weight: 100">5946336</span></span><br>
                          <span>Davčna številka: <span style="font-weight: 100">31591191 (nismo zavezanci za DDV)</span></span><br><br>
                          <span>Transakcijski račun: <span style="font-weight: 100">SI56 0310 6200 0051 633</span></span><br><br>
                          
                          <!--PREDSTAVNIKI KLUBA-->
                          <?php
                            $query = 'SELECT * FROM funkcionarji ORDER BY FunkcionarId';
                            $rezultat = mysqli_query($conn,$query);

                            while($tab = mysqli_fetch_assoc($rezultat)){
                              echo '<span><b>'.$tab['Funkcija'].':</b></span><br/>';
                              echo '<span style="font-weight: 100">'.$tab['Ime'].' '.$tab['Priimek'].', '.$tab['Telefon'].'</span><br/>';

                              if($tab['Gmail'] != null)
                                echo '<span style="font-weight: 100">'.$tab['Gmail'].'</span><br/><br/>';
                              else
                                echo '<br/>';

                            }
                          ?>

                        <div class="Brano" style="margin-top: 5%;">
                          <h1 class="Aktualno">OBIŠČITE NAS</h1>
                        </div>

                        <div class="Koledar">
                          <article class="atext" style="margin-bottom: 20px; font-size: 16px; color: grey;">V letnem času oziroma tekmovalni sezoni, ki poteka od meseca aprila do
                          novembra nogometaši vadijo in igrajo prvenstvene tekme na nogometnem
                          igrišču ŠRC Korant. Treningi potekajo v terminih, ki se določijo z letnim
                          planom dela in so objavljeni na spletni strani pod informacijami. V
                          zimskem času je organizirana vadba v telovadnici Osnovne šole Janka
                          Modra Dol pri Ljubljani, kar je posledica našega odličnega sodelovanja.
                          </article><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2766.7501702457926!2d14.63920677889911!3d46.095957037777154!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47653122be4eb58d%3A0x3fe8858fa1d367ec!2sZabor%C5%A1t+pri+Dolu+90%2C+1262+Dol+pri+Ljubljani!5e0!3m2!1ssl!2ssi!4v1514549200759" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </body>
    
    <?php
      /*INCLUDE FROM footer.php*/
      require_once("footer.php");
    ?>
</html>