-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 10. maj 2020 ob 15.58
-- Različica strežnika: 10.1.26-MariaDB
-- Različica PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `nk_dol`
--

-- --------------------------------------------------------

--
-- Struktura tabele `admin`
--

CREATE TABLE `admin` (
  `ID_Admin` int(11) NOT NULL,
  `Username` char(25) NOT NULL,
  `Password` text NOT NULL,
  `Datum` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `admin`
--

INSERT INTO `admin` (`ID_Admin`, `Username`, `Password`, `Datum`) VALUES
(3, 'Admin', 'fbf1002da9688011058ddf7bb08bf7e77207148020c653a7f91f7593b791a125', '2020-05-10'),
(7, 'User1', 'ca1bdea82714fa860cdc5b9430af0454faa1d3e2ca5391f9f4d2879821133103', '2020-05-10');

-- --------------------------------------------------------

--
-- Struktura tabele `funkcionarji`
--

CREATE TABLE `funkcionarji` (
  `FunkcionarId` int(11) NOT NULL,
  `Ime` char(25) CHARACTER SET utf8 COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Priimek` char(30) CHARACTER SET utf8 COLLATE utf8_slovenian_ci DEFAULT NULL,
  `Funkcija` char(40) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Telefon` char(11) DEFAULT NULL,
  `Gmail` char(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `funkcionarji`
--

INSERT INTO `funkcionarji` (`FunkcionarId`, `Ime`, `Priimek`, `Funkcija`, `Telefon`, `Gmail`) VALUES
(3, 'Marjan', 'Ferk', 'Predsednik kluba', '041 647 362', ''),
(4, 'Mladen', 'Košmerl', 'Tajnik kluba', '040 578 078', ''),
(6, 'Sebastjan', 'Lunar', 'Tehnični koordinator', '031 685 889', 'lunar.seba@gmail.com');

-- --------------------------------------------------------

--
-- Struktura tabele `komentarji`
--

CREATE TABLE `komentarji` (
  `ID_Komentarja` int(11) NOT NULL,
  `Avtor` char(35) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Datum` date NOT NULL,
  `Vsebina` char(200) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `ObjavaID` int(11) NOT NULL,
  `Preverjeno` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `komentarji`
--

INSERT INTO `komentarji` (`ID_Komentarja`, `Avtor`, `Datum`, `Vsebina`, `ObjavaID`, `Preverjeno`) VALUES
(169, 'Ivanka', '2020-05-10', 'Zelo dobra spletna stran!', 1, 1),
(170, 'Janez', '2020-05-10', 'Bravo člani!', 73, 0);

-- --------------------------------------------------------

--
-- Struktura tabele `novica`
--

CREATE TABLE `novica` (
  `ObjavaID` int(11) NOT NULL,
  `Datum_objave` date NOT NULL,
  `Slika` char(200) DEFAULT NULL,
  `Naslov` char(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Vsebina` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `KratekOpis` char(255) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Vabilo` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `novica`
--

INSERT INTO `novica` (`ObjavaID`, `Datum_objave`, `Slika`, `Naslov`, `Vsebina`, `KratekOpis`, `Vabilo`) VALUES
(1, '2019-10-01', 'turnir_v_lj_U8.jpg', 'Dobrodošli na spletni strani NK Dol', '<p>Pozdravljeni na prenovljeni strani nogometnega kluba Dol. Na tej strani boste lahko spremljali vsa dogajanja v zvezi s klubom, ter pridobili vse pomembne informacije glede posameznih selekcij, dogodkov.</p>\r\n<p>&nbsp;</p>\r\n<p>Lep pozdrav, NK Dol</p>', 'Nogometni klub Dol ima po dolgih letih čakanja prenovljeno spletno stran.', 0),
(72, '2020-03-13', 'event1.jpg', 'Treningi vseh selekcij do nadaljnega odpovedani', '<p>Nogometni klub Dol, je v petek, 13. 3. 2020, sprejel sklep o&nbsp;<em><strong>prekinitvi vseh dejavnosti do nadaljnega.</strong></em><em>&nbsp;</em>Preklic je posledica pandemije koronavirusa in zaustritev vladnih ukrep za preprečevanje le-te. Prosimo za razumevanje, hkrati pa upamo, da se bomo kmalu vrnili na zelenice.</p>\r\n<p>Pozdrav, NK Dol</p>', 'Prekinitev vseh klubsih dejavnosti', 0),
(73, '2019-11-04', 'clani.jpg', 'Člani zaključili prvenstvo na 4. mestu', '<p>Velik uspeh na&scaron;e članske selekcije, ki je zimski del, 5. MNZ lige, končala na zelo dobrem 4. mestu. Vsem igralce iskreno čestitamo in upamo, da bo spomladanski del &scaron;e bolj&scaron;i.</p>\r\n<p>Pozdrav NK Dol</p>', 'Člani zaključili jesen na zavidljivem 4. mestu', 0);

-- --------------------------------------------------------

--
-- Struktura tabele `selekcija`
--

CREATE TABLE `selekcija` (
  `SelekcijaID` int(11) NOT NULL,
  `Naziv` char(15) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Gmail` char(45) DEFAULT NULL,
  `Koledar` text,
  `Slika` char(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `selekcija`
--

INSERT INTO `selekcija` (`SelekcijaID`, `Naziv`, `Gmail`, `Koledar`, `Slika`) VALUES
(1, 'U-13', 'selekcija.U13@gmail.com', NULL, 'slide3.jpg'),
(418, 'U-15', 'selekcija.U15@gmail.com', NULL, 'U-9.jpg'),
(419, 'Člani', 'selekcija.clani@gmail.com', NULL, 'clani.jpg');

-- --------------------------------------------------------

--
-- Struktura tabele `sponzorji`
--

CREATE TABLE `sponzorji` (
  `SponzorId` int(11) NOT NULL,
  `Naziv` char(30) NOT NULL,
  `Slika` char(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Odloži podatke za tabelo `sponzorji`
--

INSERT INTO `sponzorji` (`SponzorId`, `Naziv`, `Slika`) VALUES
(1, 'Velika planina', 'logo-velika-planina.jpg'),
(2, 'Jub', 'jub-logo.png'),
(3, 'Vrtaric transport', 'vrtaric.jpg');

-- --------------------------------------------------------

--
-- Struktura tabele `trener`
--

CREATE TABLE `trener` (
  `TrenerID` int(11) NOT NULL,
  `Priimek` char(30) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Ime` char(25) CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `Gmail` char(40) DEFAULT NULL,
  `Telefonska_stevilka` char(15) DEFAULT NULL,
  `SelekcijaID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `trener`
--

INSERT INTO `trener` (`TrenerID`, `Priimek`, `Ime`, `Gmail`, `Telefonska_stevilka`, `SelekcijaID`) VALUES
(22, 'Guček', 'Boštjan', 'bostjan.gucek@gmail.com', '051 231 112', 419),
(23, 'Colarič', 'Emil', 'emil.colaric@gmail.com', '064 212 121', 418),
(1, 'Ferk', 'Tadej', 'tadej.ferk@gmail.com', '031 723 822', 1);

-- --------------------------------------------------------

--
-- Struktura tabele `vsebina`
--

CREATE TABLE `vsebina` (
  `SelekcijaID` int(11) NOT NULL,
  `ID_Obvestila` int(11) NOT NULL,
  `Naslov` varchar(80) NOT NULL,
  `Vsebina` text CHARACTER SET utf8 COLLATE utf8_slovenian_ci NOT NULL,
  `DatumObjave` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `vsebina`
--

INSERT INTO `vsebina` (`SelekcijaID`, `ID_Obvestila`, `Naslov`, `Vsebina`, `DatumObjave`) VALUES
(419, 35, 'Odpoved tekmovanja', '<p>MNZ Ljubljana je sprejel sklep, da se nadaljevanje članske 5. lige&nbsp;<em><strong>do nadaljnega ODPOVE</strong></em><em>.&nbsp;</em>To pomeni, da so na&scaron;i člani prvenstvo zaključi na zelo dobrem 4. mestu, naprej pa napreduje ekipa Vrhnike.</p>\r\n<p>NK Dol</p>', '2020-05-01'),
(418, 36, 'Potek treningov', '<p>Zaradi pandemije koronavirusa, je spomladanski začetek prvenstva, do preklica,&nbsp;<em><strong>ODPOVEDAN</strong></em>. O začetku treningov, vas bomo pravočasno obvestili.</p>\r\n<p>Lep pozdrav,</p>\r\n<p>NK Dol</p>', '2020-03-13'),
(1, 33, 'Potek treningov', '<p>Zaradi pandemije koronavirusa, je spomladanski začetek prvenstva, do preklica,&nbsp;<em><strong>ODPOVEDAN</strong></em>. O začetku treningov, vas bomo pravočasno obvestili.</p>\r\n<p>Lep pozdrav,</p>\r\n<p>NK Dol</p>', '2020-03-13'),
(419, 34, 'Potek prvenstva', '<p>Člani so si v zimskem delu prvenstva priigrali veliko prednost in so na zavidljivem 4. mestu lestvice. Vendar pa je nadaljevanje sezone, zaradi pandemije koronavirusa, pod vpra&scaron;ajem. Za dodatne informacije vas bomo obve&scaron;čali sproti.</p>\r\n<p>NK Dol</p>', '2020-03-13');

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ID_Admin`),
  ADD UNIQUE KEY `ID_Admin` (`ID_Admin`);

--
-- Indeksi tabele `funkcionarji`
--
ALTER TABLE `funkcionarji`
  ADD PRIMARY KEY (`FunkcionarId`);

--
-- Indeksi tabele `komentarji`
--
ALTER TABLE `komentarji`
  ADD PRIMARY KEY (`ID_Komentarja`,`ObjavaID`),
  ADD UNIQUE KEY `ID_Komentarja` (`ID_Komentarja`),
  ADD KEY `ObjavaID` (`ObjavaID`);

--
-- Indeksi tabele `novica`
--
ALTER TABLE `novica`
  ADD PRIMARY KEY (`ObjavaID`),
  ADD UNIQUE KEY `ObjavaID` (`ObjavaID`);

--
-- Indeksi tabele `selekcija`
--
ALTER TABLE `selekcija`
  ADD PRIMARY KEY (`SelekcijaID`),
  ADD UNIQUE KEY `SelekcijaID` (`SelekcijaID`);

--
-- Indeksi tabele `sponzorji`
--
ALTER TABLE `sponzorji`
  ADD PRIMARY KEY (`SponzorId`);

--
-- Indeksi tabele `trener`
--
ALTER TABLE `trener`
  ADD PRIMARY KEY (`TrenerID`,`SelekcijaID`),
  ADD UNIQUE KEY `TrenerID` (`TrenerID`),
  ADD KEY `SelekcijaID` (`SelekcijaID`);

--
-- Indeksi tabele `vsebina`
--
ALTER TABLE `vsebina`
  ADD PRIMARY KEY (`SelekcijaID`,`ID_Obvestila`),
  ADD KEY `ID_Obvestila` (`ID_Obvestila`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `admin`
--
ALTER TABLE `admin`
  MODIFY `ID_Admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT tabele `funkcionarji`
--
ALTER TABLE `funkcionarji`
  MODIFY `FunkcionarId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT tabele `komentarji`
--
ALTER TABLE `komentarji`
  MODIFY `ID_Komentarja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT tabele `novica`
--
ALTER TABLE `novica`
  MODIFY `ObjavaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT tabele `selekcija`
--
ALTER TABLE `selekcija`
  MODIFY `SelekcijaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=420;

--
-- AUTO_INCREMENT tabele `sponzorji`
--
ALTER TABLE `sponzorji`
  MODIFY `SponzorId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT tabele `trener`
--
ALTER TABLE `trener`
  MODIFY `TrenerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT tabele `vsebina`
--
ALTER TABLE `vsebina`
  MODIFY `ID_Obvestila` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
