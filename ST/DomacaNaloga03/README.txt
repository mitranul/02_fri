﻿Vsi uporabniki, lahko do "admin" strani dostopajo preko domene:
localhost:[port]/DomacaNaloga03/code/Admin/login.php

Vsi ostali uporabniki, pa lahko do spletne strani dostopajo preko domene:
localhost:[port]/DomacaNaloga03/code/index.php

Ob uspešni prijavi, na administratorski strani, se vzpostavi seja, ki prijavljenemu uporabniku omogoča dodajanje in urejanje zapisov v podatkovni bazi. Do takrat, je dostop, do vseh datotek nemogoč.
V podatkovni bazi sta vnešena dva uporabnika - administratorja:
    1.
        Username: Admin
        Password: Pozdravljeni12
    2.
        Username: User1
        Password: newPassword1
